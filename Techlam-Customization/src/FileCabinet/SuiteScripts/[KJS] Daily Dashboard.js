function suitelet(request, response)
	{	


		var form = nlapiCreateForm('Techlam Dashboard - Daily');

		addGoogleAnalytics(form)

		response.writePage(form);	
	


		// Create the form
		
	}

	function addGoogleAnalytics(form)
	{ 

		/******CONTROLLER SECTION*****/
		var maincontroller = nlapiLoadRecord('customrecord_dashboard_main_controller',1);

		var datepicker = maincontroller.getFieldValue('custrecord_date_picker');
		var rangefld = maincontroller.getFieldText('custrecord_specific_range');
		var fromdate = maincontroller.getFieldValue('custrecord_so_fromdate_filter');
		var todate = maincontroller.getFieldValue('custrecord_so_todate_filter');
		var targetVal = maincontroller.getFieldValue('custrecord_target_total');

		var ondemandfrom = maincontroller.getFieldValue('custrecord_ondemand_from');
		var ondemandto = maincontroller.getFieldValue('custrecord_ondemand_to');
		var ondemandDR = maincontroller.getFieldValue('custrecord_ondemand_datepicker');
		var ondemandPR = maincontroller.getFieldText('custrecord_ondemand_dateperiod');
		

		var sototal = form.addField('custpage_google_sototal', 'inlinehtml');

		//LOGSSSSSSS
		nlapiLogExecution('DEBUG','range',rangefld);
		nlapiLogExecution('DEBUG','datepick',datepicker);


		//Sales Order Total
		var filterSO = new Array();
			if (datepicker == 'T')
			{
		    	filterSO[0] = new nlobjSearchFilter("saleseffectivedate",null,"within",fromdate,todate);
			}else{
		    	filterSO[0] = new nlobjSearchFilter("saleseffectivedate",null,"within",rangefld);
			}
				filterSO[1] = new nlobjSearchFilter("type",null,"anyof","SalesOrd");

	    var searchSO = nlapiSearchRecord("transaction",'customsearch_dashboard_transaction',filterSO);	
		var columnSO = searchSO[0].getAllColumns();
	    var totalSO = searchSO[0].getValue(columnSO[0]);
	    var totalOrderLams = searchSO[0].getValue(columnSO[1]);

		var totalcontainer;
		var totalorderlamcon;
			nlapiLogExecution('DEBUG','Total OLM',totalOrderLams);


		if(totalSO == ""){
			totalcontainer = 0;

		}else{
			totalcontainer = totalSO;
			nlapiLogExecution('DEBUG','Total SO',totalcontainer);
		}


	 if(totalOrderLams == "")
	 	{
			totalorderlamcon= 0;
		}else{
			totalorderlamcon = totalOrderLams;

			nlapiLogExecution('DEBUG','Total ORDERLM',totalorderlamcon);

		}



		//Quote
		var filterQuote = new Array();
    	if (datepicker == 'T')
      	{
          filterQuote[0] = new nlobjSearchFilter("datecreated",null,"within",fromdate,todate);
      	}else{
          filterQuote[0] = new nlobjSearchFilter("datecreated",null,"within",rangefld);
      	}
		  filterQuote[1] = new nlobjSearchFilter("type",null,"anyof","Estimate");

		  var searchQuote = nlapiSearchRecord("transaction",'customsearch_dashboard_transaction',filterQuote);	
		  var columnQuote = searchQuote[0].getAllColumns();
	      var totalQuote = searchQuote[0].getValue(columnQuote[0]);
	      var totalquotecon = totalQuote;

			nlapiLogExecution('DEBUG','Total Quote',totalquotecon);


	      if (totalQuote == ""){
			
			totalquotecon = 0;

	      }




		//M3 OUT
		var filterM3out;
		if (datepicker =='T'){

			filterM3out =["systemnotes.date","within",fromdate,todate];
		}else{
			filterM3out =["systemnotes.date","within",rangefld];
		}

		var searchm3out =  nlapiSearchRecord("itemfulfillment",null,
		[
		   ["type","anyof","ItemShip"], 
		   "AND", 
		   ["status","anyof","ItemShip:C"], 
		//    "AND", 
		//    ["mainline","is","T"], 
		   "AND", 
		   [[["systemnotes.oldvalue","is","Packed"],"OR",["systemnotes.oldvalue","is","Picked"]],"AND",["systemnotes.newvalue","is","Shipped"]], 
		   "AND", filterM3out
		], 
		[
		//    new nlobjSearchColumn("custbody_m3_total_if",null,"SUM")
		new nlobjSearchColumn("formulanumeric",null,"SUM").setFormula("ROUND({appliedtotransaction.custcol_craft_trxn_salelength}*({appliedtotransaction.custcol_craft_trxn_depth}/1000)*({appliedtotransaction.custcol_craft_trxn_thickness}/1000)*{quantity},3)")
		]
		);
		var columnM3 = searchm3out[0].getAllColumns();
		var totalM3out = searchm3out[0].getValue(columnM3[0]);
		var totalM3outdata = totalM3out;

		if (totalM3out == ""){
			totalM3outdata = 0;
		}



		//Current Average Lead Time

		  var aveLeadsearch = nlapiSearchRecord("transaction",'customsearch_ave_lead_time');	
		  var aveLeadcol = aveLeadsearch[0].getAllColumns();
	      var aveLeadTime = aveLeadsearch[0].getValue(aveLeadcol[0]);


		//DIFOTIS
		var filterDIFO = new Array();
		if(datepicker =='T'){

		filterDIFO[0] = new nlobjSearchFilter("custrecord14",null,"within",fromdate,todate);

		}else{
		filterDIFO[0] = new nlobjSearchFilter("custrecord14",null,"within",rangefld);
		}

		var difosearch = nlapiSearchRecord('customrecord_craft_internal_prodorder','customsearch_difotis_dashboard',filterDIFO);
		var arraydifo = new Array();
		// var labels = ["Status","Total Jobs"];
		// arraydifo.push(labels);
		if (difosearch!=null){
		for (var i = 0; i < difosearch.length; i++){
		var subarraydifo= new Array();
		var results = difosearch[i];
		var columns = results.getAllColumns();
			
		// labels.push(columns[i].getLabel());

		var sub = results.getValue(columns[0]);
			if (sub == 1){
			subarraydifo.push(results.getText(columns[0]));
			}else if (sub ==2){
			subarraydifo.push(results.getText(columns[0]));
			}
				for (var n = 1; n < columns.length; n++){
			
				subarraydifo.push(parseInt(results.getValue(columns[n])));

				}
				arraydifo.push(subarraydifo);
				}
			}

	
		//IN-SPEC
		var filterrev = new Array();

		if(datepicker =='T'){

		filterrev[0] = new nlobjSearchFilter("custrecord14",null,"within",fromdate,todate);

		}else{
		filterrev[0] = new nlobjSearchFilter("custrecord14",null,"within",rangefld);
		}


		var searchrev  = nlapiSearchRecord('customrecord_craft_internal_prodorder','customsearch592',filterrev);
		var arrayrev = new Array();

		if (searchrev != null){

			for (var i = 0; i < searchrev.length; i ++){
		var subarrayrev = new Array();
		var resultsrev = searchrev[i];
		var columnsrev = resultsrev.getAllColumns();
        
        subarrayrev.push(resultsrev.getValue(columnsrev[0]));
		for (var n = 1; n < columnsrev.length; n++){
			
				subarrayrev.push(parseInt(resultsrev.getValue(columnsrev[n])));
						}
		arrayrev.push(subarrayrev);
		}

		}else{
			arrayrev.push(["None",1])
		}
		
		/******MODEL SECTION*****/

		nlapiLogExecution('DEBUG','array',arraydifo);
		nlapiLogExecution('DEBUG','arrayrev',JSON.stringify(arrayrev));

		//Target %

		var perValue = (totalcontainer/targetVal)*100;
		var perFinal = parseFloat(perValue).toFixed(2);


		var scriptSO='';
			scriptSO+='<html>';
			scriptSO+='<head>';
			scriptSO+='<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>';
			scriptSO+='<script type="text/javascript">';
			scriptSO+='google.charts.load("current", {"packages":["table","corechart","gauge"]});';
			//scriptSO+='google.charts.setOnLoadCallback(drawChartSO);';
			scriptSO+='google.charts.setOnLoadCallback(drawChartDIFOTIS);';
			//scriptSO+='google.charts.setOnLoadCallback(drawTable);';
			scriptSO+='google.charts.setOnLoadCallback(drawChartpie);';



			scriptSO+='function drawChartpie() {';
			scriptSO+='var datadifo =  new google.visualization.DataTable();';
			scriptSO+='datadifo.addColumn("string","Reason");';
			scriptSO+='datadifo.addColumn("number","Count");';
			scriptSO+='datadifo.addRows('+JSON.stringify(arrayrev)+');';
				//scriptSO+='datadifo.addRows([["ontime", 10],["delayed", 20]]);';
			scriptSO+=' var optionsdifo = {';
			scriptSO+='title: "Reason for Delay",';
			scriptSO+='titleTextStyle: {fontName:"Roboto",fontSize: 20,bold: true, color:"FFFFFF"},';
			scriptSO+='legend:{textStyle: {fontName:"Roboto Slab",fontSize: 15,bold: true, color:"FFFFFF"}, position: "labeled"},';
			scriptSO+='chartArea:{width: "100%",height: "80%"},';
			scriptSO+='backgroundColor:{"fill": "292020"},';
			scriptSO+='pieSliceTextStyle: {"fontSize": 14,"bold": true},';
			scriptSO+='pieHole: "0.4"';
			scriptSO+='};';
			scriptSO+='var chartdifo = new google.visualization.PieChart(document.getElementById("chartdonut"));';
			scriptSO+='chartdifo.draw(datadifo, optionsdifo);';
			scriptSO+='}';


   			scriptSO+='function drawChartDIFOTIS() {';
			scriptSO+='var datadifo =  new google.visualization.DataTable();';
			scriptSO+='datadifo.addColumn("string","Status");';
			scriptSO+='datadifo.addColumn("number","Total");';
			scriptSO+='datadifo.addRows('+JSON.stringify(arraydifo)+');';
				//scriptSO+='datadifo.addRows([["ontime", 10],["delayed", 20]]);';
			scriptSO+=' var optionsdifo = {';
			scriptSO+='title: "DIFOT",';
      	    scriptSO+='colors: ["#17B423","#E81C0F"],';
			scriptSO+='titleTextStyle: {fontName:"Roboto",fontSize: 20,bold: true, color:"FFFFFF"},';
			scriptSO+='legend:{textStyle: {fontName:"Roboto Slab",fontSize: 20,bold: true, color:"FFFFFF"}},';
			scriptSO+='chartArea:{width: "100%",height: "80%"},';
			scriptSO+='backgroundColor:{"fill": "292020"},';
			scriptSO+='pieSliceTextStyle: {"fontSize": 32,"bold": true}'
			scriptSO+='};';
			scriptSO+='var chartdifo = new google.visualization.PieChart(document.getElementById("piechart"));';
			scriptSO+='chartdifo.draw(datadifo, optionsdifo);';
			scriptSO+='}';



         	//HTML Body Div Layout
			scriptSO+='</script>';
			scriptSO+='<style>'

			//TOP
			scriptSO+='table.GeneratedTable {';
			scriptSO+='width: 1800px;';
			scriptSO+='position: absolute;';
			scriptSO+='left:50px;';
			scriptSO+='top:80px;';
			scriptSO+='background-color: #292020;';
			scriptSO+='border-collapse: separate;';
			scriptSO+='border-width: 5px;';
			scriptSO+='border-color: #b9b6a6;';
			scriptSO+='border-style: hidden;';
			scriptSO+='border-spacing: 50px 0rem;';
			scriptSO+='color: #000000;'
			scriptSO+='}';

			scriptSO+='table.GeneratedTable th {';
			scriptSO+='height: 80px;';
			scriptSO+='font-size: 43px;';
			scriptSO+='font-family: "Roboto Slab", serif;';
			scriptSO+='color:#F3FBFB;';
			scriptSO+='text-align:center;';
			scriptSO+='border-width: 2px;';
			scriptSO+='border-color: #101313;';
			scriptSO+='border-style: outset;';
			scriptSO+='border-bottom-style:ridge;';
			scriptSO+='padding: 3px;';
			scriptSO+='}';


			scriptSO+='table.GeneratedTable td{';
			scriptSO+='height: 60px;';
			scriptSO+='font-size: 40px;';
			scriptSO+='font-weight: bold;';
			scriptSO+='font-family: "Roboto", sans-serif;';
			scriptSO+='color:#F3FBFB;';
			scriptSO+='text-align:center;';
			scriptSO+='border-width: 2px;';
			scriptSO+='border-color: #101313;';
			scriptSO+='border-style: outset;';
			scriptSO+='border-top-style:hidden;';
			scriptSO+='background-color:#282929;';
			scriptSO+='padding: 3px;';
			scriptSO+='}';
			

			scriptSO+='table.GeneratedTable thead {';
			scriptSO+='background-color: #282929;';
			scriptSO+='}';
			

			//SIDE- Right Top
			scriptSO+='table.sidetable {';
			scriptSO+='width: 550px;';
			scriptSO+='position: absolute;';
			scriptSO+='left:1360px;';
			scriptSO+='top:400px;';
			scriptSO+='background-color: #292020;';
			scriptSO+='border-collapse: separate;';
			scriptSO+='border-width: 5px;';
			scriptSO+='border-color: #b9b6a6;';
			scriptSO+='border-style: hidden;';
			scriptSO+='border-spacing: 50px 0rem;';
			scriptSO+='color: #000000;'
			scriptSO+='}';

			scriptSO+='table.sidetable th {';
			scriptSO+='height: 80px;';
			scriptSO+='font-size: 43px;';
			// scriptSO+='font-weight: bold;';
			scriptSO+='font-family: "Roboto Slab", serif;';
			scriptSO+='color:#F3FBFB;';
			scriptSO+='text-align:center;';
			scriptSO+='border-width: 2px;';
			scriptSO+='border-color: #101313;';
			scriptSO+='border-style: outset;';
			scriptSO+='border-bottom-style:ridge;';
			scriptSO+='padding: 3px;';
			scriptSO+='}';


			scriptSO+='table.sidetable td{';
			scriptSO+='height: 60px;';
			scriptSO+='font-size: 40px;';
			scriptSO+='font-weight: bold;';
			scriptSO+='font-family: "Roboto", sans-serif;';
			scriptSO+='color:#F3FBFB;';
			scriptSO+='text-align:center;';
			scriptSO+='border-width: 2px;';
			scriptSO+='border-color: #101313;';
			scriptSO+='border-style: outset;';
			scriptSO+='border-top-style:hidden;';
			scriptSO+='background-color:#282929;';
			scriptSO+='padding: 3px;';
			scriptSO+='}';
			

			scriptSO+='table.sidetable thead {';
			scriptSO+='background-color: #282929;';
			scriptSO+='}';
			

			//SIDE- Right Bottom
			scriptSO+='table.sidetablebot {';
			scriptSO+='width: 550px;';
			scriptSO+='position: absolute;';
			scriptSO+='left:1360px;';
			scriptSO+='top:600px;';
			scriptSO+='background-color: #292020;';
			scriptSO+='border-collapse: separate;';
			scriptSO+='border-width: 5px;';
			scriptSO+='border-color: #b9b6a6;';
			scriptSO+='border-style: hidden;';
			scriptSO+='border-spacing: 50px 0rem;';
			scriptSO+='color: #000000;'
			scriptSO+='}';

			scriptSO+='table.sidetablebot th {';
			scriptSO+='height: 80px;';
			scriptSO+='font-size: 43px;';
			scriptSO+='font-family: "Roboto Slab", serif;';
			scriptSO+='color:#F3FBFB;';
			scriptSO+='text-align:center;';
			scriptSO+='border-width: 2px;';
			scriptSO+='border-color: #101313;';
			scriptSO+='border-style: outset;';
			scriptSO+='border-bottom-style:ridge;';
			scriptSO+='padding: 3px;';
			scriptSO+='}';

			if (perFinal < 100){

			scriptSO+='table.sidetablebot td{';
			scriptSO+='height: 60px;';
			scriptSO+='font-size: 40px;';
			scriptSO+='font-weight: bold;';
			scriptSO+='font-family: "Roboto", sans-serif;';
			scriptSO+='color:#DF1108;';
			scriptSO+='text-align:center;';
			scriptSO+='border-width: 2px;';
			scriptSO+='border-color: #101313;';
			scriptSO+='border-style: outset;';
			scriptSO+='border-top-style:hidden;';
			scriptSO+='background-color:#282929;';
			scriptSO+='padding: 3px;';
			scriptSO+='}';
			

			}else{

			scriptSO+='table.sidetablebot td{';
			scriptSO+='height: 60px;';
			scriptSO+='font-size: 40px;';
			scriptSO+='font-weight: bold;';
			scriptSO+='font-family: "Roboto", sans-serif;';
			scriptSO+='color:#67D32E;';
			scriptSO+='text-align:center;';
			scriptSO+='border-width: 2px;';
			scriptSO+='border-color: #101313;';
			scriptSO+='border-style: outset;';
			scriptSO+='border-top-style:hidden;';
			scriptSO+='background-color:#282929;';
			scriptSO+='padding: 3px;';
			scriptSO+='}';
			}
			

			scriptSO+='table.sidetablebot thead {';
			scriptSO+='background-color: #282929;';
			scriptSO+='}';


			scriptSO+='</style>';
			scriptSO+='<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">';
			scriptSO+='<link rel="preconnect" href="https://fonts.gstatic.com">';
			scriptSO+='<link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@200&display=swap" rel="stylesheet">';
			scriptSO+='<link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@200&family=Roboto:wght@300&display=swap" rel="stylesheet">';


			//scriptSO+='<script type="text/javascript" src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>';
			scriptSO+='</head>';
			scriptSO+='<body style="background-color:black;">';
			scriptSO+='<iframe width="105%" height="105%" style ="background-color:#292020; position: fixed;top: 0px;bottom: 0px;right: 0px;width: 120%;border: none;margin: 0;padding: 0;overflow: hidden;z-index:-999999;height: 120%;"></iframe>';
		
			/******VIEW SECTION*****/


			//TOP TABLE
			scriptSO+='<table class="GeneratedTable">';
  			scriptSO+='<thead>';
			scriptSO+='<tr>';
			scriptSO+='<th>Sales <i class="fa fa-money" style="color:#32CF05;"></i></th>';
			scriptSO+='<th>Quote <i class="fa fa-edit"></i></th>';
			scriptSO+='<th>Order L/M <i class="fa fa-cubes" style="color:#B57C12;"></i></th>';
			scriptSO+='<th>M³ Out <i class="fa fa-truck" style="color:#3886DE;"></i></th>';
			scriptSO+= '</tr>';
			scriptSO+= '</thead>';
			scriptSO+='<tbody>';
			scriptSO+='<tr>';
			scriptSO+='<td>'+'NZ$ '+parseFloat(totalcontainer).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</td>';
			scriptSO+='<td>'+'NZ$ '+parseFloat(totalquotecon).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</td>';
			scriptSO+='<td>'+parseFloat(totalorderlamcon).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</td>';
			scriptSO+='<td>'+parseFloat(totalM3outdata).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</td>';
   		    scriptSO+='</tr>';
 			scriptSO+='</tbody>';
			scriptSO+='</table>';


			//SIDE TABLE - TOP
			scriptSO+='<table class="sidetable">';
  			scriptSO+='<thead>';
			scriptSO+='<tr>';
			scriptSO+='<th>Average Lead Time</th>';		
			scriptSO+= '</tr>';
			scriptSO+= '</thead>';
			scriptSO+='<tbody>';
			scriptSO+='<tr>';
			scriptSO+='<td>'+aveLeadTime+' days</td>';
   		    scriptSO+='</tr>';
 			scriptSO+='</tbody>';
			scriptSO+='</table>';
			

			//SIDE TABLE - buttom
			scriptSO+='<table class="sidetablebot">';
  			scriptSO+='<thead>';
			scriptSO+='<tr>';
			scriptSO+='<th>Target  <i class="fa fa-bullseye"></i></th>';		
			scriptSO+= '</tr>';
			scriptSO+= '</thead>';
			scriptSO+='<tbody>';
			scriptSO+='<tr>';
			scriptSO+='<td>'+perFinal+' %</td>';
   		    scriptSO+='</tr>';
 			scriptSO+='</tbody>';
			scriptSO+='</table>';
			

			
			//DIFOTIS
			scriptSO+='<div id="piechart" style="position:absolute;left:80px; top: 400px; width:34%; height: 55%"></div>';

			scriptSO+='<div id="chartdonut" style="position:absolute;left:700px; top: 400px; width:34%; height: 55%"></div>';




			scriptSO+='</body>';
			scriptSO+='</html>';


		
		// Add the script to the inline HTML field

		sototal.setDefaultValue(scriptSO);

	}
	