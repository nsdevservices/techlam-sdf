function afterSubmitUpliftCalc(type)
{
	
if (type.toLowerCase() == 'edit')

	{
	var UC_Record = nlapiGetNewRecord();
    var oldUC_Record = nlapiGetOldRecord();
	var UC_id = UC_Record.getId();
	var trans_id = nlapiGetFieldValue('custrecord_craft_quote_no');
	var trans_line = nlapiGetFieldValue('custrecord_craft_quote_lineid');
	var trans_treatment = nlapiGetFieldText('custrecord_craft_glutreat');
	var trans_grade = nlapiGetFieldText('custrecord_craft_glugrade');
	var trans_finish = nlapiGetFieldText('custrecord_tech_finish');
    var trans_finish_v = nlapiGetFieldValue('custrecord_tech_finish');
	var trans_rate = nlapiGetFieldValue('custrecord_tech_unit_value');
	var trans_permtr = nlapiGetFieldValue('custrecord_craft_glulam_unit_price');
	var trans_name = nlapiGetFieldValue('custrecord_craft_part_no');
	var trans_name_res = trans_name.split(" ");
	var trans_length = nlapiGetFieldValue('custrecord_craft_tmbr_length');
	var trans_lnth_dec = parseFloat(trans_length);
	//nlapiLogExecution('DEBUG', 'length', trans_lnth_dec );
	var trans_qty = nlapiGetFieldValue('custrecord_craft_tmbr_qty');
	var trans_camber = nlapiGetFieldValue('custrecord_craft_precamber');
	var trans_size = nlapiGetFieldValue('custrecord_craft_sz_text');
	var sz_result = trans_size.split("x");
	var trans_note = nlapiGetFieldValue('custrecord_tech_uplift_notes');
	var trans_gpPerc = nlapiGetFieldValue('custrecord_craft_gp_percent');
    var product = nlapiGetFieldValue('custrecord_craft_product');
    var default_comp_item = nlapiLookupField('item', product, 'custitem_tech_component_item');
	var line_desc = trans_name_res[0] + ' ' + trans_name_res[1] + ' ' + ' ' + trans_size + ' ' + trans_finish + ' ' + trans_grade;
    var itemId = nlapiGetFieldValue('custrecord_component_item');
      
    //start : find if Size value changed then change RM Item as well
    var new_Sz = UC_Record.getFieldValue('custrecord_tech_uplift_new_size');
    var old_Sz = oldUC_Record.getFieldValue('custrecord_tech_uplift_new_size');
    var def_Th = UC_Record.getFieldValue('custrecord_tech_default_thickness');
    var new_Th = UC_Record.getFieldValue('custrecord_tech_uplift_new_thick');
    
    if (new_Th != def_Th)
       {
          if (new_Th == 19)
             {
               nlapiSubmitField('customrecord_tech_uplift_calculation', UC_id, 'custrecord_component_item', null);
               itemId = "";
               nlapiLogExecution('DEBUG', 'Thickness :', new_Th);
             }

          else
          {
            var itemId = "";
            var def_itemName = nlapiGetFieldText('custrecord_tech_def_component');
            var itemName = nlapiGetFieldText('custrecord_component_item');
            var treatment= nlapiGetFieldText('custrecord_craft_glutreat');

            if(new_Sz != null && new_Sz != " ")
            {
              var mainItem = itemName.split(" ");
              var def_mainItem = def_itemName.split(" ");
              nlapiLogExecution('DEBUG', 'Default RM Item treatment:', def_mainItem[def_mainItem.length-1]);


              var filter = new Array();
              filter[0] = new nlobjSearchFilter('itemid', '', 'startswith', new_Sz);
              filter[1] = new nlobjSearchFilter('itemid', '', 'contains', treatment);
              filter[2] = new nlobjSearchFilter('itemid', '', 'haskeywords', def_mainItem[def_mainItem.length-1]);
              filter[3] = new nlobjSearchFilter('itemid', null, 'doesnotcontain', "Tan-E");
              filter[4] = new nlobjSearchFilter('isinactive', null, 'is', "F");
           	  filter[5] = new nlobjSearchFilter('islotitem', null, 'is', "T");

              var recSearch = nlapiSearchRecord('inventoryitem', null, filter, null) || [] ;	

              if(recSearch.length > 0) 
              {
                itemId = recSearch[0].getId();
                var UC_Rec= nlapiLoadRecord("customrecord_tech_uplift_calculation",UC_id);
                UC_Rec.setFieldValue("custrecord_component_item",itemId);
                var newUC_Rec = nlapiSubmitRecord(UC_Rec,true);
                nlapiLogExecution('DEBUG', 'Got the New RM Item with updated size:: ', itemId );
              }
            }
          }
      }
     //start : find if Treatment's value changed then change RM Item as well
	var new_trans_treatment = UC_Record.getFieldValue('custrecord_craft_glutreat');
    var old_trans_treatment = oldUC_Record.getFieldValue('custrecord_craft_glutreat');
    var newSz = nlapiGetFieldValue('custrecord_tech_uplift_new_size');
    var new_Th = UC_Record.getFieldValue('custrecord_tech_uplift_new_thick');
    var def_treatment = nlapiGetFieldText('custrecord_tech_rmitem_def_treat');
    var new_treatment = nlapiGetFieldText('custrecord_craft_glutreat');

    if (new_trans_treatment != old_trans_treatment)
    	{
        if (new_Th == 19)
           {
             nlapiSubmitField('customrecord_tech_uplift_calculation', UC_id, 'custrecord_component_item', null);
             itemId = "";
             nlapiLogExecution('DEBUG', 'Thickness :', new_Th);
           }

        else
          {
              var itemId = "";
              var itemName = nlapiGetFieldText('custrecord_component_item');
              var def_itemName = nlapiGetFieldText('custrecord_tech_def_component');
              var treatment= nlapiGetFieldText('custrecord_craft_glutreat');

              if(itemName != null && itemName != "")
              {
                var mainItem = itemName.split(" ");
                var def_mainItem = def_itemName.split(" ");
                var isSize = false;

                if((newSz != null || newSz != "") && mainItem[0] != newSz)
                   isSize = true;

                var filter = new Array();

                if (isSize == false)
                filter[0] = new nlobjSearchFilter('itemid', '', 'startswith',mainItem[0]);
                else if (isSize == true)
                filter[0] = new nlobjSearchFilter('itemid', '', 'startswith', newSz);
                filter[1] = new nlobjSearchFilter('itemid', '', 'contains', treatment);
                filter[2] = new nlobjSearchFilter('itemid', '', 'haskeywords', def_mainItem[def_mainItem.length-1]);
                filter[3] = new nlobjSearchFilter('itemid', null, 'doesnotcontain', "Tan-E");
                filter[4] = new nlobjSearchFilter('isinactive', null, 'is', "F");
                filter[5] = new nlobjSearchFilter('islotitem', null, 'is', "T");

                var recSearch = nlapiSearchRecord('inventoryitem', null, filter, null) || [] ;	

                if(recSearch.length > 0) 
                {
                  itemId = recSearch[0].getId();
                  var UC_Rec= nlapiLoadRecord("customrecord_tech_uplift_calculation",UC_id);
                  UC_Rec.setFieldValue("custrecord_component_item",itemId);
                  var newUC_Rec = nlapiSubmitRecord(UC_Rec,true);
                  nlapiLogExecution('DEBUG', 'Got the New RM Item :: ', itemId );
                }
          }
        }
      }
     //End : find if Treatment's value changed then change RM Item as well

      // Start : Grade is GL12, GL17 and thickness between 40-45 or 60-65 then change the RM component item
      var grade= nlapiGetFieldText('custrecord_craft_glugrade');
      var gradeVl = nlapiGetFieldValue('custrecord_craft_glugrade');
      var size= nlapiGetFieldValue('custrecord_craft_sz_text');
      var itemName = nlapiGetFieldText('custrecord_component_item');
      var def_itemName = nlapiGetFieldText('custrecord_tech_def_component');
      var treatment= nlapiGetFieldText('custrecord_craft_glutreat');
      var newSz = nlapiGetFieldValue('custrecord_tech_uplift_new_size');
      var new_Th = nlapiGetFieldValue('custrecord_tech_uplift_new_thick');
      var def_Th = nlapiGetFieldValue('custrecord_tech_default_thickness');
      var def_grade = nlapiGetFieldText('custrecord_tech_rmitem_def_grade');
      
      if ((grade != def_grade) || (new_Th != def_Th))
        {
      nlapiLogExecution('DEBUG', ' value check :: ', grade + "  "+size+ "  "+itemName+ "  "+treatment);
      
      if (new_Th == 19)
       {
         nlapiSubmitField('customrecord_tech_uplift_calculation', UC_id, 'custrecord_component_item', null);
         nlapiLogExecution('DEBUG', 'Thickness :', new_Th);
         itemId = "";
       }
      
      else
      {
      if(size  != "" && itemName != "")
      {
        var thickness =  size.split("x");  
        var mainItem = itemName.split(" ");
        nlapiLogExecution('DEBUG', 'Got the old size :: ', mainItem[0]);
        nlapiLogExecution('DEBUG', 'Got the new size :: ', newSz);
        var def_mainItem = def_itemName.split(" ");
        var isGrade = false;
        var isThickness = false;
        var isSize = false;
        var newGrade = false;
        var newGrade2 = false;
        var newGrade3 = false;

      nlapiLogExecution('DEBUG', ' thickness/1:: ', parseInt(thickness[1]));

           // if(grade == "GL10" && grade != def_grade && parseInt(thickness[1]) < 180)
           //    newGrade3 = true;

            // if(grade == "GL10" && grade != def_grade && parseInt(thickness[1]) > 180)
            //   newGrade2 = true;

        	//Generalize GL10 is equal to SG10 raw material regardless of any thinkness
			//change 21/04/21 at 7:50am
			//Admin: Karl Sebastian
            if(grade == "GL10" && grade != def_grade)
              newGrade2 = true;

            if(grade == "GL12" && grade != def_grade)
              isGrade = true;
            
           if(grade == "GL17" && def_grade != grade)
              newGrade = true;
            
            if((parseInt(thickness[1]) > 40 && parseInt(thickness[1]) <46) || (parseInt(thickness[1]) > 60 && parseInt(thickness[1]) <66) || parseInt(thickness[1]) == 112)
              isThickness = true;
            if ((newSz != null || newSz != "") && mainItem[0] != newSz)
               isSize = true;
      
            //if (isGrade || isThickness || isSize)
          //{
            //var itemId = "";
      var filter = new Array();
            if(isSize == false)
            filter[0] = new nlobjSearchFilter('itemid', '','startswith',  mainItem[0]);
            else 
            filter[0] = new nlobjSearchFilter('itemid', '', 'startswith', newSz);
            
            if(newGrade == false)
            filter[1] = new nlobjSearchFilter('itemid', '', 'contains', treatment);
            else
            filter[1] = new nlobjSearchFilter('itemid', '', 'contains', "H3.2");

            //create filter beased on grade and thickness of record
            if(isGrade)
              filter[2] = new nlobjSearchFilter('itemid', '','contains', "SG12");
            else if(isThickness && !isGrade && trans_finish_v != 110)
              filter[2] = new nlobjSearchFilter('itemid', '','contains', "CCA Dress");
            else if(newGrade == true)
              filter[2] = new nlobjSearchFilter('itemid', '','contains', "Elliotti Pine");
             //nlapiLogExecution('DEBUG', ' Elliotti Pine Filter:: ', newGrade);
             else if(newGrade2)
              filter[2] = new nlobjSearchFilter('itemid', '','contains', "SG10");
             // else if(newGrade3)
             //  filter[2] = new nlobjSearchFilter('itemid', '','contains', "SG10");
            else
              filter[2] = new nlobjSearchFilter('itemid', '', 'contains', mainItem[mainItem.length-1]);
        nlapiLogExecution('DEBUG', ' Elliotti Pine Filter:: ', newGrade);
			  //filter[3] = new nlobjSearchFilter('itemid', '', 'contains', mainItem[2]);
              filter[3] = new nlobjSearchFilter('itemid', null, 'doesnotcontain', "Tan-E");
              filter[4] = new nlobjSearchFilter('isinactive', null, 'is', "F");
       		  filter[5] = new nlobjSearchFilter('islotitem', null, 'is', "T");
            
            nlapiLogExecution('DEBUG', ' Grade & Thickness :: ', thickness[1] + "  "+grade);
            //nlapiLogExecution('DEBUG', ' Grade & Thickness result :: ', filter);
			nlapiLogExecution('DEBUG', ' Grade12:: ', isGrade);
      nlapiLogExecution('DEBUG', ' Grade10/1:: ', newGrade2);
        nlapiLogExecution('DEBUG', ' Grade10/2:: ', newGrade3);



            //If item found from the search then save that item into Uplift calculation record
            var recSearch = nlapiSearchRecord('inventoryitem', null, filter, null) || [] ; 

            nlapiLogExecution('DEBUG', ' Grade & Thickness result :: ', JSON.stringify(recSearch));
 
            if(recSearch.length > 0) 
            {
              var itemId = recSearch[0].getId();
              var UC_Rec= nlapiLoadRecord("customrecord_tech_uplift_calculation",UC_id);
              UC_Rec.setFieldValue("custrecord_component_item",itemId);
              var newUC_Rec = nlapiSubmitRecord(UC_Rec,true);
              nlapiLogExecution('DEBUG', 'RM component Item updated as per Grade & Thickness :: ', itemId );
            }
       //}
   }
  }
}
	 // END : Grade is GL12 and thickness between 40-45 or 60-65 then change the RM component item
       
	var quote_id = nlapiLookupField('estimate', trans_id,'internalid');
	if (quote_id)
		{
		var quote_record = nlapiLoadRecord('estimate', trans_id);

                var quote_item_chk = quote_record.findLineItemValue('item', 'line', trans_line);
		nlapiLogExecution('DEBUG', 'post', 'line check: '+quote_item_chk);
                if (quote_item_chk >0)
                {
		quote_record.selectLineItem('item', quote_item_chk);

		quote_record.setCurrentLineItemValue('item', 'custcol_uplift_cal', UC_id);
		quote_record.setCurrentLineItemValue('item', 'custcol_craft_trxn_salelength', trans_lnth_dec);
		quote_record.setCurrentLineItemValue('item', 'quantity', trans_qty);
		quote_record.setCurrentLineItemValue('item', 'custcol_craft_trxn_camber', trans_camber);
		quote_record.setCurrentLineItemValue('item', 'custcol_craft_trxn_saletreatment', trans_treatment);
		quote_record.setCurrentLineItemValue('item', 'custcol_craft_trxn_grade_sale', trans_grade);
		//quote_record.setCurrentLineItemValue('item', 'price', -1);
		quote_record.setCurrentLineItemValue('item', 'rate', trans_rate);
		quote_record.setCurrentLineItemValue('item', 'custcol_craft_trxn_finish', trans_finish);
		quote_record.setCurrentLineItemValue('item', 'description', line_desc);
		quote_record.setCurrentLineItemValue('item', 'custcol_tech_price_per_mtr', trans_permtr);
		quote_record.setCurrentLineItemValue('item', 'custcol_tech_trxn_sz', trans_size);
                  
                  if(trans_note!=""){
                     
			quote_record.setCurrentLineItemValue('item', 'custcol_tech_trxn_notes', trans_note);

                     }
		quote_record.setCurrentLineItemValue('item', 'custcol_craft_trxn_depth', sz_result[0]);
		quote_record.setCurrentLineItemValue('item', 'custcol_craft_trxn_thickness', sz_result[1]);
		quote_record.setCurrentLineItemValue('item', 'custcol_tech_trxn_line_gp', trans_gpPerc);

        //if(itemId == null)
          //quote_record.setCurrentLineItemValue('item', 'custcol_rm_comp_item', null);		
		//else
          quote_record.setCurrentLineItemValue('item', 'custcol_rm_comp_item', itemId);
         
         quote_record.commitLineItem('item');

		nlapiSubmitRecord(quote_record, true);	

                }
		}

	var so_id = nlapiLookupField('salesorder', trans_id,'internalid');
	if (so_id)
		{
		var so_record = nlapiLoadRecord('salesorder', trans_id);
                var so_item_chk = so_record.findLineItemValue('item', 'line', trans_line);
		nlapiLogExecution('DEBUG', 'post', 'line check: '+so_item_chk);
                if (so_item_chk >0)
                {
	
		so_record.selectLineItem('item', so_item_chk);

		so_record.setCurrentLineItemValue('item', 'custcol_uplift_cal', UC_id);
		so_record.setCurrentLineItemValue('item', 'custcol_craft_trxn_salelength', trans_length);
		so_record.setCurrentLineItemValue('item', 'quantity', trans_qty);
		so_record.setCurrentLineItemValue('item', 'custcol_craft_trxn_camber', trans_camber);
		so_record.setCurrentLineItemValue('item', 'custcol_craft_trxn_saletreatment', trans_treatment);
		so_record.setCurrentLineItemValue('item', 'custcol_craft_trxn_grade_sale', trans_grade);
		so_record.setCurrentLineItemValue('item', 'rate', trans_rate);
		so_record.setCurrentLineItemValue('item', 'custcol_craft_trxn_finish', trans_finish);
		so_record.setCurrentLineItemValue('item', 'description', line_desc);
		so_record.setCurrentLineItemValue('item', 'custcol_tech_price_per_mtr', trans_permtr);
		so_record.setCurrentLineItemValue('item', 'custcol_tech_trxn_sz', trans_size);
                  
                 if(trans_note!=""){
		so_record.setCurrentLineItemValue('item', 'custcol_tech_trxn_notes', trans_note);
                 }
                  
		so_record.setCurrentLineItemValue('item', 'custcol_craft_trxn_depth', sz_result[0]);
                  //nlapiLogExecution("DEBUG", "Thickness :", sz_result[0]);
		so_record.setCurrentLineItemValue('item', 'custcol_craft_trxn_thickness', sz_result[1]);
                  //nlapiLogExecution("DEBUG", "Depth :", sz_result[1]);
		so_record.setCurrentLineItemValue('item', 'custcol_tech_trxn_line_gp', trans_gpPerc);
		//so_record.setCurrentLineItemValue('item', 'custcol_rm_comp_item', itemId);
        
         nlapiLogExecution("DEBUG", "The RM Item :", itemId);
        //if(itemId == null)
         // so_record.setCurrentLineItemValue('item', 'custcol_rm_comp_item', null);		
		//else
          so_record.setCurrentLineItemValue('item', 'custcol_rm_comp_item', itemId);
          	
                  
		so_record.commitLineItem('item');

		nlapiSubmitRecord(so_record, true);	
		}
		}

	}

}
