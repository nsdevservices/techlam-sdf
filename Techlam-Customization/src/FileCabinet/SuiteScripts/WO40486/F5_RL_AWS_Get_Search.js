/**
 *@NApiVersion 2.0
 *@NScriptType Restlet
 */
define(['../common/F5_Search', '../common/F5_Runtime'], function(f5_search, f5_runtime) {

        function _get(context) {
               try{

                       var getSeaId = f5_runtime.getScriptParameter('custscript_f5_aws_sea')
                       log.debug({title: 'getSeaId', details: getSeaId})
                       var jsonObj = f5_search.createJSONFromSearch({id:getSeaId});
                       log.debug({title: 'jsonObj', details: jsonObj})
                       return jsonObj;
               }catch (e) {
                       log.error({title: 'error details', details: e.message})
               }

        }

        function _post(context) {

        }

        function _put(context) {

        }

        function _delete(context) {

        }

        return {
                get: _get,
                post: _post,
                put: _put,
                delete: _delete
        }
});
