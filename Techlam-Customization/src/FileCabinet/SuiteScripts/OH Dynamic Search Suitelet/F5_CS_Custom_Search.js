/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 */
define(['N/url','N/currentRecord','N/format'],
    function (url,curRec,format) {
        function fieldChanged(context) {
            // Navigate to selected page
            //.match(/(?<=_)(.*?)(?=_)/g)
            var fieldCont = context.currentRecord.getText({fieldId : 'custpage_fieldcontainer'});
            fieldCont = fieldCont.split(',');
            var filteredArr = fieldCont.filter(function(data){
                return data !== '';
            });
            console.log(filteredArr)
            console.log(context.fieldId)

            if (context.fieldId == 'custpage_pageid') {
                var pageId = context.currentRecord.getValue({
                    fieldId : 'custpage_pageid'
                });

                var searchId = context.currentRecord.getValue({
                    fieldId : 'custpage_searchid'
                });
                pageId = parseInt(pageId.split('_')[1]);

                var params = {
                    'page': pageId,
                    'searchid': searchId
                }
                filteredArr.forEach(function (fieldId){
                    var filterField = context.currentRecord.getValue({
                        fieldId : fieldId
                    });
                    var getFieldType = context.currentRecord.getField({
                        fieldId : fieldId
                    });

                    if (getFieldType.type == 'date'){
                        filterField = filterField ? format.format({type: format.Type.DATE, value: filterField}) : filterField;
                    }
                    params[fieldId] = filterField;
                })



                document.location = url.resolveScript({
                    scriptId : getParameterFromURL('script'),
                    deploymentId : getParameterFromURL('deploy'),
                    params : params

                });
            }

            if (context.fieldId == 'custpage_searchid') {
                var searchId = context.currentRecord.getValue({
                    fieldId : 'custpage_searchid'
                });

                // pageId = parseInt(pageId.split('_')[1]);
                document.location = url.resolveScript({
                    scriptId : getParameterFromURL('script'),
                    deploymentId : getParameterFromURL('deploy'),
                    params : {
                        'searchid' : searchId
                    }
                });
            }
            if (filteredArr.indexOf(context.fieldId.toString()) > -1){
                var searchId = context.currentRecord.getValue({
                    fieldId : 'custpage_searchid'
                });
                var params = {
                    'searchid': searchId
                }
                filteredArr.forEach(function (fieldId){
                    var filterField = context.currentRecord.getValue({
                        fieldId : fieldId
                    });
                    var getFieldType = context.currentRecord.getField({
                        fieldId : fieldId
                    });

                    if (getFieldType.type == 'date'){
                        filterField = filterField ? format.format({type: format.Type.DATE, value: filterField}) : filterField;
                    }
                    params[fieldId] = filterField;
                })

                document.location = url.resolveScript({
                    scriptId : getParameterFromURL('script'),
                    deploymentId : getParameterFromURL('deploy'),
                    params : params
                });

            }




        }

        function exportResults(SEARCH_ID,paramsPush){
           var paramsUse = paramsPush;
           paramsUse['cust_export'] = 'export';
            var goToUrl = url.resolveScript({
                scriptId :'customscript_f5_sl_export_search',
                deploymentId :'customdeploy_f5_sl_export_sea',
                // scriptId : getParameterFromURL('script'),
                // deploymentId : getParameterFromURL('deploy'),
                params : paramsUse
            });

            window.open(goToUrl);
        }
        function getSuiteletPage(suiteletScriptId, suiteletDeploymentId, searchParam, pageId) {
            document.location = url.resolveScript({
                    scriptId : suiteletScriptId,
                deploymentId : suiteletDeploymentId,
                params : {
                        'searchid': searchParam,
                        'page' : pageId
                }
            });
        }

        function getParameterFromURL(param) {
            var query = window.location.search.substring(1);
            console.log(query)
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair[0] == param) {
                    return decodeURIComponent(pair[1]);
                }
            }
            return (false);
        }

        // function setPageTest(page){
        //     page = page.next();
        //     console.log(page)
        //     var record = curRec.get();
        //     record.setValue({fieldId: 'custpage_pagetest', value:'1'})
        //     record.selectLine({sublistId: 'custpage_table', line: 1});
        //     record.setCurrentSublistValue({fieldId: 'custpage_coltest', value: '1', sublistId: 'custpage_table'})
        //     record.commitLine({sublistId: 'custpage_table' })
        //
        // }

        return {
            fieldChanged : fieldChanged,
            getSuiteletPage : getSuiteletPage,
            exportResults: exportResults,
            //setPageTest: setPageTest
        };

    });