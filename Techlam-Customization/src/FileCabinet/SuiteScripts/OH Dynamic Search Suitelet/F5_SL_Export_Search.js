/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 */



define(['../common/F5_Search', 'N/redirect','N/encode', 'N/file'],
    function (f5_search,redirect,encode,file) {
        var logMap = {};
        function onRequest(context) {
            if (context.request.method == 'GET') {

                if (context.request.method == 'GET') {
                    log.debug('parameters push', context.request.parameters)
                    var paramsUse = context.request.parameters
                    var exportData = context.request.parameters.cust_export;
                    var searchid = context.request.parameters.searchid;
                    log.debug('searchid', searchid)


                    if (exportData == 'export'){
                        start('exportPerf')
                        var arrLines = getLines(searchid,paramsUse);
                        var file = createExcelFile(arrLines);

                        log.debug('new Arr lines ', arrLines)
                        context.response.writeFile({file: file});
                        end('exportPerf')
                    }
                }

            }
        }



        return {
            onRequest : onRequest
        };

        function getSeaExport(searchid,paramSearchData){
            log.debug('paramSearch on export sea', paramSearch)
            var paramSearch = filterParamSearch(paramSearchData);
            var filters = [];
            var trimFilters = [];
            for (var key in paramSearch) {
                var exist = false;
                if (paramSearch.hasOwnProperty(key)) {
                    // console.log(key + " -> " + paramSearchData[key]);
                    var trimmedFilter = key.substring(
                        key.indexOf("_") + 1,
                        key.lastIndexOf("_")
                    );
                    if (trimFilters.length > 0) {
                        for (var k = 0; k < trimFilters.length; k++) {
                            if (trimFilters[k] == trimmedFilter) {
                                filters.forEach(function (filtFind) {
                                    if(filtFind[0].indexOf(trimmedFilter) > -1){
                                        filtFind[3] = paramSearchData[key];
                                    }
                                })
                                exist = true;
                            }
                        }
                    }
                    if(trimmedFilter.indexOf('date') > -1 && paramSearchData[key] != '' && !exist){
                        trimFilters.push(trimmedFilter);
                        filters.push(['formuladate: {'+trimmedFilter+'}','within',paramSearchData[key],""])
                    }
                    if(trimmedFilter.indexOf('date') == -1 && paramSearchData[key] != ''){
                        filters.push(['formulatext: {'+trimmedFilter+'}','contains',paramSearchData[key]])
                    }

                }
            }
            var loadsea = f5_search.load({id: searchid.toString()});
            if(filters.length > 0){
                filters.forEach(function(filter){
                    log.debug('filter to push', filter)
                    loadsea.addFilterExpression(filter)
                })
            }
            var runsea = loadsea.run();
            var allResults = [];
            var start = 0;
            var results = [];
            do {
                results = runsea.getRange({
                    start: start,
                    end: start + 1000
                });
                allResults = allResults.concat(results)
                start += 1000;
            } while (results.length > 0);

            return allResults;
        }

        function getLines(searchid,paramSearchData){
            log.debug('paramSearchData', paramSearchData)
            var aLineItems = [];
            var aResults = getSeaExport(searchid,paramSearchData);
            log.debug({title: 'oResult', details: JSON.stringify(aResults)})

            aResults.map(function (lines){
                var mappedLine = {};
                var columns = lines.columns
                // log.debug({title: 'columns', details: columns})
                columns.forEach(function(column){
                    var value =lines.getText(column) ? lines.getText(column) : lines.getValue(column);
                    var columnName = column.name;
                    mappedLine[columnName] = value;
                })
                // log.debug({title: 'mappedLine', details: JSON.stringify(mappedLine)})
                aLineItems.push(mappedLine);
            })

            return aLineItems;
        }

        function createExcelFile(arrLines){
            var lines = arrLines;
            var xmlResultsData = '';
            var header = '';

            Object.keys(lines[0]).forEach(function (key) {
                header += '<Cell><Data ss:Type="String"> ' + key + ' </Data></Cell>';
            })
            xmlResultsData += '<Row>' + header + '</Row>';

            lines.forEach(function (result) {
                var line = '';
                Object.keys(result).forEach(function(key) {
                    line += '<Cell><Data ss:Type="String"> ' + result[key] + ' </Data></Cell>';
                });
                xmlResultsData += '<Row>' + line + '</Row>';
            })

            var xmlString = '<?xml version="1.0"?>';
            xmlString += '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" ';
            xmlString += 'xmlns:o="urn:schemas-microsoft-com:office:office" ';
            xmlString += 'xmlns:x="urn:schemas-microsoft-com:office:excel" ';
            xmlString += 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" ';
            xmlString += 'xmlns:html="http://www.w3.org/TR/REC-html40">';
            xmlString += '<Worksheet ss:Name="Sheet1">';
            xmlString += '<Table>';
            xmlString += xmlResultsData;
            xmlString += '</Table></Worksheet></Workbook>';
            var xmlEncoded = encode.convert({
                string : xmlString,
                inputEncoding : encode.Encoding.UTF_8,
                outputEncoding : encode.Encoding.BASE_64
            });

            var fileObj = file.create({name: 'Export Search.xls', fileType: file.Type.EXCEL, contents: xmlEncoded})
            log.debug('fileobj', fileObj )
            return fileObj;

        }

        function filterParamSearch(paramSearchData){
            var mappedFilters = {};
            var filteredParams = Object.keys(paramSearchData).filter(function(key) { return key.indexOf("custpage") > -1})
            filteredParams.sort().forEach(function (result) {
                mappedFilters[result] = paramSearchData[result];
            })
            return mappedFilters;

        }

        function start(name) {
            logMap[name] = new Date();

        }
        function end(name) {

            if (logMap[name]) {
                log.debug({title: name, details: 'elapsed time: ' + [(new Date() - logMap[name]) / 1000, 'seconds'].join(' ')});
            }
        }

    });