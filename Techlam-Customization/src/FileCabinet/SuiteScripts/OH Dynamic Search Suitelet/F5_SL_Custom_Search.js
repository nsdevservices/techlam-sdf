/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 */
//customsearchcustomergeneralview_4
var PAGE_SIZE = 200;
var DEFAULT_SEARCH = '';
// var ALL_SEARCH_ID = 'customsearch_f5_sea_emp_center';
var CLIENT_SCRIPT_FILE_ID = 4702558;


define(['N/ui/serverWidget', 'N/search', 'N/redirect','N/encode', 'N/file', 'N/runtime','N/format','../common/F5_Search'],
    function (serverWidget, search, redirect,encode,file,runtime,format,f5_search) {
        var logMap = {};
        function onRequest(context) {
            if (context.request.method == 'GET') {
                var form = serverWidget.createForm({
                    title : 'F5 Custom Search Page',
                    hideNavBar : false
                });

                //Client Script on Suitelet
                form.clientScriptFileId = CLIENT_SCRIPT_FILE_ID;
                // Get parameters
                var pageId = parseInt(context.request.parameters.page);
                var scriptId = context.request.parameters.script;
                var deploymentId = context.request.parameters.deploy;
                var searchParam = context.request.parameters.searchid || null;
                var exportData = context.request.parameters.custpage_f5_export;
                var filterSelect = context.request.parameters.filterColumn;
                var filterData = context.request.parameters.filterData;
                var parameters = context.request.parameters;
                var getScript = runtime.getCurrentScript();
                var getSearchList  = getScript.getParameter({name: 'custscript_f5_sl_search_list'});
                // log.debug('parameters', parameters)
                //log.debug('getSearchList start', getSearchList)


                var fieldContainers =  form.addField({
                    id : 'custpage_fieldcontainer',
                    label : 'test con',
                    type : serverWidget.FieldType.TEXTAREA
                });
                fieldContainers.updateDisplayType({
                    displayType : serverWidget.FieldDisplayType.HIDDEN
                });

                var sublist = form.addSublist({
                    id : 'custpage_table',
                    type : serverWidget.SublistType.LIST,
                    label : 'Search Data'
                });

                //List of all search available for this Suitelet
                var selectSearch = form.addField({
                    id : 'custpage_searchid',
                    label : 'Search List',
                    type : serverWidget.FieldType.SELECT
                });
                selectSearch.addSelectOption({
                    value : '',
                    text : '',
                    isSelected: true
                });

                log.debug('filterSelect', filterSelect)
                log.debug('filterData', filterData)
                // log.debug('All Sea', JSON.stringify(getAllSearch()))




                var arrListSearch = getAllSearch(getSearchList);
                for (var a = 0; a < arrListSearch.length; a++){

                    var seaId = arrListSearch[a].internalid;
                    var seatitle = arrListSearch[a].title;
                    log.debug('sea data', seaId + ' : ' + seatitle);

                    if (seaId == searchParam){
                        selectSearch.addSelectOption({
                            value : seaId,
                            text : seatitle,
                            isSelected : true
                        });
                    }else{
                        selectSearch.addSelectOption({
                            value : seaId,
                            text : seatitle
                        });
                    }

                }

                start('runSearch')

                var SEARCH_ID = searchParam == null ? DEFAULT_SEARCH :  searchParam;

                if (SEARCH_ID != '') {

                    log.debug({
                        title: 'SEARCH_ID',
                        details: runtime.getCurrentScript().getParameter('custpage_searchid')
                    })
                    var retrieveSearch = runSearch(SEARCH_ID, PAGE_SIZE, parameters);
                    end('runSearch')
                    log.debug({title: 'retrievesea', details: retrieveSearch.count})
                    var pageCount = Math.ceil(retrieveSearch.count / PAGE_SIZE);

                    // Set pageId to correct value if out of index
                    if (!pageId || pageId == '' || pageId < 0)
                        pageId = 0;
                    else if (pageId >= pageCount)
                        pageId = pageCount - 1;

                    // Add buttons to simulate Next & Previous
                    // if (pageId != 0) {
                    //     form.addButton({
                    //         id: 'custpage_previous',
                    //         label: 'Previous',
                    //         functionName: 'getSuiteletPage(' + scriptId + ', ' + deploymentId + ', ' + searchParam + ', ' + (pageId - 1) + ')'
                    //     });
                    // }
                    //
                    // if (pageId != pageCount - 1) {
                    //     form.addButton({
                    //         id: 'custpage_next',
                    //         label: 'Next',
                    //         functionName: 'getSuiteletPage(' + scriptId + ', ' + deploymentId + ', ' + searchParam + ', ' + (pageId + 1) + ')'
                    //     });
                    // }

                    log.debug('params to use', parameters)
                    // var arrParamPush = new Array();
                    // arrParamPush.push(parameters)
                    var paramsPush = JSON.stringify(parameters)
                    form.addButton({
                        id: 'custpage_export',
                        label: 'Export',
                        functionName: 'exportResults(' + SEARCH_ID + ','+paramsPush+')'
                    });
                    // if (exportData == 'export') {
                    //     var arrLines = getLines(SEARCH_ID);
                    //     var file = createExcelFile(arrLines);
                    //
                    //     log.debug('new Arr lines ', arrLines)
                    //     context.response.writeFile({file: file});
                    //
                    // }

                    // Add drop-down and options to navigate to specific page
                    var selectOptions = form.addField({
                        id: 'custpage_pageid',
                        label: 'Page Index',
                        type: serverWidget.FieldType.SELECT
                    });

                    start('addselect')
                    for (i = 0; i < pageCount; i++) {

                        if (i == pageId) {
                            selectOptions.addSelectOption({
                                value: 'pageid_' + i,
                                text: ((i * PAGE_SIZE) + 1) + ' - ' + ((i + 1) * PAGE_SIZE),
                                isSelected: true
                            });
                        } else {
                            selectOptions.addSelectOption({
                                value: 'pageid_' + i,
                                text: ((i * PAGE_SIZE) + 1) + ' - ' + ((i + 1) * PAGE_SIZE)
                            });
                        }
                    }
                    end('addselect')


                    // Get subset of data to be shown on page
                    start('fetch')

                    if (retrieveSearch.count > 0) {
                        var addResults = fetchSearchResult(retrieveSearch, pageId);
                        log.debug('addResults 0', addResults[0])
                        end('fetch')
                        var i = 1;
                        var filterfields = '';
                        Object.keys(addResults[0]).forEach(function (key) {

                            if (key.indexOf('_FILTER') > -1) {
                                var fieldIdSplit = key.split('_');
                                var fieldLabel = fieldIdSplit[0];
                                if (addResults[0][key].columnType == 'datetime' || addResults[0][key].columnType == 'date') {

                                    log.debug('parameters', parameters)
                                    log.debug('parameters', addResults[0][key].columnName)


                                    form.addField({
                                        id: 'custpage_' + addResults[0][key].columnName + '_' + i,
                                        label: 'From ' + fieldLabel,
                                        type: serverWidget.FieldType.DATE
                                    }).defaultValue = parameters['custpage_' + addResults[0][key].columnName + '_' + i]

                                    filterfields += 'custpage_' + addResults[0][key].columnName + '_' + i + ',';
                                    i++;

                                    form.addField({
                                        id: 'custpage_' + addResults[0][key].columnName + '_' + i,
                                        label: 'To ' + fieldLabel,
                                        type: serverWidget.FieldType.DATE
                                    }).defaultValue = parameters['custpage_' + addResults[0][key].columnName + '_' + i];
                                    filterfields += 'custpage_' + addResults[0][key].columnName + '_' + i + ',';
                                    i++;

                                } else {
                                    log.debug('parameters', addResults[0][key].columnName)
                                    form.addField({
                                        id: 'custpage_' + addResults[0][key].columnName + '_',
                                        label: fieldLabel,
                                        type: serverWidget.FieldType.TEXT
                                    }).defaultValue = parameters['custpage_' + addResults[0][key].columnName + '_']
                                    filterfields += 'custpage_' + addResults[0][key].columnName + '_,'
                                }

                            }
                            sublist.addField({
                                id: key.toLowerCase().replace(/[^0-9a-zA-Z]/g, ''),
                                label: key.replace('_FILTER', ''),
                                type: serverWidget.FieldType.TEXTAREA
                            });
                        })

                        fieldContainers.defaultValue = filterfields;



                        var j = 0;
                        addResults.forEach(function (result) {
                            Object.keys(result).forEach(function (key) {
                                sublist.setSublistValue({
                                    id: key.toLowerCase().replace(/[^0-9a-zA-Z]/g, ''),
                                    line: j,
                                    value:result[key].value == '' ? 'null' : result[key].value
                                    //
                                });
                            })
                            j++
                        });

                    }
                }
                if(exportData != "export"){

                    context.response.writePage(form);
                }
            }
        }

        return {
            onRequest : onRequest
        };

        function start(name) {
            logMap[name] = new Date();

        }
       function end(name) {

            if (logMap[name]) {
                log.debug({title: name, details: 'elapsed time: ' + [(new Date() - logMap[name]) / 1000, 'seconds'].join(' ')});
            }
        }

        function filterParamSearch(paramSearchData){
            var mappedFilters = {};
            var filteredParams = Object.keys(paramSearchData).filter(function(key) { return key.indexOf("custpage") > -1})

            filteredParams.sort().forEach(function (result) {
                mappedFilters[result] = paramSearchData[result];
            })
            return mappedFilters;

        }

        function runSearch(searchId, searchPageSize, paramSearchData) {

            var paramSearch = filterParamSearch(paramSearchData);
            start('runSearchData')
            log.debug('paramsearch', paramSearch)
            var filters = [];
            var trimFilters = [];
            for (var key in paramSearch) {
                var exist = false;
                if (paramSearch.hasOwnProperty(key)) {
                   // console.log(key + " -> " + paramSearchData[key]);
                    var trimmedFilter = key.substring(
                        key.indexOf("_") + 1,
                        key.lastIndexOf("_")
                    );
                    if (trimFilters.length > 0) {
                        for (var k = 0; k < trimFilters.length; k++) {
                            if (trimFilters[k] == trimmedFilter) {
                                filters.forEach(function (filtFind) {
                                    if(filtFind[0].indexOf(trimmedFilter) > -1){
                                        filtFind[3] = paramSearchData[key];
                                    }
                                })
                                exist = true;
                            }
                        }
                    }
                    if(trimmedFilter.indexOf('date') > -1 && paramSearchData[key] != '' && !exist){
                        trimFilters.push(trimmedFilter);
                        filters.push(['formuladate: {'+trimmedFilter+'}','within',paramSearchData[key],""])
                    }
                    if(trimmedFilter.indexOf('date') == -1 && paramSearchData[key] != ''){
                        filters.push(['formulatext: {'+trimmedFilter+'}','contains',paramSearchData[key]])
                    }

                }
            }
            log.debug('filters', filters)
            log.debug('trimmed', trimFilters)


            var searchObj = f5_search.load({
                id : searchId
            });
            if(filters.length > 0){
                filters.forEach(function(filter){
                    log.debug('filter to push', filter)
                    searchObj.addFilterExpression(filter)
                })
            }



            return searchObj.runPaged({
                pageSize : searchPageSize
            });
        }
        function fetchSearchResult(pagedData, pageIndex) {

            var searchPage = pagedData.fetch({
                index : pageIndex
            });
            var results = new Array();
            searchPage.data.map(function(lines){

                var mappedCols = {};
                var columns = lines.columns
                //log.debug('columns', columns);
                columns.forEach(function(column){

                    var columnStringify = JSON.stringify(column);
                    var columnParse = JSON.parse(columnStringify);
                    var value = lines.getText(column) ? lines.getText(column): lines.getValue(column);
                    // var textVal = lines.getText(column);
                    // log.debug('value check', value)
                    // log.debug('textVal check', textVal)

                    var columnLabel = column.label;
                    var columnName = column.name;
                    var columnType = columnParse.type;
                    mappedCols[columnLabel] = {value:value, columnName:columnName, columnType: columnType};
                })
                results.push(mappedCols);
            })
            return results;
        }
        function fetchColumns(pagedData, pageIndex) {

            var searchPage = pagedData.fetch({
                index : pageIndex
            });
            var results = new Array();
            log.debug('searchPage', searchPage.data.columns);

            searchPage.data.map(function(lines){

            })
            return results;
        }
        function fetchSearchpage(pagedData, pageIndex) {

            var searchPage = pagedData.fetch({
                index : pageIndex
            });
            return searchPage;
        }

        function createExcelFile(arrLines){
            var lines = arrLines;
            var xmlResultsData = '';
            var header = '';

            Object.keys(lines[0]).forEach(function (key) {
                header += '<Cell><Data ss:Type="String"> ' + key + ' </Data></Cell>';
            })
            xmlResultsData += '<Row>' + header + '</Row>';

            lines.forEach(function (result) {
                var line = '';
                Object.keys(result).forEach(function(key) {
                    line += '<Cell><Data ss:Type="String"> ' + result[key] + ' </Data></Cell>';
                });
                xmlResultsData += '<Row>' + line + '</Row>';
            })

            var xmlString = '<?xml version="1.0"?>';
            xmlString += '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" ';
            xmlString += 'xmlns:o="urn:schemas-microsoft-com:office:office" ';
            xmlString += 'xmlns:x="urn:schemas-microsoft-com:office:excel" ';
            xmlString += 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" ';
            xmlString += 'xmlns:html="http://www.w3.org/TR/REC-html40">';
            xmlString += '<Worksheet ss:Name="Sheet1">';
            xmlString += '<Table>';
            xmlString += xmlResultsData;
            xmlString += '</Table></Worksheet></Workbook>';
            var xmlEncoded = encode.convert({
                string : xmlString,
                inputEncoding : encode.Encoding.UTF_8,
                outputEncoding : encode.Encoding.BASE_64
            });

            var fileObj = file.create({name: 'Export Search.xls', fileType: file.Type.EXCEL, contents: xmlEncoded})
            log.debug('fileobj', fileObj )
            return fileObj;

        }

        function getColumnSearch(searchid){
            var loadsea = search.load({id: searchid});
            var runsea = loadsea.run();
            var allResults = [];
            var aLineItems = [];
            var start = 0;
            var results = [];

                results = runsea.getRange({
                    start: start,
                    end: 1
                });
                allResults = allResults.concat(results)


            allResults.map(function (lines){
                var mappedLine = {};
                var columns = lines.columns
                log.debug({title: 'columns', details: columns})
                columns.forEach(function(column){
                    var value =lines.getValue(column)
                    var columnName = column.name;
                    mappedLine[columnName] = value;
                })
                log.debug({title: 'mappedLine', details: JSON.stringify(mappedLine)})
                aLineItems.push(mappedLine);
            })
        return aLineItems;
        }

        function getSeaExport(revArrSea){

            var loadsea = search.load({id: revArrSea});
            var runsea = loadsea.run();
            var allResults = [];
            var start = 0;
            var results = [];
            do {
                results = runsea.getRange({
                    start: start,
                    end: start + 1000
                });
                allResults = allResults.concat(results)
                start += 1000;
            } while (results.length > 0);

            return allResults;
        }

        function getLines(searchid){
            var aLineItems = [];
            var aResults = getSeaExport(searchid);
            log.debug({title: 'oResult', details: JSON.stringify(aResults)})

            aResults.map(function (lines){
                var mappedLine = {};
                var columns = lines.columns
                columns.forEach(function(column){
                    var value =lines.getValue(column)
                    var columnName = column.name;
                    mappedLine[columnName] = value;
                })
                log.debug({title: 'mappedLine', details: JSON.stringify(mappedLine)})
                aLineItems.push(mappedLine);
            })

            return aLineItems;
        }

        function getAllSearch(getSearchList){
            //log.debug('getAllSearch', getSearchList)

            var loadsea = search.load({id: getSearchList.toString(), type: search.Type.SAVED_SEARCH});
            var runsea = loadsea.run();
            var dataResults = []
            var allResults = [];
            var start = 0;
            var results = [];
            do {
                results = runsea.getRange({
                    start: start,
                    end: start + 1000
                });
                allResults = allResults.concat(results)
                start += 1000;
            } while (results.length > 0);

            allResults.forEach(function (result) {
                var oColumns = {};
                var aColumn = result.columns;
                var intId = result.getValue(aColumn[0]);
                var title = result.getValue(aColumn[1]);
                var seaId = result.getValue(aColumn[2]);
                oColumns['internalid'] = intId;
                oColumns['title'] = title;
                oColumns['searchid'] = seaId;
                dataResults.push(oColumns);
            })

            return dataResults;

        }
    });