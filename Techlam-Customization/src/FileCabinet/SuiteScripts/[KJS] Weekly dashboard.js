function suitelet(request, response)
	{	


		var form = nlapiCreateForm('Techlam Dashboard - Weekly');

		addGoogleAnalytics(form)

		response.writePage(form);	
	


		// Create the form
		
	}

	function addGoogleAnalytics(form)
	{ 

		/******CONTROLLER SECTION*****/
		var maincontroller = nlapiLoadRecord('customrecord_dashboard_main_controller',1);
		//var datepicker = maincontroller.getFieldValue('custrecord_date_picker');
		//var rangefld = maincontroller.getFieldText('custrecord_specific_range');
		//var fromdate = maincontroller.getFieldValue('custrecord_so_fromdate_filter');
		//var todate = maincontroller.getFieldValue('custrecord_so_todate_filter');


		var weekly = maincontroller.getFieldText('custrecord_weekly_range');
		//sales line weekly
		var lineweekly = maincontroller.getFieldText('custrecord_line_weekly');
		//sales line monthly
		var linemonthly = maincontroller.getFieldText('custrecord_line_monthly');
		//sales target
		var salestar = maincontroller.getFieldValue('custrecord_target_total');
		//Breakeven
		var breakeve = maincontroller.getFieldValue('custrecord_controller_breakeven');
		//Breakeven controller
		var breakevecon = maincontroller.getFieldText('custrecord_control_breakeven');


		var sototal = form.addField('custpage_google_sototal', 'inlinehtml');

		//LOGSSSSSSS
		//nlapiLogExecution('DEBUG','range',rangefld);
		//nlapiLogExecution('DEBUG','datepick',datepicker);

		//Sales Order Total
		var filterSO = new Array();
		
		    	filterSO[0] = new nlobjSearchFilter("saleseffectivedate",null,"within",weekly);
		
				filterSO[1] = new nlobjSearchFilter("type",null,"anyof","SalesOrd");

	    var searchSO = nlapiSearchRecord("transaction",'customsearch_dashboard_transaction',filterSO);	
		var columnSO = searchSO[0].getAllColumns();
	    var totalSO = searchSO[0].getValue(columnSO[0]);
	    var totalOrderLams = searchSO[0].getValue(columnSO[1]);

		var totalcontainer;
		var totalorderlamcon;
			nlapiLogExecution('DEBUG','Total OLM',totalOrderLams);


		if(totalSO == ""){
			totalcontainer = 0;

		}else{
			totalcontainer = totalSO;
			nlapiLogExecution('DEBUG','Total SO',totalcontainer);
		}


	 if(totalOrderLams == "")
	 	{
			totalorderlamcon= 0;
		}else{
			totalorderlamcon = totalOrderLams;

			nlapiLogExecution('DEBUG','Total ORDERLM',totalorderlamcon);

		}



		//Quote
		var filterQuote = new Array();
   
          filterQuote[0] = new nlobjSearchFilter("datecreated",null,"within",weekly);
      	
		  filterQuote[1] = new nlobjSearchFilter("type",null,"anyof","Estimate");

		  var searchQuote = nlapiSearchRecord("transaction",'customsearch_dashboard_transaction',filterQuote);	
		  var columnQuote = searchQuote[0].getAllColumns();
	      var totalQuote = searchQuote[0].getValue(columnQuote[0]);
	      var totalquotecon = totalQuote;

			nlapiLogExecution('DEBUG','Total Quote',totalquotecon);


	      if (totalQuote == ""){
			
			totalquotecon = 0;

	      }

		//Total L/M & Average 
		var filterTLM = new Array();

	
		    	filterTLM[0] = new nlobjSearchFilter("custrecord_fj_date",null,"within",weekly);
			
		var searchTLM = nlapiSearchRecord('customrecord_craft_internal_prodorder','customsearch_total_lm_search',filterTLM);
		var columnTLM = searchTLM[0].getAllColumns();

		var totallinemedata = searchTLM[0].getValue(columnTLM[0]);
		var avelmhourly= totallinemedata/10;
		var totalconlmdata;

		if( totallinemedata == ""){

			totallinemedata = 0;
		}
 


		//M3 OUT
		var filterM3out;
	
			filterM3out =["systemnotes.date","within",weekly];
		
		var searchm3out =  nlapiSearchRecord("itemfulfillment",null,
		[
		   ["type","anyof","ItemShip"], 
		   "AND", 
		   ["status","anyof","ItemShip:C"], 
		   "AND", 
		   ["mainline","is","T"], 
		   "AND", 
		   [[["systemnotes.oldvalue","is","Packed"],"OR",["systemnotes.oldvalue","is","Picked"]],"AND",["systemnotes.newvalue","is","Shipped"]], 
		   "AND", filterM3out
		], 
		[
		   new nlobjSearchColumn("custbody_m3_total_if",null,"SUM")
		]
		);
		var columnM3 = searchm3out[0].getAllColumns();
		var totalM3out = searchm3out[0].getValue(columnM3[0]);
		var totalM3outdata = totalM3out;

		if (totalM3out == ""){
			totalM3outdata = 0;
		}



		//Current Average Lead Time

		  var aveLeadsearch = nlapiSearchRecord("transaction",'customsearch_ave_lead_time');	
		  var aveLeadcol = aveLeadsearch[0].getAllColumns();
	      var aveLeadTime = aveLeadsearch[0].getValue(aveLeadcol[0]);


		//DIFOTIS
		var filterDIFO = new Array();

		filterDIFO[0] = new nlobjSearchFilter("custrecord14",null,"within",weekly);
		

		var difosearch = nlapiSearchRecord('customrecord_craft_internal_prodorder','customsearch_difotis_dashboard',filterDIFO);
		var arraydifo = new Array();
		// var labels = ["Status","Total Jobs"];
		// arraydifo.push(labels);
		if (difosearch!=null){
		for (var i = 0; i < difosearch.length; i++){
		var subarraydifo= new Array();
		var results = difosearch[i];
		var columns = results.getAllColumns();
			
		// labels.push(columns[i].getLabel());

		var sub = results.getValue(columns[0]);
			if (sub == 1){
			subarraydifo.push(results.getText(columns[0]));
			}else if (sub ==2){
			subarraydifo.push(results.getText(columns[0]));
			}
				for (var n = 1; n < columns.length; n++){
			
				subarraydifo.push(parseInt(results.getValue(columns[n])));

				}
				arraydifo.push(subarraydifo);
				}
			}

	
		//IN-SPEC
		var filterrev = new Array();

		
		filterrev[0] = new nlobjSearchFilter("custrecord14",null,"within",weekly);
		


		var searchrev  = nlapiSearchRecord('customrecord_craft_internal_prodorder','customsearch592',filterrev);
		var arrayrev = new Array();

		if (searchrev != null){

			for (var i = 0; i < searchrev.length; i ++){
		var subarrayrev = new Array();
		var resultsrev = searchrev[i];
		var columnsrev = resultsrev.getAllColumns();
        
        subarrayrev.push(resultsrev.getValue(columnsrev[0]));
		for (var n = 1; n < columnsrev.length; n++){
			
				subarrayrev.push(parseInt(resultsrev.getValue(columnsrev[n])));
						}
		arrayrev.push(subarrayrev);
		}

		}else{
			arrayrev.push(["None",1])
		}


		 //Line- sales weekly
		 	var filterlineweek = new Array();
		 	filterlineweek[0]= new nlobjSearchFilter("saleseffectivedate",null,"within",lineweekly);
		 	filterlineweek[1]= new nlobjSearchFilter("type",null,"anyof","SalesOrd");
			var sealine  = nlapiSearchRecord('transaction','customsearch695',filterlineweek);
					var arrayline = new Array();
			        var firdata = 0;
			        var target = 0			       
					if (sealine != null){
 					
						for (var i = 0; i < sealine.length; i ++){
					var subarrayline = new Array();
					var resultsline = sealine[i];
					var columnsline = resultsline.getAllColumns();
			        
			        subarrayline.push(resultsline.getValue(columnsline[0]));
					for (var n = 1; n < columnsline.length; n++){
			            var target2 = parseInt(salestar);
			            var secdata = parseInt(resultsline.getValue(columnsline[n]))
			            var finaldata = firdata += secdata;
			            var finaltarget = target += target2;
							subarrayline.push(finaldata);
			                subarrayline.push(finaltarget);
			          

									}
					arrayline.push(subarrayline);
					}

					}
		nlapiLogExecution('DEBUG','ArrayLine',JSON.stringify(arrayline));


		// Line- Sales Monthly

		var filterlinemonth = new Array();
		 	filterlinemonth[0]= new nlobjSearchFilter("datecreated",null,"within",linemonthly);
		 	filterlinemonth[1]= new nlobjSearchFilter("type",null,"anyof","SalesOrd");
			var sealinemon  = nlapiSearchRecord('transaction','customsearch695',filterlinemonth);
					var arraylinemonthly = new Array();
			        var firdatamon = 0;
			        var targetmon = 0
			        //var targetBE = 0;
			       
					if (sealinemon != null){
 					var totalleng = sealinemon.length;
			        var targetbreak = breakeve / totalleng;
						for (var k = 0; k < sealinemon.length; k ++){
					var subarraylinemon = new Array();
					var resultslinemon = sealinemon[k];
					var columnslinemon = resultslinemon.getAllColumns();


			        
			        subarraylinemon.push(resultslinemon.getValue(columnslinemon[0]));
					for (var m = 1; m < columnslinemon.length; m++){
			            var target2mon = parseInt(salestar);
			            var secdatamon = parseInt(resultslinemon.getValue(columnslinemon[m]))
			            var finaldatamon = firdatamon += secdatamon;
			            var finaltargetmon = targetmon += target2mon;
			            var finaltargetBE = 650000;
							subarraylinemon.push(finaldatamon);
			                subarraylinemon.push(finaltargetmon);
			                subarraylinemon.push(finaltargetBE);		  
									}
					arraylinemonthly.push(subarraylinemon);
					}

					}
		nlapiLogExecution('DEBUG','ArrayLine',JSON.stringify(arraylinemonthly));

		//BreakEven Data

		var filterbed = new Array();
		
		    	filterbed[0] = new nlobjSearchFilter("saleseffectivedate",null,"within",breakevecon);
		
				filterbed[1] = new nlobjSearchFilter("type",null,"anyof","SalesOrd");

					    var searchBed = nlapiSearchRecord("transaction",'customsearch_dashboard_transaction',filterbed);	
						var columnBed = searchBed[0].getAllColumns();
					    var totalBed = searchBed[0].getValue(columnBed[0]);

							var totalcontainerBed;


							if(totalBed == ""){
								totalcontainerBed = 0;

							}else{
								totalcontainerBed = totalBed;
								nlapiLogExecution('DEBUG','Total SO',totalcontainerBed);
							}


				//Breakeven - Percentage

				var totalper = (totalcontainerBed/breakeve)*100;

		
		/******MODEL SECTION*****/

		nlapiLogExecution('DEBUG','array',arraydifo);
		nlapiLogExecution('DEBUG','arrayrev',JSON.stringify(arrayrev));


		var scriptSO='';
			scriptSO+='<html>';
			scriptSO+='<head>';
			scriptSO+='<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>';
			scriptSO+='<script type="text/javascript">';
			scriptSO+='google.charts.load("current", {"packages":["table","corechart","gauge","line"]});';
			//scriptSO+='google.charts.setOnLoadCallback(drawChartSO);';
			scriptSO+='google.charts.setOnLoadCallback(drawChartDIFOTIS);';
			//scriptSO+='google.charts.setOnLoadCallback(drawTable);';
			scriptSO+='google.charts.setOnLoadCallback(drawChartpie);';
			scriptSO+='google.charts.setOnLoadCallback(drawLinethismonth);';
			scriptSO+='google.charts.setOnLoadCallback(drawLinelastmonth);';


			scriptSO+='function drawChartpie() {';
			
			scriptSO+='var datadifo =  new google.visualization.DataTable();';
			scriptSO+='datadifo.addColumn("string","Reason");';
			scriptSO+='datadifo.addColumn("number","Count");';
			scriptSO+='datadifo.addRows('+JSON.stringify(arrayrev)+');';
				//scriptSO+='datadifo.addRows([["ontime", 10],["delayed", 20]]);';
			scriptSO+=' var optionsdifo = {';
			scriptSO+='title: "Reason for Delay",';
			scriptSO+='titleTextStyle: {fontName:"Roboto",fontSize: 20, color:"FFFFFF"},';
			scriptSO+='legend:{textStyle: {fontName:"Roboto Slab",fontSize: 15, color:"FFFFFF"}, position: "labeled"},';
			scriptSO+='chartArea:{width: "80%",height: "80%"},';
			scriptSO+='backgroundColor:{"fill": "292020"},';
			scriptSO+='pieSliceTextStyle: {"fontSize": 14,"bold": true},';
			scriptSO+='pieHole: "0.4"';
			scriptSO+='};';
			scriptSO+='var chartdifo = new google.visualization.PieChart(document.getElementById("chartdonut"));';
			scriptSO+='chartdifo.draw(datadifo, optionsdifo);';


			scriptSO+='}';




   			scriptSO+='function drawChartDIFOTIS() {';
			scriptSO+='var datadifo =  new google.visualization.DataTable();';
			scriptSO+='datadifo.addColumn("string","Status");';
			scriptSO+='datadifo.addColumn("number","Total");';
			scriptSO+='datadifo.addRows('+JSON.stringify(arraydifo)+');';
				//scriptSO+='datadifo.addRows([["ontime", 10],["delayed", 20]]);';
			scriptSO+=' var optionsdifo = {';
			scriptSO+='title: "DIFOT",';
			scriptSO+='colors: ["#17B423","#E81C0F"],';
			scriptSO+='titleTextStyle: {fontName:"Roboto",fontSize: 20, color:"FFFFFF"},';
			scriptSO+='legend:{textStyle: {fontName:"Roboto Slab",fontSize: 20, color:"FFFFFF"}},';
			scriptSO+='chartArea:{width: "80%",height: "80%"},';
			scriptSO+='backgroundColor:{"fill": "292020"},';
			scriptSO+='pieSliceTextStyle: {"fontSize": 32,"bold": true}'
			scriptSO+='};';
			scriptSO+='var chartdifo = new google.visualization.PieChart(document.getElementById("piechart"));';
			scriptSO+='chartdifo.draw(datadifo, optionsdifo);';
			scriptSO+='}';



			//Sales Line - This Month 

			
				scriptSO+='function drawLinethismonth() {';
				scriptSO+='var dataline = new google.visualization.DataTable();';
				scriptSO+='dataline.addColumn("string", "Date");';
				scriptSO+='dataline.addColumn("number", "Actual");';
				scriptSO+='dataline.addColumn("number", "Target");';
				scriptSO+='dataline.addColumn("number", "Break");';
				scriptSO+='dataline.addRows('+JSON.stringify(arraylinemonthly)+');';
					scriptSO+='var optionline = {';
				//scriptSO+='chart: {title: "Testing Karl"},';

				scriptSO+='title: "Month to Date",';
				scriptSO+='titleTextStyle: {color: "white", bold: "true"},';
				scriptSO+='backgroundColor: {fill: "#292020"},';
				scriptSO+='chartArea: {backgroundColor: "#292020"},';
				scriptSO+='width: 1000,';
				scriptSO+='height: 270,';
				//scriptSO+='series: {0:{curveType: "function"}},';
				//scriptSO+='axes: {y:{Actual :{label:"Sales Actual"}, Target: {label: "Sales Traget"}}, x: {label: "Date"}}';
				scriptSO+='};';
				scriptSO+=' var chartline = new google.charts.Line(document.getElementById("chart_line"));';
				scriptSO+='chartline.draw(dataline, google.charts.Line.convertOptions(optionline));';
				scriptSO+='}';


				//Sales Line - this  week 
				scriptSO+='function drawLinelastmonth() {';
				scriptSO+='var dataline = new google.visualization.DataTable();';
				scriptSO+='dataline.addColumn("string", "Date");';
				scriptSO+='dataline.addColumn("number", "Actual");';
				scriptSO+='dataline.addColumn("number", "Target");';
				scriptSO+='dataline.addRows('+JSON.stringify(arrayline)+');';
				scriptSO+='var optionline = {';
					//scriptSO+='chart: {title: "Testing Karl"},';

				scriptSO+='title: "Week to Date",';
				scriptSO+='titleTextStyle: {color: "white", bold: "true"},';
				scriptSO+='backgroundColor: {fill: "#292020"},';
				scriptSO+='chartArea: {backgroundColor: "#292020"},';
				scriptSO+='width: 1000,';
				scriptSO+='height: 270,';
				//scriptSO+='series: {0:{curveType: "function"}},';
					//scriptSO+='axes: {y:{Actual :{label:"Sales Actual"}, Target: {label: "Sales Traget"}}, x: {label: "Date"}}';
				scriptSO+='};';
				scriptSO+=' var chartline = new google.charts.Line(document.getElementById("chart_linelast"));';
				scriptSO+='chartline.draw(dataline, google.charts.Line.convertOptions(optionline));';
				scriptSO+='}';

			scriptSO+='</script>';

			scriptSO+='<style>'

			//Sales
			scriptSO+='table.GeneratedTable {';
			scriptSO+='width: 400px;';
			scriptSO+='position: absolute;';
			scriptSO+='left:5px;';
			scriptSO+='top:80px;';
			scriptSO+='background-color: #292020;';
			scriptSO+='border-collapse: separate;';
			scriptSO+='border-width: 5px;';
			scriptSO+='border-color: #b9b6a6;';
			scriptSO+='border-style: hidden;';
			scriptSO+='border-spacing: 50px 0rem;';
			scriptSO+='color: #000000;'
			scriptSO+='}';

			scriptSO+='table.GeneratedTable th {';
			scriptSO+='height: 60px;';
			scriptSO+='font-size: 30px;';
			scriptSO+='font-family: "Roboto Slab", serif;';
			scriptSO+='color:#F3FBFB;';
			scriptSO+='text-align:center;';
			scriptSO+='border-width: 2px;';
			scriptSO+='border-color: #101313;';
			scriptSO+='border-style: outset;';
			scriptSO+='border-bottom-style:ridge;';
			scriptSO+='padding: 3px;';
			scriptSO+='}';


			scriptSO+='table.GeneratedTable td{';
			scriptSO+='height: 60px;';
			scriptSO+='font-size: 25px;';
			scriptSO+='font-weight: bold;';
			scriptSO+='font-family: "Roboto", sans-serif;';
			scriptSO+='color:#F3FBFB;';
			scriptSO+='text-align:center;';
			scriptSO+='border-width: 2px;';
			scriptSO+='border-color: #101313;';
			scriptSO+='border-style: outset;';
			scriptSO+='border-top-style:hidden;';
			scriptSO+='background-color:#282929;';
			scriptSO+='padding: 3px;';
			scriptSO+='}';
			

			scriptSO+='table.GeneratedTable thead {';
			scriptSO+='background-color: #282929;';
			scriptSO+='}';


			//Quote
			scriptSO+='table.quotetable {';
			scriptSO+='width: 400px;';
			scriptSO+='position: absolute;';
			scriptSO+='left:400px;';
			scriptSO+='top:80px;';
			scriptSO+='background-color: #292020;';
			scriptSO+='border-collapse: separate;';
			scriptSO+='border-width: 5px;';
			scriptSO+='border-color: #b9b6a6;';
			scriptSO+='border-style: hidden;';
			scriptSO+='border-spacing: 50px 0rem;';
			scriptSO+='color: #000000;'
			scriptSO+='}';

			scriptSO+='table.quotetable th {';
			scriptSO+='height: 60px;';
			scriptSO+='font-size: 30px;';
			scriptSO+='font-family: "Roboto Slab", serif;';
			scriptSO+='color:#F3FBFB;';
			scriptSO+='text-align:center;';
			scriptSO+='border-width: 2px;';
			scriptSO+='border-color: #101313;';
			scriptSO+='border-style: outset;';
			scriptSO+='border-bottom-style:ridge;';
			scriptSO+='padding: 3px;';
			scriptSO+='}';


			scriptSO+='table.quotetable td{';
			scriptSO+='height: 60px;';
			scriptSO+='font-size: 25px;';
			scriptSO+='font-weight: bold;';
			scriptSO+='font-family: "Roboto", sans-serif;';
			scriptSO+='color:#F3FBFB;';
			scriptSO+='text-align:center;';
			scriptSO+='border-width: 2px;';
			scriptSO+='border-color: #101313;';
			scriptSO+='border-style: outset;';
			scriptSO+='border-top-style:hidden;';
			scriptSO+='background-color:#282929;';
			scriptSO+='padding: 3px;';
			scriptSO+='}';
			

			scriptSO+='table.quotetable thead {';
			scriptSO+='background-color: #282929;';
			scriptSO+='}';



			//Order L/M
			scriptSO+='table.ordertable {';
			scriptSO+='width: 400px;';
			scriptSO+='position: absolute;';
			scriptSO+='left:5px;';
			scriptSO+='top:330px;';
			scriptSO+='background-color: #292020;';
			scriptSO+='border-collapse: separate;';
			scriptSO+='border-width: 5px;';
			scriptSO+='border-color: #b9b6a6;';
			scriptSO+='border-style: hidden;';
			scriptSO+='border-spacing: 50px 0rem;';
			scriptSO+='color: #000000;'
			scriptSO+='}';

			scriptSO+='table.ordertable th {';
			scriptSO+='height: 60px;';
			scriptSO+='font-size: 30px;';
			scriptSO+='font-family: "Roboto Slab", serif;';
			scriptSO+='color:#F3FBFB;';
			scriptSO+='text-align:center;';
			scriptSO+='border-width: 2px;';
			scriptSO+='border-color: #101313;';
			scriptSO+='border-style: outset;';
			scriptSO+='border-bottom-style:ridge;';
			scriptSO+='padding: 3px;';
			scriptSO+='}';


			scriptSO+='table.ordertable td{';
			scriptSO+='height: 60px;';
			scriptSO+='font-size: 25px;';
			scriptSO+='font-weight: bold;';
			scriptSO+='font-family: "Roboto", sans-serif;';
			scriptSO+='color:#F3FBFB;';
			scriptSO+='text-align:center;';
			scriptSO+='border-width: 2px;';
			scriptSO+='border-color: #101313;';
			scriptSO+='border-style: outset;';
			scriptSO+='border-top-style:hidden;';
			scriptSO+='background-color:#282929;';
			scriptSO+='padding: 3px;';
			scriptSO+='}';
			

			scriptSO+='table.ordertable thead {';
			scriptSO+='background-color: #282929;';
			scriptSO+='}';

			//M3 out
			scriptSO+='table.cubictable {';
			scriptSO+='width: 400px;';
			scriptSO+='position: absolute;';
			scriptSO+='left:400px;';
			scriptSO+='top:330px;';
			scriptSO+='background-color: #292020;';
			scriptSO+='border-collapse: separate;';
			scriptSO+='border-width: 5px;';
			scriptSO+='border-color: #b9b6a6;';
			scriptSO+='border-style: hidden;';
			scriptSO+='border-spacing: 50px 0rem;';
			scriptSO+='color: #000000;'
			scriptSO+='}';

			scriptSO+='table.cubictable th {';
			scriptSO+='height: 60px;';
			scriptSO+='font-size: 30px;';
			scriptSO+='font-family: "Roboto Slab", serif;';
			scriptSO+='color:#F3FBFB;';
			scriptSO+='text-align:center;';
			scriptSO+='border-width: 2px;';
			scriptSO+='border-color: #101313;';
			scriptSO+='border-style: outset;';
			scriptSO+='border-bottom-style:ridge;';
			scriptSO+='padding: 3px;';
			scriptSO+='}';


			scriptSO+='table.cubictable td{';
			scriptSO+='height: 60px;';
			scriptSO+='font-size: 25px;';
			scriptSO+='font-weight: bold;';
			scriptSO+='font-family: "Roboto", sans-serif;';
			scriptSO+='color:#F3FBFB;';
			scriptSO+='text-align:center;';
			scriptSO+='border-width: 2px;';
			scriptSO+='border-color: #101313;';
			scriptSO+='border-style: outset;';
			scriptSO+='border-top-style:hidden;';
			scriptSO+='background-color:#282929;';
			scriptSO+='padding: 3px;';
			scriptSO+='}';
			

			scriptSO+='table.cubictable thead {';
			scriptSO+='background-color: #282929;';
			scriptSO+='}';



			//Avg Lead time
			scriptSO+='table.leadtable {';
			scriptSO+='width: 700px;';
			scriptSO+='position: absolute;';
			scriptSO+='left:1150px;';
			scriptSO+='top:700px;';
			scriptSO+='background-color: #292020;';
			scriptSO+='border-collapse: separate;';
			scriptSO+='border-width: 5px;';
			scriptSO+='border-color: #b9b6a6;';
			scriptSO+='border-style: hidden;';
			scriptSO+='border-spacing: 50px 0rem;';
			scriptSO+='color: #000000;'
			scriptSO+='}';

			scriptSO+='table.leadtable th {';
			scriptSO+='height: 60px;';
			scriptSO+='font-size: 30px;';
			scriptSO+='font-family: "Roboto Slab", serif;';
			scriptSO+='color:#F3FBFB;';
			scriptSO+='text-align:center;';
			scriptSO+='border-width: 2px;';
			scriptSO+='border-color: #101313;';
			scriptSO+='border-style: outset;';
			scriptSO+='border-bottom-style:ridge;';
			scriptSO+='padding: 3px;';
			scriptSO+='}';


			scriptSO+='table.leadtable td{';
			scriptSO+='height: 60px;';
			scriptSO+='font-size: 25px;';
			scriptSO+='font-weight: bold;';
			scriptSO+='font-family: "Roboto", sans-serif;';
			scriptSO+='color:#F3FBFB;';
			scriptSO+='text-align:center;';
			scriptSO+='border-width: 2px;';
			scriptSO+='border-color: #101313;';
			scriptSO+='border-style: outset;';
			scriptSO+='border-top-style:hidden;';
			scriptSO+='background-color:#282929;';
			scriptSO+='padding: 3px;';
			scriptSO+='}';
			

			scriptSO+='table.leadtable thead {';
			scriptSO+='background-color: #282929;';
			scriptSO+='}';

			//HTML Body Div Layout

			scriptSO+='</style>';
			scriptSO+='<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">';
			scriptSO+='<link rel="preconnect" href="https://fonts.gstatic.com">';
			scriptSO+='<link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@200&display=swap" rel="stylesheet">';
			scriptSO+='<link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@200&family=Roboto:wght@300&display=swap" rel="stylesheet">';

			scriptSO+='</head>';
			scriptSO+='<body style="background-color:black;">';
			scriptSO+='<iframe width="105%" height="105%" style ="background-color:#292020; position: fixed;top: 0px;bottom: 0px;right: 0px;width: 120%;border: none;margin: 0;padding: 0;overflow: hidden;z-index:-999999;height: 120%;"></iframe>';
		
			/******VIEW SECTION*****/


			//Sales Order Div
				
			scriptSO+='<table class="GeneratedTable">';
  			scriptSO+='<thead>';
			scriptSO+='<tr>';
			scriptSO+='<th>Sales <i class="fa fa-money" style="color:#32CF05;"></i></th>';
			scriptSO+= '</tr>';
			scriptSO+= '</thead>';
			scriptSO+='<tbody>';
			scriptSO+='<tr>';
			scriptSO+='<td>'+'NZ$ '+parseFloat(totalcontainer).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</td>';
   		    scriptSO+='</tr>';
 			scriptSO+='</tbody>';
			scriptSO+='</table>';

			//Quote
			scriptSO+='<table class="quotetable">';
  			scriptSO+='<thead>';
			scriptSO+='<tr>';
			scriptSO+='<th>Quote <i class="fa fa-edit"></i></th>';
			scriptSO+= '</tr>';
			scriptSO+= '</thead>';
			scriptSO+='<tbody>';
			scriptSO+='<tr>';
			scriptSO+='<td>'+'NZ$ '+parseFloat(totalquotecon).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</td>';
   		    scriptSO+='</tr>';
 			scriptSO+='</tbody>';
			scriptSO+='</table>';

			//Order L/M		
			scriptSO+='<table class="ordertable">';
  			scriptSO+='<thead>';
			scriptSO+='<tr>';
			scriptSO+='<th>Order L/M <i class="fa fa-cubes" style="color:#B57C12;"></i></th>';
			scriptSO+= '</tr>';
			scriptSO+= '</thead>';
			scriptSO+='<tbody>';
			scriptSO+='<tr>';
			scriptSO+='<td>'+parseFloat(totalorderlamcon).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</td>';
   		    scriptSO+='</tr>';
 			scriptSO+='</tbody>';
			scriptSO+='</table>';

			 //M3 out Div 1280px
			scriptSO+='<table class="cubictable">';
  			scriptSO+='<thead>';
			scriptSO+='<tr>';
			scriptSO+='<th>M³ Out <i class="fa fa-truck" style="color:#3886DE;"></i></th>';
			scriptSO+= '</tr>';
			scriptSO+= '</thead>';
			scriptSO+='<tbody>';
			scriptSO+='<tr>';
			scriptSO+='<td>'+parseFloat(totalM3outdata).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</td>';
   		    scriptSO+='</tr>';
 			scriptSO+='</tbody>';
			scriptSO+='</table>';


			//Lead time
			scriptSO+='<table class="leadtable">';
  			scriptSO+='<thead>';
			scriptSO+='<tr>';
			scriptSO+='<th>Average Lead Time</th>';		
			scriptSO+= '</tr>';
			scriptSO+= '</thead>';
			scriptSO+='<tbody>';
			scriptSO+='<tr>';
			scriptSO+='<td>'+aveLeadTime+' days</td>';
   		    scriptSO+='</tr>';
 			scriptSO+='</tbody>';
			scriptSO+='</table>';

			//Sales-Line This Month
			scriptSO+='<div id="chart_line" style="position:absolute; left:800px; top: 30px"></div>'

			//sales-Line Last Month
			scriptSO+='<div id="chart_linelast" style="position:absolute; left:800px; top: 300px"></div>'

			
			//DIFOTIS
			scriptSO+='<div id="piechart" style="position:absolute;left:-30px; top: 590px; width:35%; height: 35%"></div>';

			scriptSO+='<div id="chartdonut" style="position:absolute;left:520px; top: 590px; width:35%; height: 35%"></div>';

			

			scriptSO+='</body>';
			scriptSO+='</html>';


		
		// Add the script to the inline HTML field+[]

		sototal.setDefaultValue(scriptSO);
		// totallineme.setDefaultValue(scriptTLM);
		// avelmhourlyfld.setDefaultValue(scriptavehr);
	 //    totalM3outfld.setDefaultValue(scriptM3out);
	
	}
	