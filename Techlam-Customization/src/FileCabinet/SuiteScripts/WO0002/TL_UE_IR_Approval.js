/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * Task          Date                Author                                         Remarks
 * WO-0002      11/05/2023          NDS(ns.devservices@outlook.com)                 Inital script design
 */
define(['N/record','N/search','N/runtime','./TL_IR_Utils'],
    
    (record,search,runtime,tl_util) => {
        /**
         * Defines the function definition that is executed before record is loaded.
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {string} scriptContext.type - Trigger type; use values from the context.UserEventType enum
         * @param {Form} scriptContext.form - Current form
         * @param {ServletRequest} scriptContext.request - HTTP request information sent from the browser for a client action only.
         * @since 2015.2
         */
        /**
         * LOGIC PLAN
         * - hide generate button if processing approved is not marked
         * - show banner that record is pending approval if processing approved is not marked
         * - create saved search for approvers based on employee record
         *  - use saved search as parameter 
         */
        const beforeLoad = (scriptContext) => {
            var form = scriptContext.form;
            var type = scriptContext.type;
            var recId = scriptContext.newRecord.id;
            var recType = scriptContext.newRecord.type;
            var searchIRs = 860;
            form.clientScriptModulePath = './TL_CS_SetActionButton.js';
            if (type == 'view'){

                var options = {
                    searchIRs,
                    recType,
                    recId
                }

                var loadData = tl_util.searchLoad(options)
            
                if (loadData > 0){
                   var genBtn= form.getButton({ id: 'custpage_pobutton' }) 

                   if(genBtn){
                      form.removeButton({ id: 'custpage_pobutton'})
                   }

                    form.addButton({
                        id: 'custpage_approveIR',
                        label: 'Approve',
                        functionName: 'doApprove('+recId+')'
                    })
                    
             
        
                }
            }
        }

        /**
         * Defines the function definition that is executed before record is submitted.
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @param {string} scriptContext.type - Trigger type; use values from the context.UserEventType enum
         * @since 2015.2
         */
        const beforeSubmit = (scriptContext) => {

        }

        /**
         * Defines the function definition that is executed after record is submitted.
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @param {string} scriptContext.type - Trigger type; use values from the context.UserEventType enum
         * @since 2015.2
         */
        const afterSubmit = (scriptContext) => {

        }

        return {beforeLoad, beforeSubmit, afterSubmit}

    });
