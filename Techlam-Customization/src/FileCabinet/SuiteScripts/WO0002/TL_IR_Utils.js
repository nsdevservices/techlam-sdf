/**
 * @NApiVersion 2.1
 */
define(['N/search'],
    
    (search) => {

        const searchLoad = (options) => {

         var seaLoad = search.load({ id: options.searchIRs, type: options.recType })
         var keyColumnMap = {};

          if(options.recId){
            seaLoad.filters.push(search.createFilter({ name: 'internalid', operator: search.Operator.ANYOF, values: options.recId}));
         }
         
        //  seaLoad.columns.forEach(function (column) {
        //     var label = column.label.toUpperCase().replace(' ', '_');
        //         keyColumnMap[label] = column;
        //         });
        
        //  var jsonRes = getJSONResults({keyColumnMap,seaLoad});
        var resData = getResults(seaLoad);

        return resData.length;
    }


        function getResults(seaLoad){

            let limit = 4000;
            let allResults = [];
            let rs = seaLoad.run();

            rs.each(function (result) {
                allResults.push(result);

                return allResults.length < limit;
            });

            if (allResults.length == limit) {
                let start = limit;
                let results = [];
                do {
                    results = rs.getRange({
                        start: start,
                        end: start + 1000
                    });
                    allResults = allResults.concat(results);
                    start += 1000;
                } while (results.length > 0);
            }

            return allResults;
        }

        function getJSONResults(options) {
         
            var jsonResults = [];

            (getResults(options.seaLoad) || []).forEach(function (result) {
                var jsonResult = {};
                for (var key in options.keyColumnMap) {
                    var column = options.keyColumnMap[key];
                    if (column) {
                        jsonResult[key] = key.indexOf('Text') > -1 ? result.getText(column) : result.getValue(column);
                    }
                }
                jsonResults.push(jsonResult);
            });

            return jsonResults || [];
          
        };



        return {searchLoad}

    });
