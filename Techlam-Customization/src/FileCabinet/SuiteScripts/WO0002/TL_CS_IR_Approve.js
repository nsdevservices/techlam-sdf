/**
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 * Task          Date                Author                                         Remarks 
*/
define(['N/record'],

function(record) {

    function lineInit(ctx) {}
    
    function pageInit(ctx) {}
    
    function postSourcing(ctx) {}
    
    function saveRecord(ctx) {}
    
    function sublistChanged(ctx) {}
    
    function validateDelete(ctx) {}
    
    function validateField(ctx) {}
    
    function validateInsert(ctx) {}
    
    function validateLine(ctx) {}
    
    function fieldChanged(ctx) {}

    function doApprove()

    return {
            // lineInit: lineInit,
            // pageInit: pageInit,
            // postSourcing : postSourcing,
            // saveRecord : saveRecord,
            // sublistChanged : sublistChanged,
            // validateDelete : validateDelete,
            // validateField : validateField,
            // validateInsert : validateInsert,
            // validateLine : validateLine,
            // fieldChanged : fieldChanged
            doApprove: doApprove
        };
 }
);
