/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 * Task          Date                Author                                         Remarks
 * WO-0002      11/05/2023          NDS(ns.devservices@outlook.com)                 Inital script design
*/
 define(['N/'],

    function() {


        function beforeLoad(context) {}
        
        function afterSubmit(context) {}
        
        function beforeSubmit(context) {}

        return {
                beforeLoad : beforeLoad,
                afterSubmit : afterSubmit,
                beforeSubmit : beforeSubmit
          }
   }
)