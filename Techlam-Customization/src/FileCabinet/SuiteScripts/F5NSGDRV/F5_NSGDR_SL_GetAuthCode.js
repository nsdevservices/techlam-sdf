/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 */
define(['N/runtime', './common/F5_Url', './common/F5_Https', './common/F5_Constants'],
    
    function (runtime, f5_url, f5_https, f5_constants) {
        /**
         * Defines the Suitelet script trigger point.
         * @param {Object} scriptContext
         * @param {ServerRequest} scriptContext.request - Incoming request
         * @param {ServerResponse} scriptContext.response - Suitelet response
         * @since 2015.2
         */
        function  onRequest (scriptContext)  {

            if (scriptContext.request.method === 'GET') {

                var scriptObj = runtime.getCurrentScript()
                var sessionObj = runtime.getCurrentSession();
                var redirect_uri = f5_url.resolveScript({
                    scriptId: scriptObj.id ,
                    deploymentId: scriptObj.deploymentId ,
                    returnExternalUrl: true
                });

                var headers = {}
                var authURL = f5_url.format({
                    domain: f5_constants.GoogleDrive.authURL,
                    params: {
                        redirect_uri : redirect_uri,
                        prompt : f5_constants.GoogleDrive.prompt,
                        response_type : f5_constants.GoogleDrive.responseType,
                        client_id : f5_constants.GoogleDrive.integrationClient,
                        scope : f5_constants.GoogleDrive.scope,
                        access_type : f5_constants.GoogleDrive.accessType
                    }})
                // Send Authorization Code to get Access Token
                var response = f5_https.get({
                    url: authURL,
                })
                logOptions(response, 'options')


                var j = 1
            }


        }


        var logOptions = function (objectToLog, title) {
            var objectToLogJSON = JSON.stringify(objectToLog)
            var times = Math.round(objectToLogJSON.length / 3999);
            for (var k = 0; k <= times; k++) {
                var bodyPart = objectToLogJSON.substr(k * 3999, 3999);
                log.debug({title: title + k, details: bodyPart});
            }
        };

        return {
            onRequest: onRequest
        };

    });
