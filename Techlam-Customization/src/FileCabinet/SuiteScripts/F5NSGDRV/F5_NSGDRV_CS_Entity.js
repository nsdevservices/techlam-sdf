/**
 * Module Description
 * Displays message on Folder Creation in Google Drive
 *
 * Version    Date            Author           Remarks
 * 1.00       05 Nov 2021     F5-JIP            Initial Development
 * 1.01       03 Aug 2022     F5-JIP            Remove PageInit as not required. Added Alert for Job
 *
 * @NApiVersion 2.0
 * @NScriptType ClientScript
 */
define([
        './common/F5_Constants',
        './common/F5_Search'
    ],
    function (f5_constants, f5_search) {

        /**
         * Validation function to be executed when record is saved.
         *
         * @param {Object} scriptContext
         * @param {Record} scriptContext.currentRecord - Current form record
         * @returns {boolean} Return true if record is valid
         *
         * @since 2015.2
         */
        function saveClientRecord(scriptContext) {
            debugger
            console.log('saveRecord function')
            if ( scriptContext.currentRecord.getValue({fieldId: f5_constants.EntityField.CREATEFOLDER})) {
            var recordType = scriptContext.currentRecord.type
            if (recordType === 'customer' || recordType === 'prospect' || recordType === 'vendor')
                return true

            if (recordType === f5_constants.CustomRecordType.CONTRACTMANAGEMENT) {
                var customer = scriptContext.currentRecord.getValue({fieldId: f5_constants.ContractManagementField.BILLTOCUSTOMER})
                if (!customer){
                    var vendor = scriptContext.currentRecord.getValue({fieldId: f5_constants.ContractManagementField.VENDOR})
                }

            }

            if (recordType === 'job') {
                customer = scriptContext.currentRecord.getValue({fieldId: f5_constants.EntityField.PARENT})
            }
            if (recordType === 'opportunity') {
                customer = scriptContext.currentRecord.getValue({fieldId: 'entity'})
            }
            if (customer) {
                var parentGoogleDriveInfo = f5_search.lookupFields({
                    type: f5_search.Type.CUSTOMER,
                    id: customer,
                    columns: [f5_constants.EntityField.ISFOLDERCREATED, f5_constants.EntityField.GDRVFOLDERID]
                })
                if (!parentGoogleDriveInfo.custentity_f5_nsgdrv_foldercreated) {
                    if (recordType === 'job') {
                        alert('Cannot create this folder in Google Drive. The Customer (Bill to Organization) needs to have a Google drive folder created. Please create the folder first. ');
                    } else {
                        alert('Cannot create this folder. The Customer needs to have a Google drive folder created.');
                    }
                    return false;
                }
            }
            if (vendor){
                 parentGoogleDriveInfo = f5_search.lookupFields({
                    type: f5_search.Type.VENDOR,
                    id: vendor,
                    columns: [f5_constants.EntityField.ISFOLDERCREATED, f5_constants.EntityField.GDRVFOLDERID]
                })
                if (!parentGoogleDriveInfo.custentity_f5_nsgdrv_foldercreated) {
                    alert('Cannot create this folder. The Vendor needs to have a Google drive folder created.');
                    return false;
                }
            }
            }
            return true;
        }

        return {
            saveRecord: saveClientRecord

        };
    });