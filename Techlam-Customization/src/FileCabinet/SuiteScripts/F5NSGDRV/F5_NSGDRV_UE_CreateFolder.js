/**
 *@NApiVersion 2.0
 *@NScriptType UserEventScript
 */

define([
    './common/F5_Message',
    './common/F5_Redirect',
    './common/F5_Search',
    './common/F5_Constants',
    './common/F5_Format',
    './common/F5_Runtime',
    './common/F5_Error',
    './common/F5_Record',
    'N/log',
    './common/F5_ServerWidget',
    './common/F5_Shared',
    './common/F5_Customer',
    './F5_NSGDRV_Lib',
    'N/error',
    './common/F5_HREmployee',
    './common/F5_ContractMgmt',
    './common/F5_Opportunity',
    './common/F5_Project',
], function (f5_message, f5_redirect, f5_search,  f5_constants, f5_format, f5_runtime, f5_error, f5_record, log, f5_serverWidget, f5_shared, f5_customer, f5_nsgdrv, error, f5_hremployee, f5_contractmgmt, f5_opportunity, f5_project) {

    function beforeLoad(context) {

        var form = context.form;
        var recordToProcess = context.newRecord;
        var recordType = recordToProcess.type
        var isFolderCreatedFieldName,
            dateFolderCreatedFieldName,
            folderUrlFieldName,
            folderIdFieldName,
            createFolderFieldName;

        log.debug({title: 'recordType', details: recordType});

        switch (recordType) {
            case 'customrecord_oh_contract_management':
                isFolderCreatedFieldName = f5_constants.ContractManagementField.ISFOLDERCREATED;
                dateFolderCreatedFieldName = f5_constants.ContractManagementField.DATEFOLDERCREATED;
                folderUrlFieldName = f5_constants.ContractManagementField.FOLDERURL;
                folderIdFieldName = f5_constants.ContractManagementField.GDRVFOLDERID;
                createFolderFieldName = f5_constants.ContractManagementField.CREATEFOLDER;
                break;
            case 'customrecord_f5_oh_hremp_record':
                isFolderCreatedFieldName = f5_constants.HREmployeeField.ISFOLDERCREATED;
                dateFolderCreatedFieldName = f5_constants.HREmployeeField.DATEFOLDERCREATED;
                folderUrlFieldName = f5_constants.HREmployeeField.FOLDERURL;
                folderIdFieldName = f5_constants.HREmployeeField.GDRVFOLDERID;
                createFolderFieldName = f5_constants.HREmployeeField.CREATEFOLDER;
                break;
            case 'opportunity' :
                isFolderCreatedFieldName = f5_constants.TransactionField.ISFOLDERCREATED;
                dateFolderCreatedFieldName = f5_constants.TransactionField.DATEFOLDERCREATED;
                folderUrlFieldName = f5_constants.TransactionField.FOLDERURL;
                folderIdFieldName = f5_constants.TransactionField.GDRVFOLDERID;
                createFolderFieldName = f5_constants.TransactionField.CREATEFOLDER;
                break;
            default:
                isFolderCreatedFieldName = f5_constants.EntityField.ISFOLDERCREATED;
                dateFolderCreatedFieldName = f5_constants.EntityField.DATEFOLDERCREATED;
                folderUrlFieldName = f5_constants.EntityField.FOLDERURL;
                folderIdFieldName = f5_constants.EntityField.GDRVFOLDERID;
                createFolderFieldName = f5_constants.EntityField.CREATEFOLDER;
        }

        //   if (recordType === 'customer' || recordType === 'vendor' || recordType === 'Prospect'){
        if (context.type === context.UserEventType.CREATE) {
            isFolderCreatedFld = form.getField(isFolderCreatedFieldName);
            isFolderCreatedFld.updateDisplayType({
                displayType: f5_serverWidget.FieldDisplayType.HIDDEN
            });
            dateFolderCreatedFld = form.getField(dateFolderCreatedFieldName);
            dateFolderCreatedFld.updateDisplayType({
                displayType: f5_serverWidget.FieldDisplayType.HIDDEN
            })
            var folderUrlFld = form.getField(folderUrlFieldName)
            folderUrlFld.updateDisplayType({
                displayType: f5_serverWidget.FieldDisplayType.HIDDEN
            })
        }

        if (context.type === context.UserEventType.EDIT) {


            var recordId = recordToProcess.id
            var isFolderCreated = recordToProcess.getValue(isFolderCreatedFieldName)
            if (isFolderCreated) {
                if (f5_runtime.getCurrentRole() !== f5_constants.Role.ADMINISTRATOR) {


                    // For Administrator, show fields to enable corrections
                    createFolderFld = form.getField(createFolderFieldName);
                    createFolderFld.updateDisplayType({
                        displayType: f5_serverWidget.FieldDisplayType.HIDDEN
                    });
                    isFolderCreatedFld = form.getField(isFolderCreatedFieldName);
                    isFolderCreatedFld.updateDisplayType({
                        displayType: f5_serverWidget.FieldDisplayType.DISABLED
                    });
                    var folderIdFld = form.getField(folderIdFieldName);
                    folderIdFld.updateDisplayType({
                        displayType: f5_serverWidget.FieldDisplayType.DISABLED
                    });
                    var dateFolderCreatedFld = form.getField(dateFolderCreatedFieldName);
                    dateFolderCreatedFld.updateDisplayType({
                        displayType: f5_serverWidget.FieldDisplayType.DISABLED
                    });

                    folderUrlFld = form.getField(folderUrlFieldName)
                    folderUrlFld.updateDisplayType({
                        displayType: f5_serverWidget.FieldDisplayType.DISABLED
                    })
                } else {
                    // Displays the link
                    createFolderFld = form.getField(createFolderFieldName);
                    createFolderFld.updateDisplayType({
                        displayType: f5_serverWidget.FieldDisplayType.NORMAL
                    });
                    isFolderCreatedFld = form.getField(isFolderCreatedFieldName);
                    isFolderCreatedFld.updateDisplayType({
                        displayType: f5_serverWidget.FieldDisplayType.NORMAL
                    });
                    folderIdFld = form.getField(folderIdFieldName);
                    folderIdFld.updateDisplayType({
                        displayType: f5_serverWidget.FieldDisplayType.NORMAL
                    });
                    dateFolderCreatedFld = form.getField(dateFolderCreatedFieldName);
                    dateFolderCreatedFld.updateDisplayType({
                        displayType: f5_serverWidget.FieldDisplayType.NORMAL
                    });
                    folderUrlFld = form.getField(folderUrlFieldName)
                    folderUrlFld.updateDisplayType({
                        displayType: f5_serverWidget.FieldDisplayType.NORMAL
                    })
                }

            } else {

                isFolderCreatedFld = form.getField(isFolderCreatedFieldName);
                isFolderCreatedFld.updateDisplayType({
                    displayType: f5_serverWidget.FieldDisplayType.HIDDEN
                });
                dateFolderCreatedFld = form.getField(dateFolderCreatedFieldName);
                dateFolderCreatedFld.updateDisplayType({
                    displayType: f5_serverWidget.FieldDisplayType.HIDDEN
                });
                folderIDFld = form.getField(folderIdFieldName);
                folderIDFld.updateDisplayType({
                    displayType: f5_serverWidget.FieldDisplayType.HIDDEN
                });
                folderUrlFld = form.getField(folderUrlFieldName)
                folderUrlFld.updateDisplayType({
                    displayType: f5_serverWidget.FieldDisplayType.HIDDEN
                })

            }

        }

        if (context.type === context.UserEventType.VIEW) {

            recordId = recordToProcess.id

            isFolderCreated = recordToProcess.getValue(isFolderCreatedFieldName)
            if (isFolderCreated) {
                var createFolderFld = form.getField(createFolderFieldName);
                createFolderFld.updateDisplayType({
                    displayType: f5_serverWidget.FieldDisplayType.HIDDEN
                });
                var isFolderCreatedFld = form.getField(isFolderCreatedFieldName);
                isFolderCreatedFld.updateDisplayType({
                    displayType: f5_serverWidget.FieldDisplayType.HIDDEN
                });
                var folderIDFld = form.getField(folderIdFieldName);
                folderIDFld.updateDisplayType({
                    displayType: f5_serverWidget.FieldDisplayType.NORMAL
                });
                var folderURLFld = form.getField(folderUrlFieldName);
                folderURLFld.updateDisplayType({
                    displayType: f5_serverWidget.FieldDisplayType.NORMAL
                });

                try {
                    if (f5_runtime.isUserInterfaceContext()) {
                        // Checks if the request came from the redirect
                        if (context.request.parameters.custparam_f5_nsgdrv_foldercreated) {
                            var isFolderCreatedFromRequest = (context.request.parameters.custparam_f5_nsgdrv_foldercreated === 'true')
                            var form = context.form;
                            if (isFolderCreatedFromRequest) {
                                form.addPageInitMessage({
                                    type: f5_message.Type.CONFIRMATION,
                                    message: 'Google Drive Folder was successfully created',
                                    duration: 8000
                                });
                            } else {
                                form.addPageInitMessage({
                                    type: f5_message.Type.ERROR,
                                    message: 'There was an issue and the Google Drive Folder was not created. The error message was: ' + context.request.parameters.custparam_f5_nsgdrv_errormessage
                                });
                            }
                        }
                    }
                } catch (e) {
                    log.error('beforeLoad: Unexpected Error', f5_format.exceptionToString(e));
                }

            } else {

                isFolderCreatedFld = form.getField(isFolderCreatedFieldName);
                isFolderCreatedFld.updateDisplayType({
                    displayType: f5_serverWidget.FieldDisplayType.HIDDEN
                });
                dateFolderCreatedFld = form.getField(dateFolderCreatedFieldName);
                dateFolderCreatedFld.updateDisplayType({
                    displayType: f5_serverWidget.FieldDisplayType.HIDDEN
                });
                folderIDFld = form.getField(folderIdFieldName);
                folderIDFld.updateDisplayType({
                    displayType: f5_serverWidget.FieldDisplayType.HIDDEN
                });
                folderURLFld = form.getField(folderUrlFieldName);
                folderURLFld.updateDisplayType({
                    displayType: f5_serverWidget.FieldDisplayType.HIDDEN
                });


            }

        }
        //  }
    }


    function afterSubmit(context) {
        try {
            log.debug('F5_NSGDR_UE_CreateFolder.afterSubmit', 'START');
            var recordToProcess = context.newRecord;
            var scriptObj = f5_runtime.getCurrentScript();
            var googleDriveConf = JSON.parse(scriptObj.getParameter({name: f5_constants.ScriptParameter.GOOGLEDRIVECONF}))
            log.debug({title: 'GoogleDriveConf', details: scriptObj.getParameter({name: f5_constants.ScriptParameter.GOOGLEDRIVECONF})})
            // Creates the transRecord variable with the new record if it is create or edit
            if (context.type === context.UserEventType.CREATE || context.type === context.UserEventType.EDIT) {

                // Check if Customer marked for Creation and Site has not been created yet.
                var createFolder = f5_nsgdrv.isCreateFolderChecked(context);
                var isFolderCreated = f5_nsgdrv.isFolderCreatedChecked(context);
                log.debug({title: 'createFolder', details: createFolder });
                log.debug({title: 'isFolderCreated', details: isFolderCreated});
                if ((context.type === context.UserEventType.CREATE || context.type === context.UserEventType.EDIT) && createFolder === true && !isFolderCreated) {
                    // Triggers the call to Goggle
                    log.debug('F5_NSGDR_UE_CreateFolder.afterSubmit', 'TRIGGER CALL TO  GOOGLE');
                    var recObj = {}
                    var recType = recordToProcess.type;
                    recObj.RecType = recType;
                    recObj.RecID = recordToProcess.getValue('id');
                    //TODO Update with proper field for each entity Type
                    var customerId;
                    var options = {};
                    var parentFolder = '';
                    switch (recType) {
                        case 'customer':
                            recObj.FolderName = recordToProcess.getValue(f5_constants.EntityField.ENTITY_TITLE);
                            recObj.ParentFolder = googleDriveConf.parentFolders.customers;
                            recObj.driveId = googleDriveConf.drives.netsuite_organisations;
                            break;
                        case 'prospect':
                            recObj.FolderName = recordToProcess.getValue(f5_constants.EntityField.ENTITY_TITLE);
                            recObj.ParentFolder = googleDriveConf.parentFolders.customers;
                            recObj.driveId = googleDriveConf.drives.netsuite_organisations;
                            break;
                        case 'customrecord_f5_oh_hremp_record':
                            var hrEmpRec = f5_hremployee.load(recordToProcess.id);
                            var empID = hrEmpRec.getId();
                            log.debug({title: 'empID', details: empID});
                            var idEmployee = recordToProcess.getValue('externalid'); // use external id
                            idEmployee = idEmployee ? idEmployee : recordToProcess.id; // if external id is
                            // unavailable, use
                            // internal id
                            recObj.FolderName = empID + ' ' + recordToProcess.getValue(f5_constants.HREmployeeField.NAME)
                            recObj.ParentFolder = googleDriveConf.parentFolders.hremployees;
                            recObj.driveId = googleDriveConf.drives.netsuite_employees;
                            break;
                        case 'job':
                            recObj.FolderName = recordToProcess.getValue(f5_constants.EntityField.ENTITY_TITLE);
                            // Search or create parent folder for projects
                            customerId = recordToProcess.getValue(f5_constants.EntityField.PARENT);
                            options.customerId = customerId;
                            options.entityType = 'Projects';
                            options.driveId = googleDriveConf.drives.netsuite_organisations;
                            if (customerId){
                                parentFolder = f5_nsgdrv.searchOrCreateParentFolder(options);
                            } else
                            {
                                // Internal Project
                                parentFolder = googleDriveConf.parentFolders.general_projects
                            }
                            recObj.ParentFolder = parentFolder;
                            break;
                        case 'vendor':
                            recObj.FolderName = recordToProcess.getValue(f5_constants.EntityField.ENTITY_TITLE);
                            recObj.ParentFolder = googleDriveConf.parentFolders.vendors;
                            recObj.driveId = googleDriveConf.drives.netsuite_organisations;
                            break;

                        case 'customrecord_oh_contract_management':
                            var contractMgmtRec = f5_contractmgmt.load(recordToProcess.id);
                            var contractID = contractMgmtRec.getId();
                            recObj.FolderName = contractID + ' ' + recordToProcess.getText(f5_constants.ContractManagementField.TITLE);
                            // Search or create parent folder for contracts
                            customerId = recordToProcess.getValue(f5_constants.ContractManagementField.BILLTOCUSTOMER);
                            if (customerId){
                                options.customerId = customerId;
                            } else
                            {
                            var  vendorId = recordToProcess.getValue(f5_constants.ContractManagementField.VENDOR)
                                if (vendorId){
                                    options.vendorId = vendorId;
                                }
                            }
                            options.entityType = 'Contracts';
                            options.driveId = googleDriveConf.drives.netsuite_organisations;
                            parentFolder = f5_nsgdrv.searchOrCreateParentFolder(options);
                            recObj.ParentFolder = parentFolder;
                            break;

                        case 'opportunity':
                            var opportunityRec = f5_opportunity.load(recordToProcess.id);
                            var opportunityID = opportunityRec.getId();
                            recObj.FolderName = opportunityID + ' ' + recordToProcess.getText(f5_constants.TransactionField.TITLE);
                            // Search or create parent folder for opportunities
                            customerId = recordToProcess.getValue(f5_constants.TransactionField.ENTITY);
                            options.customerId = customerId;
                            options.entityType = 'Opportunities';
                            options.driveId = googleDriveConf.drives.netsuite_organisations;
                            parentFolder = f5_nsgdrv.searchOrCreateParentFolder(options);
                            recObj.ParentFolder = parentFolder;
                            break;

                        default:
                            recObj.FolderName = recordToProcess.getText(f5_constants.TransactionField.ENTITY);
                    }
                    log.debug({title: 'recObj', details: JSON.stringify(recObj)});
                    var createFolderResponse = f5_nsgdrv.createFolder(recObj);
                    log.error(createFolderResponse)
                    if (!createFolderResponse.issuccess) {

                        var errorObj = error.create({
                            name: "ENT001",
                            message: createFolderResponse.message,
                            notifyOff: true
                        });
                        log.debug(errorObj.name, errorObj.message
                        );
                        throw errorObj


                    } else {

                        var createFolderResponseObj = JSON.parse(createFolderResponse.body)
                        var recValues = {}
                        switch (recType) {

                            case 'customrecord_f5_oh_hremp_record':
                                recValues[f5_constants.HREmployeeField.ISFOLDERCREATED] = true;
                                recValues[f5_constants.HREmployeeField.DATEFOLDERCREATED] = f5_shared.getUserLocalTimeDate();
                                recValues[f5_constants.HREmployeeField.GDRVFOLDERID] = createFolderResponseObj.id;
                                recValues[f5_constants.HREmployeeField.FOLDERURL] = f5_constants.GoogleDrive.URL + createFolderResponseObj.id
                                break;

                            case 'customrecord_oh_contract_management':
                                recValues[f5_constants.ContractManagementField.ISFOLDERCREATED] = true;
                                recValues[f5_constants.ContractManagementField.DATEFOLDERCREATED] = f5_shared.getUserLocalTimeDate();
                                recValues[f5_constants.ContractManagementField.GDRVFOLDERID] = createFolderResponseObj.id;
                                recValues[f5_constants.ContractManagementField.FOLDERURL] = f5_constants.GoogleDrive.URL + createFolderResponseObj.id
                                break;
                            case 'opportunity':
                                recValues[f5_constants.TransactionField.ISFOLDERCREATED] = true;
                                recValues[f5_constants.TransactionField.DATEFOLDERCREATED] = f5_shared.getUserLocalTimeDate();
                                recValues[f5_constants.TransactionField.GDRVFOLDERID] = createFolderResponseObj.id;
                                recValues[f5_constants.TransactionField.FOLDERURL] = f5_constants.GoogleDrive.URL + createFolderResponseObj.id
                                break;

                            default:
                                recValues = {
                                    custentity_f5_nsgdrv_foldercreated: true,
                                    custentity_f5_nsgdrv_dateffoldercreated: f5_shared.getUserLocalTimeDate(),
                                    custentity_f5_nsgdrv_folderid: createFolderResponseObj.id,
                                    custentity_f5_nsgdrv_folder_url: f5_constants.GoogleDrive.URL + createFolderResponseObj.id
                                }
                        }
                        var recordId = f5_record.submitFields({
                            id: recObj.RecID,
                            type: recType,
                            values: recValues
                        });
                        if (recordId) {
                            var successvalues = {
                                custparam_f5_nsgdrv_foldercreated: true,
                                custparam_f5_nsgdrv_errormessage: ''
                            }
                            f5_redirect.toRecord({
                                id: recordId,
                                type: recType,
                                isEditMode: false,
                                parameters: successvalues
                            })
                        }
                    }
                }
            }
        } catch (e) {
            log.error('afterSubmit: Unexpected Error', f5_format.exceptionToString(e));
            var errorValues = {
                custparam_f5_nsgdrv_foldercreated: false,
                custparam_f5_nsgdrv_errormessage: f5_format.exceptionToString(e)
            }
            f5_redirect.toRecord({
                id: recordId,
                type: recType,
                isEditMode: false,
                parameters: errorValues
            })
        }
    }


    return {
        beforeLoad: beforeLoad,
        afterSubmit: afterSubmit
    }
});
