/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope Public
 */
define([  './common/F5_OAuthToken'],
    /**
     * @param {error} error
     * @param {record} record
     * @param {search} search
     */
    function(f5_OauthToken) {

        /**
         * Definition of the Scheduled script trigger point.
         *
         * @param {Object} scriptContext
         * @param {string} scriptContext.type - The context in which the script is executed. It is one of the values from the scriptContext.InvocationType enum.
         * @Since 2015.2
         */
        function execute(scriptContext) {


            var currentToken = f5_OauthToken.getActiveToken()

            if (!currentToken){
                currentToken = f5_OauthToken.getLastToken();

            }
            var newToken = f5_OauthToken.refreshAccessToken(currentToken);
            log.debug({title: 'New Token', details: JSON.stringify(newToken)});

            var deletedToken = f5_OauthToken.deleteToken({id: currentToken.id})
            log.debug({title: 'Deleted Token', details: deletedToken});
        }
        return {
            execute: execute
        };

    });
