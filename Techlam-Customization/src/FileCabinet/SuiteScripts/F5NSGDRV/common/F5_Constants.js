define([
        '../common/F5_StandardConstants'
    ],
    function (f5_standard) {

        var TRANSACTION_FIELDS = f5_standard.TransactionField;
        TRANSACTION_FIELDS.CREATEFOLDER = 'custbody_f5_nsgdrv_createfolder';
        TRANSACTION_FIELDS.ISFOLDERCREATED = 'custbody_f5_nsgdrv_foldercreated';
        TRANSACTION_FIELDS.GDRVFOLDERID = 'custbody_f5_nsgdrv_folderid';
        TRANSACTION_FIELDS.DATEFOLDERCREATED = 'custbody_f5_nsgdrv_datefoldercreated';
        TRANSACTION_FIELDS.FOLDERURL = 'custbody_f5_nsgdrv_folder_url';

        var ENTITY_FIELDS = f5_standard.EntityField;
        ENTITY_FIELDS.CREATEFOLDER = 'custentity_f5_nsgdrv_createfolder';
        ENTITY_FIELDS.ISFOLDERCREATED = 'custentity_f5_nsgdrv_foldercreated';
        ENTITY_FIELDS.GDRVFOLDERID = 'custentity_f5_nsgdrv_folderid';
        ENTITY_FIELDS.DATEFOLDERCREATED = 'custentity_f5_nsgdrv_dateffoldercreated';
        ENTITY_FIELDS.FOLDERURL = 'custentity_f5_nsgdrv_folder_url';

        // Add custom roles here
        var TABS = f5_standard.Tab;
        // Add custom tabs here
        var SUBTABS = f5_standard.Subtab;
        SUBTABS.CUSTOMERSUBTAB = 'custscript_f5_nsgdrv_customersubtabid'
        SUBTABS.VENDORSUBTAB = 'custscript_f5_nsgdrv_vendorsubtabid'
        SUBTABS.EMPLOYEESUBTAB = 'custscript_f5_nsgdrv_employeesubtabid'

        return {
            Field: f5_standard.Field,
            TransactionField: TRANSACTION_FIELDS,
              EntityField: ENTITY_FIELDS,
            Join: f5_standard.Join,
            CheckBox: f5_standard.CheckBox,
            Feature: f5_standard.Feature,
            Preference: f5_standard.Preference,
            User: f5_standard.User,
            Role: f5_standard.Role,
            Mode: f5_standard.Mode,
            ButtonId: f5_standard.ButtonId,
            Subtab: SUBTABS,
            Tab: TABS,
            CustomRecordType: {
                 GOOGLETOKENS: 'customrecord_f5_nsgdrv_googletokens',
                 HREMPLOYEERECORD: 'customrecord_f5_oh_hremp_record',
                 CONTRACTMANAGEMENT: 'customrecord_oh_contract_management'
            },
            CustomList: {
                // BAR: 'customlist_f5_bar'
            },
            ScriptParameter: {
                 GOOGLEDRIVECONF: 'custscript_f5_nsgdrv_google_config'
            },
            ScriptId: {
                SLDISPLAYERROR: 'customscript_f5_gdrv_sl_display_error',
            },
            DeploymentId: {
                 SLDISPLAYERROR: 'customdeploy_f5_gdrv_sl_display_error',
            },
            FormulaTemplate: {
                CASE_IN_STRINGS: "CASE WHEN {{name}} IN ('{values}') THEN 1 ELSE 0 END",
                CASE_IN_INTEGERS: "CASE WHEN {{name}} IN ({values}) THEN 1 ELSE 0 END"
            },
          /*  SANDBOX
          GoogleDrive:{
              URL:'https://drive.google.com/drive/folders/',
              authURL: "https://accounts.google.com/o/oauth2/v2/auth",
              tokenURL : "https://oauth2.googleapis.com/token",
              integrationClient :'804185333952-nai7qls2j2tlbmr1hv1olo6krf8seahm.apps.googleusercontent.com',
              integrationSecret :'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',
              redirect_uri: 'https://4509608-sb1.extforms.netsuite.com/app/site/hosting/scriptlet.nl?script=1053',
              refreshToken : '1//0grDDplXZ-tqvCgYIARAAGBASNwF-L9IruaMOndOyxPwvJHsubKMzQEqcjOeEyGfnYujRN0pXfMtdqB9nx7TBSoXE-8ewWeO7TLM',
              googleDriveUrL : 'https://drive.google.com/drive/folders/',
              authCode : '4/0AX4XfWgjaAe51jFNxyz2pQdkhKqtcPxZkAXaQ1fSv6rnU1-IUvrLExZ1aeEoE8yh6At2Gg',
              scope: 'https://www.googleapis.com/auth/drive',
              prompt: 'consent',
              responseType: 'code',
                accessType : 'offline'
            },*/
            GoogleDrive:{
                URL:'https://drive.google.com/drive/folders/',
                authURL: "https://accounts.google.com/o/oauth2/v2/auth",
                tokenURL : "https://oauth2.googleapis.com/token",
                integrationClient :'705851179688-b1mgvs57f8igas7995a2so366mp2l27c.apps.googleusercontent.com',
                integrationSecret :'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',
                redirect_uri: 'https://4509608.extforms.netsuite.com/app/site/hosting/scriptlet.nl?script=1499',
                refreshToken : '1//0grDDplXZ-tqvCgYIARAAGBASNwF-L9IruaMOndOyxPwvJHsubKMzQEqcjOeEyGfnYujRN0pXfMtdqB9nx7TBSoXE-8ewWeO7TLM',
                googleDriveUrL : 'https://drive.google.com/drive/folders/',
                authCode : '4/0AVHEtk7fnIckgbjCTwZ5lIAWxxfF4VmipiMnqiTgp0LfYsT6CIrdr5NXKrsiWFMk0BhyMQ',
                scope: 'https://www.googleapis.com/auth/drive',
                prompt: 'consent',
                responseType: 'code',
                accessType : 'offline'
            },
            ContractManagementField: {
                ID: 'name',
                TITLE: 'altname',
                PARENTCONTRACT: 'parent',
                ENDUSERCUSTOMER: 'custrecord_oh_cm_endusercustomer',
                BILLTOCUSTOMER: 'custrecord_oh_cm_billtocustomer',
                CREATEFOLDER :'custrecord_f5_nsgdrv_cm_createfolder',
                ISFOLDERCREATED : 'custrecord_f5_nsgdrv_cm_foldercreated',
                GDRVFOLDERID : 'custrecord_f5_nsgdrv_cm_folderid',
                DATEFOLDERCREATED : 'custrecord_f5_nsgdrv_cm_datefoldcreated',
                FOLDERURL : 'custrecord_f5_nsgdrv_cm_folder_url',
                VENDOR: 'custrecord_oh_cm_vendor'
            },
            HREmployeeField: {
                ID: 'name',
                NAME : 'altname',
                CREATEFOLDER :'custrecord_f5_nsgdrv_he_createfolder',
                ISFOLDERCREATED : 'custrecord_f5_nsgdrv_he_foldercreated',
                GDRVFOLDERID : 'custrecord_f5_nsgdrv_he_folderid',
                DATEFOLDERCREATED : 'custrecord_f5_nsgdrv_he_datefoldcreated',
                FOLDERURL : 'custrecord_f5_nsgdrv_he_folder_url'
            }
        }
    });