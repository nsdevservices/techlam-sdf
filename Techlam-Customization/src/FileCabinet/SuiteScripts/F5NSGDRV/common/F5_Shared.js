define([
        './F5_Constants',
        './F5_Query',
        'N/config',
        'N/format'
    ],
    function (f5_constants, f5_query, config, format) {
        return {


            // Gets the TimeZone for the current user
            getUserTimeZone: function () {

                var configRecObj = config.load(
                    {
                        type: config.Type.USER_PREFERENCES
                    });
                return configRecObj.getValue('TIMEZONE');
            },
            // Gets the current date time for the user based on his preference
            getUserLocalTime: function () {
                var myDate = new Date();
                var userTimeZone = this.getUserTimeZone();
                return format.format({
                    value: myDate,
                    type: format.Type.DATETIMETZ,
                    timezone: userTimeZone
                });
            },
            getUserLocalTimeDate: function () {
                var myDate = new Date();
                var userTimeZone = this.getUserTimeZone();
                return format.parse({
                    value: myDate,
                    type: format.Type.DATETIME,
                    timezone:  format.Timezone.PACIFIC_AUCKLAND
                });
            },
            getUserLocalTimeISO: function () {
                var myDate = new Date();
                var userTimeZone = this.getUserTimeZone();
                var now = format.parse({
                    value: myDate,
                    type: format.Type.DATE,
                    timezone: userTimeZone
                });
                return now.toISOString()
            },
        };
    });
