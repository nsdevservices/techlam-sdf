define([
        './F5_Constants',
        './F5_Shared',
        './F5_Search',
        './F5_Record',
        './F5_Format',
        './F5_Https',
        './F5_Query',
        'N/format',
        'N/error',
        '../lib/moment'
    ],
    function (f5_constants, f5_shared, f5_search, f5_record, f5_format, f5_https, f5_query, format, error, moment) {
        var TYPE = f5_search.CustomType.GOOGLETOKENS;
        var FIELDS = {
            ID: 'id',
            INTERNALID: 'internalid',
            ACCESTOKEN: 'custrecord_f5_gt_access_token',
            EXPIRES: 'custrecord_f5_gt_expires',
            REFRESH_TOKEN: 'custrecord_f5_gt_refresh_token',
            IS_INACTIVE: 'isinactive'
        };

        function GToken(record) {
            var me = this;
            // inherit from Transaction
            f5_record.Record.apply(this, [{record: record}]);

            this.getAccessToken = function getAccessToken() {
                return me.getValue({fieldId: FIELDS.ACCESTOKEN});
            };
            this.getExpires = function getExpires() {
                return me.getValue({fieldId: FIELDS.EXPIRES});
            };
            this.setAccessToken = function setAccessToken(value) {
                me.setValue({fieldId: FIELDS.ACCESTOKEN, value: value});
            };
            this.setExpires = function setUser(value) {
                me.setValue({fieldId: FIELDS.EXPIRES, value: value});
            };
            this.setRefreshToken = function setRefreshToken(value) {
                me.setValue({fieldId: FIELDS.REFRESH_TOKEN, value: value});
            };
            this.setInactive = function setInactive(value) {
                me.setValue({fieldId: FIELDS.IS_INACTIVE, value: value});
            };

        }

        return {
            TYPE: TYPE,
            FIELDS: FIELDS,
            cast: function cast(record) {
                return new GToken(record);
            },
            load: function load(options) {
                options.type = TYPE;
                return new GToken(f5_record.load(options));
            },
            create: function create(options) {
                options.type = TYPE;
                return new GToken(f5_record.create(options));
            },
            delete: function deleterec(options) {
                options.type = TYPE;
                return new GToken(f5_record.delete(options));
            },

            getActiveToken: function getActiveToken(){
                var nowLocal = f5_shared.getUserLocalTime().toString()
                log.debug({title: 'nowLocal', details: nowLocal});
                var nowTrimmed = nowLocal.substr(0,nowLocal.length-6)
                var dateMoment = moment().format('DD/MM/YYYY hh:mm');
                log.debug({title: 'dateMoment', details: dateMoment});
                log.debug({title: 'nowTrimmed', details: nowTrimmed});

                var customrecord_f5_google_tokensSearchObj = f5_search.create({
                    type: f5_constants.CustomRecordType.GOOGLETOKENS,
                    filters:
                        [
                            ["custrecord_f5_gt_expires","onorafter",dateMoment],
                            "AND",
                            [FIELDS.IS_INACTIVE, "is", "F"]
                        ],
                    columns:
                        [
                            f5_search.createColumn({name: "internalid", sort: f5_search.Sort.DESC,label: "Internal ID"}),
                            f5_search.createColumn({name: FIELDS.ACCESTOKEN, label: "Access Token"}),
                            f5_search.createColumn({name: FIELDS.REFRESH_TOKEN, label: "Refresh Token"}),
                            f5_search.createColumn({name: FIELDS.EXPIRES, label: "Expires"})
                        ]
                });
                var tokenAccess = []
                var searchResultCount = customrecord_f5_google_tokensSearchObj.runPaged().count;
                log.debug("customrecord_f5_google_tokensSearchObj result count",searchResultCount);
                customrecord_f5_google_tokensSearchObj.run().each(function(result){
                            tokenAccess.push({
                                id:result.id,
                                accessToken: result.getValue(FIELDS.ACCESTOKEN),
                                refreshToken: result.getValue(FIELDS.REFRESH_TOKEN),
                                expires: result.getValue(FIELDS.EXPIRES)
                            })
                            log.debug({title: 'tokenAccess', details: tokenAccess })
                    return true;
                });

                if (tokenAccess.length> 0) {
                    return tokenAccess[0]
                }
                    else {
                    return ''
                }
            },

            getLastToken : function getLastToken(){
                var suiteQL = "SELECT TOP 1 " +
                    FIELDS.ID + ", " +
                    FIELDS.ACCESTOKEN + " as access_token, " +
                    FIELDS.REFRESH_TOKEN + " as refresh_token " +
                    "FROM " +
                    f5_constants.CustomRecordType.GOOGLETOKENS +
                    " ORDER BY " +  FIELDS.ID + " DESC"

                var results = f5_query.runSuiteQL({
                    query: suiteQL
                }).asMappedResults()
                log.debug({title: 'Last token', details: JSON.stringify(results[0])});
                return results[0]
            },

            refreshAccessToken: function refreshAccessToken(currentToken) {

                var lastToken = this.getLastToken();

                var requestBody = {}
                requestBody.client_id = f5_constants.GoogleDrive.integrationClient;
                requestBody.client_secret = f5_constants.GoogleDrive.integrationSecret;
                requestBody.grant_type = "refresh_token"
                requestBody.refresh_token = lastToken.refresh_token
                requestBody.redirect_uri = f5_constants.GoogleDrive.redirect_uri

                var refreshUrl = f5_constants.GoogleDrive.tokenURL
                  log.error({title: 'refreshURL', details: refreshUrl})
                   var response = f5_https.post({
                       url:  refreshUrl,
                       headers: {Authorization: f5_https.getBasicAuth({clientId: f5_constants.GoogleDrive.integrationClient, clientSecret: f5_constants.GoogleDrive.integrationSecret }), 'Content-Type': 'application/x-www-form-urlencoded'},
                       body : requestBody
                   })
                   log.debug({title: 'afterSubmit', details: 'Token response ' + response.code + ' : ' + response.body})
                   if (response.code === 200) {
                       var newToken = {}
                       var responseData = JSON.parse(response.body)
                       newToken.access_token = responseData.access_token;
                       newToken.expires_in = responseData.expires_in;
                       newToken.refresh_token = lastToken.refresh_token;

                       var tokenId = this.storeToken(newToken)

                       log.debug({title: 'refreshAccessToken', details: 'tokenId=' + tokenId})
                       return newToken

                   } else {
                       throw error.create({
                           name: 'RefreshTokenError',
                           message: 'There was a problem refreshing the token. Call F5 Support.',
                           notifyOff: false
                       })
                   }
            },

            storeToken: function storeToken(options) {
                var timeObject = new Date();
                timeObject.setSeconds(timeObject.getSeconds() + options.expires_in)
                var userTimeZone = f5_shared.getUserTimeZone();
              var expires = format.parse({
                    value: timeObject,
                    type: f5_format.Type.DATETIME,
                    timezone: userTimeZone
                });
                var newToken = this.create({type: f5_search.CustomType.GOOGLETOKENS});
                newToken.setAccessToken(options.access_token);
                newToken.setExpires(expires);
                newToken.setRefreshToken(options.refresh_token);
                return newToken.save();
            },

            deleteToken: function deleteToken(options) {
                return this.delete(options);
            },
        };
    });