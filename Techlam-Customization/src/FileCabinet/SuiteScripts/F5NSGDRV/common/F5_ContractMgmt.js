define([
        './F5_Record',
        './F5_Search',
        './F5_Constants'
    ],
    function (f5_record, f5_search, f5_constants) {
        var TYPE = f5_search.CustomType.CONTRACTMANAGEMENT;
        var FIELDS = {
            INTERNALID: 'internalid',
            ID: 'name',
            TITLE: 'altname',
            CREATEGDRVFOLDER: 'custrecord_f5_nsgdrv_cm_createfolder',
            DATEFOLDERCREATED: 'custrecord_f5_nsgdrv_cm_datefoldcreated',
            FOLDERURL: 'custrecord_f5_nsgdrv_cm_folder_url',
            GDRVFOLDERCREATED: 'custrecord_f5_nsgdrv_cm_foldercreated',
            GDRVFOLDERID: 'custrecord_f5_nsgdrv_cm_folderid'
        };

        function ContractManagement(record) {
            var me = this;
            // inherit from Record
            f5_record.Record.apply(this, [{record: record}]);

            this.getId = function getId(){
                return me.getValue({fieldId: FIELDS.ID});
            };

            this.isGoogleDriveFolderCreated = function isGoogleDriveFolderCreated() {
                return me.getValue({fieldId: FIELDS.GDRVFOLDERCREATED});
            };

            this.createGoogleDriveFolder = function createGoggleDrive() {
                return me.getValue({fieldId: FIELDS.CREATEGDRVFOLDER});
            };


        };

        return {
            TYPE: TYPE,
            cast: function cast(record) {
                return new ContractManagement(record);
            },
            create: function create(options) {
                return new ContractManagement(f5_record.create(options));
            },
            load: function load(id) {
                return new ContractManagement(f5_record.load({type: TYPE, id: id}));
            }
        };
    });