define([
        './F5_Record',
        './F5_Search',
        './F5_Constants'
    ],
    function (f5_record, f5_search, f5_constants) {
        var TYPE = f5_search.Type.JOB;
        var FIELDS = f5_constants.EntityField;

        function Project(record) {
            var me = this;
            // inherit from Record
            f5_record.Record.apply(this, [{record: record}]);

            this.getSubsidiary = function getSubsidiary() {
                return me.getValue({fieldId: FIELDS.SUBSIDIARY});
            };

            this.getId = function getId(){
                return me.getValue({fieldId: 'name'});
            };
        }

        return {
            TYPE: TYPE,
            cast: function cast(record) {
                return new Project(record);
            },
            create: function create(options) {
                return new Project(f5_record.create(options));
            },
            load: function load(id) {
                return new Project(f5_record.load({type: TYPE, id : id}));
            },
            getGDrvInfo: function gdrvInfo(options){
               return f5_search.lookupFields({
                    type: f5_search.Type.JOB,
                    id: options.id,
                    columns: [f5_constants.EntityField.ISFOLDERCREATED, f5_constants.EntityField.GDRVFOLDERID]
                })
            }
        };
    });