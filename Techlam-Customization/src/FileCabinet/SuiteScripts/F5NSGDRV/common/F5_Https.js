define([
        'N/https', 'N/encode'
    ],
    function (https, encode) {
        return {
            get: function get(options) {
                return https.get(options);
            },
            post: function post(options) {
                return https.post(options);
            },
            getServerRequest: function getServerRequest() {
                return https.getServerRequest();
            },
            getBasicAuth: function getBasicAuth(options) {
                var integrationClient = options.clientId;
                var integrationSecret = options.clientSecret;
                var base64Credentials = encode.convert({
                    string: integrationClient + ':' + integrationSecret,
                    inputEncoding: encode.Encoding.UTF_8,
                    outputEncoding: encode.Encoding.BASE_64
                })
                log.debug({title: 'getBasicAuthentication', details: base64Credentials});
                return 'Basic ' + base64Credentials
            }

        };
    });