define([
        './F5_Record',
        './F5_Search',
        './F5_Constants'
    ],
    function (f5_record, f5_search, f5_constants) {
        var TYPE = f5_search.CustomType.HREMPLOYEERECORD;
        var FIELDS = {
            ID: 'name',
            NAME : 'altname',
            CREATEFOLDER :'custrecord_f5_nsgdrv_he_createfolder',
            ISFOLDERCREATED : 'custrecord_f5_nsgdrv_he_foldercreated',
            GDRVFOLDERID : 'custrecord_f5_nsgdrv_he_folderid',
            DATEFOLDERCREATED : 'custrecord_f5_nsgdrv_he_datefoldcreated',
            FOLDERURL : 'custrecord_f5_nsgdrv_he_folder_url'
        };

        function HREmployee(record) {
            var me = this;
            // inherit from Record
            f5_record.Record.apply(this, [{record: record}]);

            this.getId = function getId(){
                return me.getValue({fieldId: FIELDS.ID});
            };

            this.isGoogleDriveFolderCreated = function isGoogleDriveFolderCreated() {
                return me.getValue({fieldId: FIELDS.GDRVFOLDERCREATED});
            };

            this.createGoogleDriveFolder = function createGoggleDrive() {
                return me.getValue({fieldId: FIELDS.CREATEGDRVFOLDER});
            };

            this.setFolderCreated = function setFolderCreated(value) {
                me.setValue({
                    fieldId: FIELDS.ISFOLDERCREATED,
                    value: value
                });
            };

            this.setGDriveFolderId = function setGDriveFolderId(value) {
                me.setValue({
                    fieldId: FIELDS.GDRVFOLDERID,
                    value: value
                });
            };
            this.setDateFolderCreated = function setDateFolderCreated(value) {
                me.setValue({
                    fieldId: FIELDS.DATEFOLDERCREATED,
                    value: value
                });
            };
            this.setFolderURL = function setFolderURL(value) {
                me.setValue({
                    fieldId: FIELDS.FOLDERURL,
                    value: value
                });
            }

        }

        return {
            TYPE: TYPE,
            cast: function cast(record) {
                return new HREmployee(record);
            },
            create: function create(options) {
                return new HREmployee(f5_record.create(options));
            },
            load: function load(id) {
                return new HREmployee(f5_record.load({type: TYPE, id: id}));
            }

        };
    });