define([
        'N/query'
    ],
    function (query) {
        return {
            create: function create(options) {
                return query.create(options);
            },
            load: function load(options) {
                return query.load(options);
            },
            createPeriod: function createPeriod(options) {
                return query.createPeriod(options);
            },
            runSuiteQL: function runSuiteQL(options) {
                return query.runSuiteQL(options)
            },
            runSuiteQLPaged: function runSuiteQLPaged(options) {
                return query.runSuiteQLPaged(options)
            }

        };
    });