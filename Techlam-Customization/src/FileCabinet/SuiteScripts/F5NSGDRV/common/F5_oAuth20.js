/**
 * @NApiVersion 2.x
 *
 * @NModuleScope Public
 */

define(['./F5_Search', './/F5_Https','N/config', './/F5_Format',  'N/error','.//F5_Runtime'],
		/**
		 * @param {search} f5_search
		 * @param {record} f5_record
		 * @param {https} f5_https
		 * @param {config} config
		 * @param {error} error
		 * @param {runtime} f5_runtime
		 * @param {format} format
		 */
		function(f5_search, f5_https, config, format, f5_record, error, f5_runtime) {

	return  {
        
        // Get the Subsidiary for either customer or vendor. The string 'customer' or 'vendor' shoudl be passed ti the function
		refreshToken: function (entityObj, entityType){



		},

		getToken : function(){

			var token = ''




			return token

		},

		isTokenExpired : function () {



		},
        
        calculateCustomerSiteURL : function(customerid)
		{
			var scriptObj = f5_runtime.getCurrentScript();
			var sharepointBaseURL = scriptObj.getParameter({name: 'custscript_f5_nsshp_sharepointbaseurl'});
		//	var url = 'https://circle88.sharepoint.com/sites/NetSuite-Site/' + customerid
			var url = sharepointBaseURL + customerid
			return url
		},

		calculateProjectSiteURL : function(customerid, projectid) {
			var scriptObj = f5_runtime.getCurrentScript();
			var sharepointBaseURL = scriptObj.getParameter({name: 'custscript_f5_nsshp_sharepointbaseurl'});
			return sharepointBaseURL  + customerid + '/' + projectid
		},
		
        // Retrieve the name name of a record in a customlist or customrecord. Returns both the internal id and name.
		getListValueIdAndName: function ( keyfieldvalues, tablename){
			var list = [];
			for (var i = 0; keyfieldvalues != null && i < keyfieldvalues.length; i++) 
			{
				var listSearch = f5_search.lookupFields({
					type: tablename,
					id: keyfieldvalues[i],
					columns: ['name'],
					isDynamic: false,
				});

				list.push(
						{ 
							internalid : keyfieldvalues[i],
							name : listSearch.name
						});
			}
			return list ;
		},
		// Gets the TimeZone for the current user
		getUserTimeZone: function(){

			var configRecObj = config.load(
					{
						type: config.Type.USER_PREFERENCES
					});
			var timezonefld = configRecObj.getValue('TIMEZONE');
			return timezonefld;
		},
		// Gets the current date time for the user based on his preference
		getUserLocalTime: function()
		{
			var mydate = new Date();
			var usertimezone = this.getUserTimeZone();
			var localDateTime = format.format({
				value:mydate,
				type: format.Type.DATETIMETZ,
				timezone : usertimezone
			});
			return localDateTime;
		},

		
		

		

		f5parseQueryString: function( queryString ) {
			var params = {}, queries, temp, i, l;

			// Split into key/value pairs
			queries = queryString.split("&");

			// Convert the array of strings into an object
			for ( i = 0, l = queries.length; i < l; i++ ) {
				temp = queries[i].split('=');
				params[temp[0]] = temp[1];
			}

			return params;
		},

		// Action expects create or update 
		
		GetIndexByValue : function (arrayName, value)
		{  
			var keyName = "";
			var index = -1;
			for (var i = 0; i < arrayName.length; i++) { 
				var obj = arrayName[i]; 
				for (var key in obj) {          
					if (obj[key] == value) { 
						keyName = key; 
						index = i;
					} 
				} 
			}
			//console.log(index); 
			return index;
		},


		sortFunc : function (a, b) {
			if (a.customerid < b.customerid)  return -1;
			else if (a.customerid > b.customerid)  return 1;
			else {
				if (a.itemid < b.itemid)  return -1;
				else if (a.itemid > b.itemid)  return 1;
				/*     else {
		            if (a.itemid < b.itemid) return -1;
		            else if (a.itemdid > b.itemid) return -1;
		        }*/
				return 0;
			}
		},
//		Use example		
//		homes.sort(fieldSorter(['city', '-price']));
		fieldSorter : function (fields) {
			return function (a, b) {
				return fields
				.map(function (o) {
					var dir = 1;
					if (o[0] === '-') {
						dir = -1;
						o=o.substring(1);
					}
					if (a[o] > b[o]) return dir;
					if (a[o] < b[o]) return -(dir);
					return 0;
				})
				.reduce(function firstNonZeroValue (p,n) {
					return p ? p : n;
				}, 0);
			};
		},


		indexOfObjectArray : function(array, keyvalues) {
			for (var i = 0; i < array.length; i++) {
				var trueCount = 0;
				for (var j = 0; j < keyvalues.length; j++) {
					if (array[i][keyvalues[j]['key']] == keyvalues[j]['value']) {
						trueCount++;
					}
				}
				if (trueCount === keyvalues.length) return i;
			}
			return -1; },

			indexOfObjectArray2 : function  (array, keyvalues) {
				var myarray = []
				for (var i = 0; i < array.length; i++) {
					var trueCount = 0;
					for (var j = 0; j < keyvalues.length; j++) {
						if (array[i][keyvalues[j]['key']] == keyvalues[j]['value']) {

							trueCount++;
						}
					}
					if (trueCount == keyvalues.length) myarray.push(i)
				}
				if (myarray.length > 0) return myarray
				else
					return -1;
         }

        

	}
});



