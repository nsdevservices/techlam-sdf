define([
        './F5_Record',
        './F5_Search',
        './F5_Constants'
    ],
    function (f5_record, f5_search, f5_constants) {
        var TYPE = f5_search.Type.OPPORTUNITY;
        var FIELDS = {
            INTERNALID: 'internalid',
            ID: 'tranid',
            TITLE: 'title',
            CREATEGDRVFOLDER: 'custbody_f5_nsgdrv_createfolder',
            DATEFOLDERCREATED: 'custbody_f5_nsgdrv_datefoldercreated',
            FOLDERURL: 'custbody_f5_nsgdrv_folder_url',
            GDRVFOLDERCREATED: 'custbody_f5_nsgdrv_foldercreated',
            GDRVFOLDERID: 'custbody_f5_nsgdrv_folderidd'
        };

        function Opportunity(record) {
            var me = this;
            // inherit from Record
            f5_record.Record.apply(this, [{record: record}]);

            this.getId = function getId(){
                return me.getValue({fieldId: FIELDS.ID});
            };

            this.isGoogleDriveFolderCreated = function isGoogleDriveFolderCreated() {
                return me.getValue({fieldId: FIELDS.GDRVFOLDERCREATED});
            };

            this.createGoogleDriveFolder = function createGoggleDrive() {
                return me.getValue({fieldId: FIELDS.CREATEGDRVFOLDER});
            };


        };

        return {
            TYPE: TYPE,
            cast: function cast(record) {
                return new Opportunity(record);
            },
            create: function create(options) {
                return new Opportunity(f5_record.create(options));
            },
            load: function load(id) {
                return new Opportunity(f5_record.load({type: TYPE, id: id}));
            }
        };
    });