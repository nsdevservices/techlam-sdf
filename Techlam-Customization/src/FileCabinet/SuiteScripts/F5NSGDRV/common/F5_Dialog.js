define([
        'N/ui/dialog'
    ],
    function (dialog) {
        return {
            create: function create(options) {
                return dialog.create(options);
            }
        };
    });