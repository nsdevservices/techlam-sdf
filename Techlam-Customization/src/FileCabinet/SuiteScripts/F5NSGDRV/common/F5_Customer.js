define([
        './F5_Record',
        './F5_Search',
        './F5_Constants'
    ],
    function (f5_record, f5_search, f5_constants) {
        var TYPE = f5_search.Type.CUSTOMER;
        var FIELDS = f5_constants.EntityField;

        function Customer(record) {
            var me = this;
            // inherit from Record
            f5_record.Record.apply(this, [{record: record}]);

            this.getSubsidiary = function getSubsidiary() {
                return me.getValue({fieldId: FIELDS.SUBSIDIARY});
            };
        }

        return {
            TYPE: TYPE,
            cast: function cast(record) {
                return new Customer(record);
            },
            create: function create(options) {
                return new Customer(f5_record.create(options));
            },
            load: function load(options) {
                return new Customer(f5_record.load(options));
            },
            getGDrvInfo: function gdrvInfo(options){
               return f5_search.lookupFields({
                    type: f5_search.Type.CUSTOMER,
                    id: options.id,
                    columns: [f5_constants.EntityField.ISFOLDERCREATED, f5_constants.EntityField.GDRVFOLDERID]
                })
            }
        };
    });