/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 */
define(['N/runtime', './common/F5_Url', './common/F5_Https', './common/F5_Constants'],
    
    function (runtime, f5_url, f5_https, f5_constants) {
        /**
         * Defines the Suitelet script trigger point.
         * @param {Object} scriptContext
         * @param {ServerRequest} scriptContext.request - Incoming request
         * @param {ServerResponse} scriptContext.response - Suitelet response
         * @since 2015.2
         */
        function  onRequest (scriptContext)  {

            if (scriptContext.request.method === 'GET') {

                var scriptObj = runtime.getCurrentScript()
                var sessionObj = runtime.getCurrentSession();
                var redirect_uri = f5_url.resolveScript({
                    scriptId: scriptObj.id ,
                    deploymentId: scriptObj.deploymentId ,
                    returnExternalUrl: true
                });
                log.debug({title: 'redirect_uri', details: redirect_uri});
                var requestBody = 'code=' + encodeURIComponent(f5_constants.GoogleDrive.authCode) + '&redirect_uri='+ encodeURIComponent(redirect_uri) + '&client_id=' + f5_constants.GoogleDrive.integrationClient + '&client_secret=' + f5_constants.GoogleDrive.integrationSecret + '&scope=&grant_type=authorization_code'
                log.debug({title: 'requestBody', details: requestBody})
                // Send Authorization Code to get Access Token
                var response = f5_https.post({
                    url: f5_constants.GoogleDrive.tokenURL,
                    headers: {Authorization: f5_https.getBasicAuth({clientId: f5_constants.GoogleDrive.integrationClient, clientSecret: f5_constants.GoogleDrive.integrationSecret }), 'content-type': 'application/x-www-form-urlencoded'},
                    body: requestBody
                })
                log.debug({title: 'afterSubmit', details: 'Token response ' + response.code + ' : ' + response.body})
                if (response.code === 200) {
                    var responseData = JSON.parse(response.body)
                   // var newAccessToken = responseData.access_token;
                   // var tokenId = this.storeToken(responseData)
                    log.debug({title: 'refreshAccessToken', details: 'tokenId=' + JSON.stringify(responseData)})
                    return

                } else {
                    throw error.create({
                        name: 'exchAuthTokenError',
                        message: 'There was a problem exchanging the Authorisation token. Call F5 Support.',
                        notifyOff: false
                    })
                }

                

                var j = 1
            }


        }

        return {
            onRequest: onRequest
        };

    });
