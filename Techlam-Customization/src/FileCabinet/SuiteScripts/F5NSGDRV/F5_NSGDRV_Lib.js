/**
 * @NApiVersion 2.0
 *
 * @NModuleScope Public
 */

define(['N/encode', './common/F5_Constants', './common/F5_Https', './common/F5_Search', './common/F5_Script', './common/F5_Format', './common/F5_Record', 'N/error', './common/F5_Runtime', './common/F5_OAuthToken', './common/F5_Customer', './common/F5_Vendor',],

    function (encode, f5_constants, f5_https, f5_search, f5_script, f5_format, f5_record, error, f5_runtime, f5_gtoken, f5_customer, f5_vendor) {

        return {

            getToken: function (options) {

                var auth = f5_https.getBasicAuth({
                    clientId: f5_constants.GoogleDrive.integrationClient,
                    clientSecret: f5_constants.GoogleDrive.integrationSecret,
                });
                var activeToken = f5_gtoken.getActiveToken()

                if (!activeToken) {
                    activeToken = f5_gtoken.refreshAccessToken()

                }
                return activeToken
            },


            searchFolder: function (options) {
                try {
                    var activeToken = this.getToken()

                    var parentFolderId = options.parentFolderId;
                    var entityType = options.entityType;
                    var folderName = options.folderName;
                    var driveId = options.driveId
                    var url = encodeURI("https://www.googleapis.com/drive/v3/files?q=mimeType=") + "%27"+ "application/vnd.google-apps.folder" + "%27" +  encodeURI(" and ") + "%27" + parentFolderId + "%27" + encodeURI(" in parents and trashed=false and name contains ") +  "%27" + encodeURIComponent(folderName)  +  "%27" + encodeURI("&driveId="+ driveId + "&includeItemsFromAllDrives=true&corpora=drive&supportsAllDrives=true")

                    log.debug({title: 'url', details: url});
                    var getResponse = f5_https.get({
                        url: url,
                        headers: {Authorization: 'Bearer ' + activeToken.accessToken},
                    })
                    if (getResponse.code === 200) {
                        var responseBody = JSON.parse(getResponse.body)
                        if (responseBody.files.length > 0) {

                            return responseBody.files[0].id
                        } else
                            return null

                    } else {
                        log.error({title: 'getResponse', details: JSON.stringify(getResponse)})
                    }
                } catch (e) {
                    log.error('afterSubmit: Unexpected Error', f5_format.exceptionToString(e));
                    errorObj.message = f5_format.exceptionToString(e)

                }
            },


            createFolder: function (entityObj) {
                log.debug({title: 'function createFolder'})
                var activeToken = f5_gtoken.getLastToken();
                log.debug({title: 'activeToken.access_token', details: activeToken.access_token});
                var body = {
                    title: entityObj.FolderName,
                    parents: [{"id": entityObj.ParentFolder}],
                    mimeType: 'application/vnd.google-apps.folder'
                }

                var postResponse = f5_https.post({
                    url: 'https://www.googleapis.com/drive/v2/files?supportsAllDrives=true',
                    headers: {Authorization: 'Bearer ' + activeToken.access_token, 'Content-Type': 'application/json'},
                    body: JSON.stringify(body)
                })

                log.debug({title: 'afterSubmit', details: 'POST Response: ' + postResponse.body})

                if (postResponse.code !== 200) {
                    if (postResponse.code === 404)

                        return {
                            issuccess: false,
                            message: 'There was an issue with the API call to Google Drive',
                            errordetail: null
                        };
                    else if (postResponse.code === 202)

                        return {
                            issuccess: true, message: 'The request was initiated correctly, but has not yet completed',
                            errordetail: ''
                        };
                    else

                        return {
                            issuccess: false,
                            message: 'There was an interface problem',
                            errorcode: postResponse.code,
                            errordetail: postResponse.body
                        };

                } else
                    return {
                        issuccess: true,
                        body: postResponse.body,
                        errorcode: null,
                        errordetail: null
                    };

            }

            ,

            isCreateFolderChecked: function (scriptContext) {

                // If Record Type is Entity
                if (scriptContext.newRecord.type === 'customer'
                    || scriptContext.newRecord.type === 'job'
                    || scriptContext.newRecord.type ==='prospect'
                    || scriptContext.newRecord.type === 'vendor') {

                    return scriptContext.newRecord
                        .getValue(f5_constants.EntityField.CREATEFOLDER);

                    // If Record Type is Transaction
                } else if (scriptContext.newRecord.type === 'opportunity'
                    /* || scriptContext.newRecord.type === 'estimate'
                      || scriptContext.newRecord.type === 'salesorder'
                      || scriptContext.newRecord.type === 'purchaseorder'*/) {

                    return scriptContext.newRecord
                        .getValue(f5_constants.TransactionField.CREATEFOLDER);

                    // If Record Type is Contracts
                } else if (scriptContext.newRecord.type === f5_constants.CustomRecordType.CONTRACTMANAGEMENT) {

                    return scriptContext.newRecord
                        .getValue('custrecord_f5_nsgdrv_cm_createfolder');

                    // If Record Type is HR Employee Record
                } else if (scriptContext.newRecord.type === f5_constants.CustomRecordType.HREMPLOYEERECORD) {

                    return scriptContext.newRecord
                        .getValue('custrecord_f5_nsgdrv_he_createfolder');
                }

                return false;
            }
            ,

            isFolderCreatedChecked: function (scriptContext) {

                // If Record Type is Entity
                if (scriptContext.newRecord.type === 'customer'
                    || scriptContext.newRecord.type === 'job'
                    //   || scriptContext.newRecord.type ==='prospect'
                    || scriptContext.newRecord.type === 'vendor') {

                    return scriptContext.newRecord
                        .getValue(f5_constants.EntityField.ISFOLDERCREATED);

                    // If Record Type is Transaction
                } else if (scriptContext.newRecord.type === 'opportunity'
                    /* || scriptContext.newRecord.type === 'estimate'
                      || scriptContext.newRecord.type === 'salesorder'
                      || scriptContext.newRecord.type === 'purchaseorder'*/) {

                    return scriptContext.newRecord
                        .getValue(f5_constants.TransactionField.ISFOLDERCREATED);

                    // If Record Type is Contracts
                } else if (scriptContext.newRecord.type === f5_constants.CustomRecordType.CONTRACTMANAGEMENT) {

                    return scriptContext.newRecord
                        .getValue(f5_constants.ContractManagementField.ISFOLDERCREATED);

                    // If Record Type is HR Employee Record
                } else if (scriptContext.newRecord.type === f5_constants.CustomRecordType.HREMPLOYEERECORD) {

                    return scriptContext.newRecord.getValue(f5_constants.HREmployeeField.ISFOLDERCREATED);
                }

                return false;
            },

            searchOrCreateParentFolder: function (options) {
                if (options.hasOwnProperty('customerId')){
                    var parentGoogleDriveInfo = f5_customer.getGDrvInfo({id: options.customerId})
                }
                if (options.hasOwnProperty('vendorId')){
                     parentGoogleDriveInfo = f5_vendor.getGDrvInfo({id: options.vendorId})
                }
                if (parentGoogleDriveInfo.custentity_f5_nsgdrv_foldercreated) {
                    // Searches for Contracts/Projects Folder for specific Customer
                    var recordFolderObj = {}
                    if (options.hasOwnProperty('customerId')) {
                        var customerInfo = f5_search.lookupFields({
                            type: 'customer',
                            id: options.customerId,
                            columns: [f5_constants.EntityField.COMPANY_NAME, f5_constants.EntityField.ENTITY_ID]
                        })
                        log.debug({title: 'customerInfo', details: JSON.stringify(customerInfo)})
                        recordFolderObj.FolderName = (customerInfo.entityid === customerInfo.companyname) ? options.entityType + ' ' + customerInfo.entityid  : options.entityType + ' ' + customerInfo.entityid + ' ' + customerInfo.companyname   ;
                        recordFolderObj.ParentFolder = parentGoogleDriveInfo.custentity_f5_nsgdrv_folderid
                    }
                    if (options.hasOwnProperty('vendorId')) {
                        var vendorInfo = f5_search.lookupFields({
                            type: 'vendor',
                            id: options.vendorId,
                            columns: [f5_constants.EntityField.COMPANY_NAME, f5_constants.EntityField.ENTITY_ID]
                        })
                        log.debug({title: 'vendorInfo', details: JSON.stringify(vendorInfo)})
                        recordFolderObj.FolderName = (vendorInfo.entityid === vendorInfo.companyname) ? options.entityType + ' ' + vendorInfo.entityid   :  options.entityType + ' ' + vendorInfo.entityid + ' ' + vendorInfo.companyname
                        recordFolderObj.ParentFolder = parentGoogleDriveInfo.custentity_f5_nsgdrv_folderid
                    }


                    var parent = this.searchFolder({
                        parentFolderId: parentGoogleDriveInfo.custentity_f5_nsgdrv_folderid,
                        entityType: options.entityType,
                        folderName: recordFolderObj.FolderName,
                        driveId: options.driveId
                    })
                    if (!parent) {

                        var recordFolderResponse = this.createFolder(recordFolderObj)
                        if (recordFolderResponse.issuccess) {
                            var recordFolderResponseBody = JSON.parse(recordFolderResponse.body)
                            parent = recordFolderResponseBody.id
                        }
                    }
                    return parent
                }

            }
        }
    })
;



