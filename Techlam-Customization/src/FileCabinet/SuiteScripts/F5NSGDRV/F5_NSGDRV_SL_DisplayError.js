/**
 *@NApiVersion 2.x
 *@NScriptType Suitelet
 */
define(['N/ui/serverWidget', 'N/https','N/email', 'N/runtime'],
    function(ui, https, email, runtime) {
        function onRequest(context) {
            if (context.request.method === 'GET') {
                var form = ui.createForm({
                    title: 'But... There was an error in the Interface to Google Drive'
                });
                var errorCode = form.addField({
                    id: 'errorcode',
                    type: ui.FieldType.INLINEHTML,
                    label: "error Code"
                });
                errorCode.updateDisplayType({
                    displayType: ui.FieldDisplayType.INLINE
                });

                var errorObj = context.request.parameters
                errorCode.defaultValue = '<p><span style="font-size:16px;"><strong><span style="color:rgb(36,255,57);">Error Code: ' + errorObj.name + '</span></strong></p>';
                var errormessage = form.addField({
                    id: 'message',
                    type: ui.FieldType.INLINEHTML,
                    label: 'Message'
                });

                errormessage.defaultValue = '<p><span style="font-size:16px;"><strong><span style="color:rgb(255,0,0);">Error Code: ' + errorObj.message + '</span></strong></p>';
                //errormessage.defaultValue = context.request.parameters.message
               // message.defaulvalue = http.serverRequest.parameters
                context.response.writePage(form);
            } 
        }
        return {
            onRequest: onRequest
        };
    });