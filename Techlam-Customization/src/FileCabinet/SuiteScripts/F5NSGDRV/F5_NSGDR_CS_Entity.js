/**
 * Module Description
 * Displays message on Folder Creation in Google Drive
 *
 * Version    Date            Author           Remarks
 * 1.00       05 Nov 2021     F5-JIP            Initial Development
 *
 * @NApiVersion 2.0
 * @NScriptType ClientScript
 */
define([
        './common/F5_Format',
        './common/F5_Message',
        './common/F5_Dialog',
    ],
    function (f5_format, f5_message, f5_dialog) {

        function hasNoValue(value) {
            return value === undefined || value == null || value === '';
        }

        return {
            pageInit: function pageInit(scriptContext) {
                try {
                    debugger
                    var currentRecord = scriptContext.currentRecord;
                    var folderCreated = currentRecord.getValue('custpage_f5_foldercreated');
                    var errorMessage = currentRecord.getValue('custpage_f5_errormessage');
                    console.log('status ' + submitStatus)
                    if (folderCreated) {
                        if (folderCreated === 'T') {
                            var message = f5_message.createConfirmation({
                                title: 'Folder Creation',
                                message: 'Folder was created in Google Drive',
                                duration: 8000
                            });
                            message.show();
                        } else {
                            message = f5_message.createError({
                                title: 'Folder Creation',
                                message: 'There was an error when creating the folder in Google Drive. Error was : ' + errorMessage,
                                duration: 8000
                            });
                            message.show();
                        }
                    }
                } catch (e) {
                    log.error('pageInit', JSON.stringify(e));
                }
            },

        };
    });