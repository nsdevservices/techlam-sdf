define([
        './F5_Record',
        './F5_Constants'
    ],
    function (f5_record, f5_constants) {
        let FIELDS = f5_constants.EntityField;

        class Entity extends f5_record.Record {
            getSubsidiary() {
                return this.getValue({fieldId: FIELDS.SUBSIDIARY});
            };

            getStatus() {
                return this.getValue({fieldId: FIELDS.STATUS});
            };
        };

        return {
            Entity: Entity,
            cast(record) {
                return new Entity(record);
            },
            create(options) {
                return this.cast(f5_record.create(options));
            },
            load(options) {
                return this.cast(f5_record.load(options));
            }
        };
    });