define([
    'N/url'
],
function (url) {
    return {
        resolveDomain(options) {
            return url.resolveDomain(options);
        },
        resolveRecord(options) {
            return url.resolveRecord(options);
        },
        resolveScript(options) {
            return url.resolveScript(options);
        },
        getTransactionUrl(tranType, id) {
            let domain = this.resolveDomain({hostType: url.HostType.APPLICATION});

            return ['https://', domain, '/app/accounting/transactions/', tranType.toLowerCase(), '.nl?id=', id].join('');
        },
        getRecordUrl(options) {
            let domain = this.resolveDomain({hostType: url.HostType.APPLICATION});
            let recordUrl = this.resolveRecord(options)

            return ['https://', domain, recordUrl].join('');
        },
        getScriptStatusUrl(scriptId) {
            let domain = this.resolveDomain({hostType: url.HostType.APPLICATION});
            let scriptStatusUrl = '/app/common/scripting/mapreducescriptstatus.nl?daterange=TODAY&scripttype={id}&primarykey=';

            return ['https://', domain, scriptStatusUrl.replace('{id}', scriptId)].join('');
        }
    };
});