define([
        './F5_Constants',
        './F5_ServerWidget',
        './F5_Format'
    ],
    function (f5_constants, f5_serverWidget, f5_format) {
        var FIELDS = f5_constants.CustomField;
        var SUBLISTS = f5_constants.Sublist;
        var BUTTONS = f5_constants.Button;
        var LABELS = f5_constants.Label;

        class FilteredSublistForm extends f5_serverWidget.Form {

            setClientScript(scriptFilePath) {
                this.setClientScriptModulePath(scriptFilePath);
            };

            addGroupedFields(options) {
                this.addFieldGroup({id: options.groupId, label: options.groupLabel});
                this.addFields({fields: options.fields, container: options.groupId});
            };

            addSimpleSublist(options) {
                let lines = options.lines;
                let sublist = this.addSublist({id: options.id, type: f5_serverWidget.SublistType.LIST, label: options.label});
                if (options.includeMarkAll) {
                    sublist.addButton({id: BUTTONS.MARK_ALL, label: LABELS.MARK_ALL, functionName: 'markAll'});
                    sublist.addButton({id: BUTTONS.UNMARK_ALL, label: LABELS.UNMARK_ALL, functionName: 'unmarkAll'});
                }
                sublist.addFields(options.fields);
                if (lines.length > 0) {
                    sublist.addLineItems(lines);
                }
            };

            addInlineEditorSublist(options) {
                let lines = options.lines;
                let sublist = this.addSublist({id: options.id, type: f5_serverWidget.SublistType.INLINEEDITOR, label: options.label});
                sublist.addFields(options.fields);
                if (lines.length > 0) {
                    sublist.addLineItems(lines);
                }
            };

            addResetButton(options) {
                this.addButton({id: BUTTONS.RESET, label: options.label, functionName: 'resetForm'});
            };

            addFilterSublistButton(options) {
                this.addButton({id: BUTTONS.FILTER, label: options.label, functionName: 'filterSublist'});
            };

            addBannerMessages(options) {
                this.addInlineHtmlField({id: FIELDS.BANNER_MESSAGES, label: LABELS.BANNER_MESSAGES, displayType: f5_serverWidget.FieldDisplayType.HIDDEN, defaultValue: JSON.stringify(options.messages)});
            };
        }

        return {
            createForm(options) {
                return new FilteredSublistForm(f5_serverWidget.createForm(options));
            }
        };
    });