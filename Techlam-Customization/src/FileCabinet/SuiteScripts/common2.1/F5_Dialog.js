define([
    'N/ui/dialog'
],
function (dialog) {
    return {
        create(options) {
            return dialog.create(options);
        }
    };
});