define([
        'N/ui/serverWidget'
    ],
    function (serverWidget) {
        class Field {
            field;

            constructor(field) {
                this.field = field;
            };

            updateDisplayType(options) {
                if (this.field) {
                    this.field.updateDisplayType(options);
                }
            };
            updateLayoutType(options) {
                if (this.field) {
                    this.field.updateLayoutType(options);
                }
            };
            updateBreakType(options) {
                if (this.field) {
                    this.field.updateBreakType(options);
                }
            };
            hide() {
                this.updateDisplayType({displayType: serverWidget.FieldDisplayType.HIDDEN});
            };
            show() {
                this.updateDisplayType({displayType: serverWidget.FieldDisplayType.NORMAL});
            };
            disable() {
                this.updateDisplayType({displayType: serverWidget.FieldDisplayType.DISABLED});
            };
            require() {
                if (this.field) {
                    this.field.isMandatory = true;
                }
            };
            unRequire() {
                if (this.field) {
                    this.field.isMandatory = false;
                }
            };
            setDefaultValue(defaultValue) {
                if (this.field) {
                    this.field.defaultValue = defaultValue;
                }
            };
            removeSelectOption(options) {
                if (this.field) {
                    this.field.removeSelectOption(options);
                }
            };
            addSelectOption(options) {
                if (this.field) {
                    this.field.addSelectOption(options);
                }
            };
        }

        class Sublist {
            sublist;

            constructor(sublist) {
                this.sublist = sublist;
            }

            addButton(options) {
                if (this.sublist) {
                    return this.sublist.addButton(options);
                }
            };
            hide() {
                this.sublist.displayType = serverWidget.SublistDisplayType.HIDDEN;
            };
            addField(options) {
                return new Field(this.sublist.addField(options));
            };
            getField(options) {
                return new Field(this.sublist.getField(options));
            };
            addFields(fields) {
                let me = this;
                fields.forEach(function (field) {
                    let fld = me.addField({
                        id: field.id,
                        label: field.label,
                        type: field.type,
                        source: field.source || ''
                    });
                    if (field.displayType) {
                        fld.updateDisplayType({
                            displayType: field.displayType
                        });
                    }
                    if (field.isMandatory) {
                        fld.require();
                    }
                    if (field.height && field.width) {
                        fld.updateDisplaySize({
                            height: field.height,
                            width: field.width
                        });
                    }
                    if (field.selectOptions) {
                        field.selectOptions.forEach(function (selectOption) {
                            fld.addSelectOption(selectOption);
                        });
                    }
                    if (field.defaultValue) {
                        field.defaultValue = field.defaultValue;
                    }
                });
            };
            addLineItems(items) {
                for (let i = 0, ii = items.length; i < ii; i++) {
                    let item = items[i];
                    for (let fieldId in item) {
                        let value = item[fieldId];
                        if (value) {
                            this.sublist.setSublistValue({id: fieldId, line: i, value: value});
                        }
                    }
                }
            };
            hideFields(options) {
                options.fields.forEach(function (id) {
                    let field = this.sublist.getField({id: id});
                    if (field) {
                        field.updateDisplayType({displayType: serverWidget.FieldDisplayType.HIDDEN});
                    }
                });
            };
            requireFields(options) {
                options.fields.forEach(function (id) {
                    let field = this.sublist.getField({id: id});
                    if (field) {
                        field.isMandatory = true;
                    }
                });
            };

            setSublistValue(options) {
                if (this.sublist) {
                    this.sublist.setSublistValue(options);
                }
            };

            getLineCount() {
                return this.sublist ? this.sublist.lineCount : -1;
            };

            addMarkAllButtons() {
                this.sublist.addMarkAllButtons();
            }
        }

        class Form {
            form;

            constructor(form) {
                this.form = form;
            }

            getField(options) {
                return new Field(this.form.getField(options));
            };
            addField(options) {
                let field = new Field(this.form.addField(options));
                field.setDefaultValue(options.defaultValue);
                if (options.isMandatory) {
                    field.require();
                }
                if (options.selectOptions) {
                    options.selectOptions.forEach(function (selectOption) {
                        field.addSelectOption(selectOption);
                    });
                }
                if (options.displayType) {
                    field.updateDisplayType({displayType: options.displayType});
                }
                if (options.layoutType) {
                    field.updateLayoutType({layoutType: options.layoutType});
                }
                if (options.breakType) {
                    field.updateBreakType({breakType: options.breakType});
                }
                if (options.defaultValue) {
                    field.defaultValue = options.defaultValue;
                }

                return field;
            };
            addInlineHtmlField(options) {
                options.type = serverWidget.FieldType.INLINEHTML;
                return this.addField(options);
            };
            addLongTextField(options) {
                options.type = serverWidget.FieldType.LONGTEXT;
                return this.addField(options);
            };
            addTextField(options) {
                options.type = serverWidget.FieldType.TEXT;
                return this.addField(options);
            };
            addCheckBoxField(options) {
                options.type = serverWidget.FieldType.CHECKBOX;
                return this.addField(options);
            };
            addSelectField(options) {
                options.type = serverWidget.FieldType.SELECT;
                return this.addField(options);
            };
            addDateField(options) {
                options.type = serverWidget.FieldType.DATE;
                return this.addField(options);
            };
            addFields(options) {
                let me = this;
                options.fields.forEach(function (fieldOptions) {
                    fieldOptions.container = options.container;
                    me.addField(fieldOptions);
                });
            };
            insertField(options) {
                this.form.insertField(options);
            };
            getSublist(options) {
                return new Sublist(this.form.getSublist(options));
            };
            addSublist(options) {
                return new Sublist(this.form.addSublist(options));
            };
            hideSublist(options) {
                let sublist = this.form.getSublist(options);
                if (sublist) {
                    sublist.displayType = serverWidget.SublistDisplayType.HIDDEN;
                }
            };
            getButton(options) {
                return this.form.getButton(options);
            };
            addButton(options) {
                return this.form.addButton(options);
            };
            removeButton(options) {
                this.form.removeButton(options);
            };
            disableFields(fieldIds, editableFieldIds) {
                let me = this;
                editableFieldIds = editableFieldIds || [];
                log.debug('fieldIds', JSON.stringify(fieldIds));
                log.debug('editableFieldIds', JSON.stringify(editableFieldIds));
                fieldIds.forEach(function (id) {
                    if (editableFieldIds.indexOf(id) < 0) {
                        log.debug('id', id);
                        let field = me.getField({id: id});
                        if (field) {
                            log.debug('field.disable()', id);
                            field.disable();
                        }
                    }
                });
            };
            addSubtab(options) {
                return this.form.addSubtab(options);
            };
            addTab(options) {
                return this.form.addTab(options);
            };
            insertSubtab(options) {
                this.form.insertSubtab(options);
            };
            insertSublist(options) {
                this.form.insertSublist(options);
            };
            addSubmitButton(options) {
                return this.form.addSubmitButton(options);
            };
            addResetButton(options) {
                return this.form.addResetButton(options);
            };
            addFieldGroup(options) {
                return this.form.addFieldGroup(options);
            };
            setClientScriptModulePath(path) {
                this.form.clientScriptModulePath = path;
            };
            addPageInitMessage(options) {
                this.form.addPageInitMessage(options);
            };
        }

        return {
            get FieldType() {
                return serverWidget.FieldType;
            },
            get FieldDisplayType() {
                return serverWidget.FieldDisplayType;
            },
            get FieldLayoutType() {
                return serverWidget.FieldLayoutType;
            },
            get FieldBreakType() {
                return serverWidget.FieldBreakType;
            },
            get SublistType() {
                return serverWidget.SublistType;
            },
            Form: Form,
            castForm(form) {
                return new Form(form);
            },
            createForm(options) {
                return serverWidget.createForm(options);
            }
        };
    });