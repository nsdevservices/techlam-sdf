define([
    'N/email'
],
function (email) {
    return {
        send(options) {
            return email.send(options);
        }
    };
});