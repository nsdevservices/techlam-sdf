define([
        './F5_Constants',
        './F5_CurrentRecord',
        './F5_Message',
        './F5_Format'
    ],
    function (f5_constants, f5_currentRecord, f5_message, f5_format) {
        return {
            updateAll(options) {
                let record = f5_currentRecord.get();
                let sublistId = options.sublistId;
                let fieldId = options.fieldId;
                let value = options.value;
                let lineCount = record.getLineCount({sublistId: sublistId});
                for (let i = 0; i < lineCount; i++) {
                    record.selectLine({sublistId: sublistId, line: i});
                    record.setCurrentSublistValue({sublistId: sublistId, fieldId: fieldId, value: value, ignoreFieldChange: true});
                    record.commitLine({sublistId: sublistId});
                }
            },
            refreshPage() {
                // suppress the alert
                setWindowChanged(window, false);
                // submit the form
                document.forms.main_form.submit();
            },
            hasSelection(options) {
                let returnValue = false
                let record = options.record;
                let sublistId = options.sublistId;
                let fieldId = options.fieldId;
                let lineCount = record.getLineCount({sublistId: sublistId});
                for (let i = 0; i < lineCount; i++) {
                    let select = record.getSublistValue({sublistId: sublistId, fieldId: fieldId, line: i});
                    if (select) {
                        returnValue = true;
                    }
                }

                return returnValue;
            },
            clearFields(options) {
                let record = f5_currentRecord.get();
                let fields = options.fields || [];
                fields.forEach(function (fieldId) {
                    record.setValue({fieldId: fieldId, value: ''});
                });
            },
            displayBannerMessages(options) {
                let messages = options.messages;
                let actionMap = {
                    errors: f5_message.createError,
                    confirmations: f5_message.createConfirmation,
                    informations: f5_message.createInformation,
                    warnings: f5_message.createWarning
                };
                if (messages) {
                    Object.keys(messages).forEach(function (messageKey) {
                        let action = actionMap[messageKey];
                        let bannerMessages = messages[messageKey] || [];
                        bannerMessages.forEach(function (messageOptions) {
                            let message = action(messageOptions);
                            if (message) {
                                message.show();
                            }
                        });
                    });
                }
            }
        };
    });