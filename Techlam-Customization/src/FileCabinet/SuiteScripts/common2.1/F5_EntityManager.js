define([
        './F5_Record',
        './F5_Constants',
    './F5_Url',
        './F5_Authentication'
    ],
    function (f5_record, f5_constants,f5_url,f5_authentication) {
    return {
            createVendorFromCustomer: function createVendorFromCustomer(options) {
                let customer = options.customer;
                let headers = options.headers;
                let customerId = customer.id;
                let hackUrl = f5_url.getRecordUrl({
                    recordType: customer.type,
                    recordId: customerId,
                    isEditMode: false,
                    params: {
                        totype: 'vendor',
                        fromtype: 'custjob',
                    }
                });
                log.debug('createVendorFromCustomer: headers', headers);
                hackUrl = hackUrl.replace('custjob.nl', 'company.nl');
                log.debug('createVendorFromCustomer: hackUrl', hackUrl);
                if (!headers) {
                    headers =  f5_authentication.getBasicHeaders();
                    log.debug('createVendorFromCustomer: oauth headers', headers);
                }
                if (headers) {
                    headers['User-Agent-x'] = 'SuiteScript-Call';
                    try {
                        let response = f5_https.get({url: hackUrl, headers: headers});
                        log.debug('response data', response);
                    } catch (e) {
                        log.error('Unexpected error', f5_format.exceptionToString(e));
                    }
                }
            }
        };
    });