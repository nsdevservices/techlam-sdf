define([
        './F5_Search',
        './F5_Record',
        './F5_Constants',
        './F5_StandardTransaction'
    ],
    function (f5_search, f5_record, f5_constants, f5_standardTransaction) {
        let TYPE = f5_search.Type.TRANSACTION;
        let TYPE_IDS = f5_constants.TransactionTypeInternalId;
        let FIELDS = f5_constants.TransactionField;
        let SUBLISTS = f5_constants.Sublist;
        let SUBLIST_FIELDS = f5_constants.SublistField;
        let keyFieldIdMap = {
            subsidiary: FIELDS.SUBSIDIARY,
            class: FIELDS.CLASS,
            memo: FIELDS.MEMO
        };
        let lineKeyFieldIdMap = {
            subsidiary: SUBLIST_FIELDS.SUBSIDIARY,
            class: SUBLIST_FIELDS.CLASS,
            quantity: SUBLIST_FIELDS.QUANTITY,
        };

        class Transaction extends f5_standardTransaction.StandardTransaction {

        };

        return {
            Transaction: Transaction,
            cast(record) {
                return new Transaction(record);
            },
            create(options) {
                return this.cast(f5_record.create(options));
            },
            load(options) {
                return this.cast(f5_record.load(options));
            },
            lookupCreatedFrom(options) {
                options.columns = [FIELDS.CREATED_FROM];
                let createdFrom = '';
                let fieldLookUpObj = f5_search.lookupFields(options);
                let column = fieldLookUpObj[FIELDS.CREATED_FROM];
                if (column && column[0]) {
                    createdFrom = column[0].value || ''
                }
                return createdFrom;
            },
            lookupTransactionType(options) {
                options.columns = [FIELDS.TYPE];
                let transactionType = '';
                let fieldLookUpObj = f5_search.lookupFields(options);
                let column = fieldLookUpObj[FIELDS.TYPE];
                if (column && column[0]) {
                    transactionType = column[0].value || ''
                }
                return transactionType;
            },
            submitOrderStatus(options) {
                let values = {};
                values[FIELDS.ORDER_STATUS] = options.value;
                f5_record.submitFields({type: options.type, id: options.id, values: values});
            }
        };
    });