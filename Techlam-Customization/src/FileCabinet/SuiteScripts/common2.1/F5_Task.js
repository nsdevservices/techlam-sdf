define([
        'N/task'
    ],
    function (task) {
        return {
            submitScheduledScript(options) {
                options.taskType = task.TaskType.SCHEDULED_SCRIPT;
                let ssTask = task.create(options);
                return ssTask.submit();
            },
            submitMapReduce(options) {
                options.taskType = task.TaskType.MAP_REDUCE;
                let mapReduceTask = task.create(options);
                return mapReduceTask.submit();
            },
            createMapReduce(options) {
                options.taskType = task.TaskType.MAP_REDUCE;
                return task.create(options);
            },
            submitCSVImport(options) {
                options.taskType = task.TaskType.CSV_IMPORT;
                let csvImportTask = task.create(options);
                return csvImportTask.submit();
            },
            runMapReduce: function runMapReduce(options) {
                try {
                    this.submitMapReduce(options);
                } catch (e) {
                    log.error('runMapReduce error', e);
                    let deploymentId = f5_scriptDeployment.getDeployScriptId(options.deploymentId);
                    if (deploymentId) {
                        let scriptDeployment = f5_scriptDeployment.copy({id: deploymentId});
                        scriptDeployment.save();
                        let mapReduceTask = this.submitMapReduce(options);
                        log.error('runMapReduce mapReduceTask', mapReduceTask);
                    }
                }
            }
        };
    });