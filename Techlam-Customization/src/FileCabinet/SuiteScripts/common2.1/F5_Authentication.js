/**
 * @NApiVersion 2.0
 * @NModuleScope Public
 */
define(function (require) {
    let crypto = require('../lib/crypto-js');
    require('../lib/oauth'); // do not change.
    class getAuthDetails {
      getAuthDetails(configId) {
            let authDetails = {};
            authDetails.realm = '1234567_SB1';
            authDetails.basic = false;
            authDetails.type = 'oauth';
            authDetails.consumerKey = 'foo';
            authDetails.signatureMethod = 'HMAC-SHA256';
            authDetails.token = 'bar';
            authDetails.consumersecret = 'baz';
            authDetails.tokensecret = 'qux';

            return authDetails;
        }
    }

    class tokenAuthentication{

        constructor(authDetails, request) {
            this.authDetails = authDetails;
            this.request = request;
        }
        tokenAuthentication(){
            let oauth = OAuth({
                consumer: {
                    key: this.authDetails.consumerKey, //Manage Integrations
                    secret:this.authDetails.consumersecret //Manage Integrations
                },
                signature_method: this.authDetails.signatureMethod,
                hash_function: function (base_string, key) {
                    return crypto.HmacSHA256(base_string, key).toString(crypto.enc.Base64);
                }
            });
        let token = {
            key: this.authDetails.token,
            secret: this.authDetails.tokensecret
        };
        let request_data = {
            url: this.request.url,
            method: request.method,
            data: {}
        };
        let headerWithRealm = oauth.toHeader(oauth.authorize(request_data, token));
        headerWithRealm.Authorization += ', realm="' + this.authDetails.realm + '"';

        return headerWithRealm.Authorization;
        }
    }
    class getOauthAuthorization {
        constructor(authDetails, request) {
            this.authDetails = authDetails;
            this.request = request;
        }
        getOauthAuthorization() {
            let authentication = '';
            this.authDetails.timeStamp = Math.round(+new Date() / 1000);
            authentication = new tokenAuthentication().tokenAuthentication(this.authDetails, request);

            return authentication;
        }
    }

    return {
        getOauthHeaders: function getOauthHeaders(options) {
            let authDetails =  new getAuthDetails().getAuthDetails();
            let authorization = new getOauthAuthorization().getOauthAuthorization(authDetails, options.request);
            let headerDetail = {};
            headerDetail['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36';
            headerDetail['Accept'] = '*/*';
            headerDetail['Accept-Encoding'] = 'gzip, deflate, br';
            headerDetail['Connection'] = 'keep-alive';
            headerDetail['HOST'] = 'application.netsuite.com';
            headerDetail['Authorization'] = authorization;
            headerDetail['Content-Type'] = 'application/json';

            return headerDetail;
        },
        getBasicHeaders: function getBasicHeaders() {
            let secret = {
                realm: '4904705_SB1',
                user: {
                    email: 'integration@fusion5.com.au',
                    password: 'integratioN@1689',
                    role: '1007'
                }
            }
            let authString = 'nlauth_account = ' + secret.realm;
            authString += ',nlauth_email=' + secret.user.email
            authString += ',nlauth_signature=' + secret.user.password
            authString += ',nlauth_role=' + secret.user.role

            let headers = {
                'Authorization': 'NLAuth ' + authString
            };

            return headers;
        },
    };
});