define([
    'N/xml'
],
function (xml) {
    return {
        escape(options) {
            return xml.escape(options);
        }
    };
});