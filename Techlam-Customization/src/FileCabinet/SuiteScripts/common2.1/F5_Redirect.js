define([
    'N/redirect'
],
function (redirect) {
    return {
        toRecord(options) {
            redirect.toRecord(options);
        }
    };
});