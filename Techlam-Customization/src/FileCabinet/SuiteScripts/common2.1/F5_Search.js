define([
    'N/search',
    './F5_Constants'
],
function (search, f5_constants) {
    let module = {
        get Type() {
            return search.Type;
        },
        get CustomType() {
            return f5_constants.CustomRecordType;
        },
        get Operator() {
            return search.Operator;
        },
        get Summary() {
            return search.Summary;
        },
        get Sort() {
            return search.Sort;
        },
        get LogicalOperator() {
            return {
                AND: 'and',
                OR: 'or'
            };
        },
        get DateTime() {
            return {
                THIS_FIS_YR: 'thisFiscalYear',
                NEXT_FIS_YR: 'nextFiscalYear',
                THIS_YR: 'thisYear',
                NEXT_YR: 'nextOneYear',
                LAST_YR: 'lastyear',
                TODAY: 'today'
            };
        },
        get FormulaTemplate() {
            return f5_constants.FormulaTemplate
        },
        get FormulaField() {
            return {
                NUMERIC: 'formulanumeric'
            }
        },
        get Value() {
            return {
                NONE: '@NONE@',
                TODAY: 'today',
                THIS_MONTH: 'thismonth',
                THIS_FIS_YR: 'thisFiscalYear',
                NEXT_FIS_YR: 'nextFiscalYear',
                THIS_YR: 'thisYear',
                NEXT_YR: 'nextOneYear',
                LAST_YR: 'lastyear'
            }
        },
        get CheckBox() {
            return f5_constants.CheckBox;
        },
        create(options) {
            if (options.columnMap) {
                options.columns = Object.keys(options.columnMap).map(function (columnKey) {
                    return options.columnMap[columnKey];
                });
            }

            return new Search({search: search.create(options)});
        },
        load(options) {
            return new Search({search: search.load(options)});
        },
        createFilter: function createFilter(options) {
            return search.createFilter(options);
        },
        createAnyOfStringFilter(options) {
            return search.createFilter({
                name: module.FormulaField.NUMERIC,
                operator: search.Operator.EQUALTO,
                values: 1,
                formula: module.FormulaTemplate.CASE_IN_STRINGS.replace("{name}", name).replace("{values}", options.values.join("', '"))
            });
        },
        createColumn(options) {
            return search.createColumn(options);
        },
        lookupFields(options) {
            return search.lookupFields(options);
        }
    };

    class Search {
        search;
        columns;

        constructor(options) {
            this.search = options.search;
            this.columns = options.search.columns;
        };

        addColumns(options) {
            let additionalColumns = options.columnMap ? Object.keys(options.columnMap).map(function (key) {
                return options.columnMap[key];
            }) : options.columns;
            this.search.columns = this.search.columns.concat(additionalColumns);
            this.columns = this.search.columns;
        };

        replaceColumns(options) {
            this.search.columns = options.columns;
            this.columns = this.search.columns;
        };

        addFilterExpression(filterExpression) {
            this.search.filterExpression = [this.search.filterExpression, module.LogicalOperator.AND, filterExpression];
        };

        addFilter(filter) {
            this.search.filters.push(filter);
        };

        run() {
            return this.search.run();
        };

        runPaged(options) {
            return this.search.runPaged(options);
        };

        getIds() {
            let ids = [];
            this.search.run().each(function (result) {
                ids.push(result.id);

                return true;
            });

            return ids;
        };

        getResults(options) {
            let limit = 4000;
            let allResults = [];
            let rs = this.search.run();

            rs.each(function (result) {
                allResults.push(result);

                return allResults.length < limit;
            });

            if (allResults.length == limit) {
                let start = limit;
                let results = [];
                do {
                    results = rs.getRange({
                        start: start,
                        end: start + 1000
                    });
                    allResults = allResults.concat(results);
                    start += 1000;
                } while (results.length > 0);
            }

            return allResults;
        };

        getPagedResults(options) {
            options = options || {pageSize: 1000}
            let allResults = [];
            let pagedData = this.search.runPaged(options);
            pagedData.pageRanges.forEach(function (pageRange) {
                let page = pageRange.fetch({index: pageRange.index});
                page.data.forEach(function (result) {
                    allResults.push(result);
                });
            });

            return allResults;
        };

        getJSONResults(keyColumnMap) {
            let jsonResults = [];

            (this.getResults() || []).forEach(function (result) {
                let jsonResult = {};
                for (let key in keyColumnMap) {
                    let column = keyColumnMap[key];
                    if (column) {
                        jsonResult[key] = key.indexOf('Text') > -1 ? result.getText(column) : result.getValue(column);
                    }
                }
                jsonResults.push(jsonResult);
            });

            return jsonResults || [];
        };

        getRange(options) {
            let results = this.search.run().getRange(options);

            return results || [];
        };

        getFirstResult() {
            let resultRange = this.getRange({start: 0, end: 1});

            return resultRange.length > 0 ? resultRange[0] : null;
        };
    }

    return module;
});