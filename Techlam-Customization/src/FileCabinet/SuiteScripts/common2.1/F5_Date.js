define([], function () {
    return {
        addMonths: function (date, additionalMonths) {
            let newDate = new Date(date.getTime());
            let day = newDate.getDate();
            newDate.setMonth(newDate.getMonth() + additionalMonths);

            if (newDate.getDate() !== day) {
                newDate.setDate(0);
            }

            return newDate;
        }
    };
});