define([
        './F5_Record',
        './F5_Search',
        './F5_Constants'
    ],
    function (f5_record, f5_search, f5_constants) {

        let TYPE = f5_search.Type.SCRIPT_DEPLOYMENT;
        let FIELDS = f5_constants.Field;

        class ScriptDeployment extends f5_record.Record{

            getScriptId() {
                return this.getValue({fieldId: FIELDS.SCRIPT_ID});
            };
            setScriptId(value) {
                this.getValue({fieldId: FIELDS.SCRIPT_ID, value: value});
            };

            setTitle(value) {
                this.setValue({fieldId: FIELDS.TITLE, value: value});
            };

        }
        return {
            get Type() {
                return this.record.Type;
            },
            create(options) {
                options.type = this.Type;
                return new ScriptDeployment(f5_record.create(options));
            },
            copy(options) {
                options.type = TYPE;
                return new ScriptDeployment(f5_record.copy(options));
            },
            createEntry(options) {
                let record = this.create(options);
                if (options.title) {
                    record.setTitle(options.title);
                }
                if (options.scriptId) {
                    record.setScriptId(options.scriptId);
                }
                return record.save();
            },
            getDeployScriptID(scriptId) {
                let scriptDeploymentSearch = f5_search.create({
                    type: TYPE,
                    filters: [{name: FIELDS.SCRIPT_ID, operator: f5_search.Operator.IS, values: scriptId}]
                });

                return scriptDeploymentSearch.getFirstResult().id;
            }
        };
    });