define([
        './F5_Record',
        './F5_Search',
        './F5_Constants'
    ],
    function (f5_record, f5_search, f5_constants) {
        let TYPE = f5_search.Type.ITEM;
        let FIELDS = f5_constants.ItemField;

        //TODO Item helper functions like on hand/available

        return {
            getItemDetailsMap(items) {
                let map = {};
                let search = f5_search.create({
                    type: TYPE,
                    filters: [FIELDS.INTERNAL_ID, f5_search.Operator.ANYOF, items],
                    columns: [
                        {name: FIELDS.ITEM_ID},
                        {name: FIELDS.AVERAGE_COST},
                        {name: FIELDS.LAST_COST}
                    ]
                });
                (search.getResults() || []).forEach(function (result) {
                    map[result.id] = {
                        itemNumber: result.getValue({name: FIELDS.ITEM_ID}),
                        averageCost: parseFloat(result.getValue({name: FIELDS.AVERAGE_COST}) || 0),
                        lastCost: parseFloat(result.getValue({name: FIELDS.LAST_COST}) || 0)
                    };
                });

                return map;
            }
        };
    });