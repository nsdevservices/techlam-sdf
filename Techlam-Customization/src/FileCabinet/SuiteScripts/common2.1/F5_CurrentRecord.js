define([
        'N/currentRecord',
    './F5_Record'
    ],
    function (currentRecord, f5_record) {
        return {
            get() {
                return f5_record.cast(currentRecord.get());
            }
        };
    });