define([
        './F5_StandardConstants'
    ],
    function (f5_standard) {
        let TRANSACTION_FIELDS = f5_standard.TransactionField;
        // TRANSACTION_FIELDS.FOO = 'custbody_f5_foo';     // Add custom fields here
        let ENTITY_FIELDS = f5_standard.EntityField;
        // ENTITY_FIELDS.BAR = 'custentity_f5_foo';     // Add custom fields here
        let ITEM_FIELDS = f5_standard.ItemField;
        // ITEM_FIELDS.BAZ = 'custitem_f5_foo';     // Add custom fields here
        let ITEM_NUMBER_FIELDS = f5_standard.ItemNumberField;
        // ENTITY_FIELDS.BAR = 'custentity_f5_foo';     // Add custom fields here
        let SUBLISTS = f5_standard.Sublist;
        // Add custom sublists here
        let SUBLIST_FIELDS = f5_standard.SublistField;
        // Add custom fields here
        let ROLES = f5_standard.Role;
        // Add custom roles here
        let TABS = f5_standard.Tab;
        // Add custom tabs here
        let SUBTABS = f5_standard.Subtab;
        // Add custom subtabs here
        let BUTTONS = f5_standard.Button;
        // Add custom buttons here
        BUTTONS.FILTER = 'custpage_f5_btn_filter_sublist';
        BUTTONS.MARK_ALL = 'custpage_f5_btn_mark_all';
        BUTTONS.RESET = 'custpage_f5_btn_reset';
        BUTTONS.UNMARK_ALL = 'custpage_f5_btn_unmark_all';

        return {
            Field: f5_standard.Field,
            AddressField: f5_standard.AddressField,
            TransactionField: TRANSACTION_FIELDS,
            EntityField: ENTITY_FIELDS,
            ItemField: ITEM_FIELDS,
            ItemNumberField: ITEM_NUMBER_FIELDS,
            Sublist: SUBLISTS,
            SublistField: SUBLIST_FIELDS,
            WorkflowField: f5_standard.WorkflowField,
            Join: f5_standard.Join,
            TransactionTypeShort: f5_standard.TransactionTypeShort,
            TransactionTypeName: f5_standard.TransactionTypeName,
            TransactionTypeInternalId: f5_standard.TransactionTypeInternalId,
            CheckBox: f5_standard.CheckBox,
            Role: ROLES,
            Feature: f5_standard.Feature,
            Preference: f5_standard.Preference,
            User: f5_standard.User,
            Mode: f5_standard.Mode,
            ItemType: f5_standard.ItemType,
            ItemTypeInternalId: f5_standard.ItemTypeInternalId,
            Button: BUTTONS,
            Subtab: SUBTABS,
            Tab: TABS,
            CustomRecordType: {
                // FOO: 'customrecord_f5_foo'
            },
            CustomList: {
                // BAR: 'customlist_f5_bar'
            },
            CustomField: {
                // BAR: 'custpage_f5_bar'
                BANNER_MESSAGES: 'custpage_f5_banner_messages'
            },
            ScriptParameter: {
                // FOO: 'custscript_f5_foo'
            },
            ScriptId: {
                // SL_FOO: 'customscript_f5_sl_foo',
                // MR_BAR: 'customscript_f5_mr_bar',
                // SS_BAZ: 'customscript_f5_ss_baz'
            },
            DeploymentId: {
                // SL_FOO: 'customdeploy_f5_sl_foo',
                // MR_BAR: 'customdeploy_f5_mr_bar',
                // SS_BAZ: 'customdeploy_f5_ss_baz'
            },
            FormulaTemplate: {
                CASE_IN_STRINGS: "CASE WHEN {{name}} IN ('{values}') THEN 1 ELSE 0 END",
                CASE_IN_INTEGERS: "CASE WHEN {{name}} IN ({values}) THEN 1 ELSE 0 END"
            },
            Label: {
                BANNER_MESSAGES: 'Banner Messages',
                FILTER_SUBLIST: 'Filter Sublist',
                MARK_ALL: 'Mark All',
                RESET: 'Reset',
                UNMARK_ALL: 'Unmark All'
            }
        }
    });