define([
        'N/runtime',
        './F5_Constants'
    ],
    function (runtime, f5_constants) {
        return {
            executionContext: runtime.executionContext,
            envType: runtime.envType,
            accountId: runtime.accountId,
            isFeatureInEffect(name) {
                return runtime.isFeatureInEffect({feature: name});
            },
            getCurrentUser() {
                return runtime.getCurrentUser();
            },
            getUser() {
                return this.getCurrentUser().id + '';
            },
            getCurrentRole() {
                return this.getCurrentUser().role + '';
            },
            getPreference(options) {
                return this.getCurrentUser().getPreference(options);
            },
            getLocale() {
                return this.getPreference({name: f5_constants.Preference.LANGUAGE});
            },
            isUserInterfaceContext() {
                return runtime.executionContext == runtime.ContextType.USER_INTERFACE;
            },
            isLocationEnabled() {
                return this.isFeatureInEffect(f5_constants.Feature.LOCATIONS);
            },
            isOneWorld() {
                return this.isFeatureInEffect(f5_constants.Feature.ONE_WORLD);
            },
            getCurrentScript() {
                return runtime.getCurrentScript();
            },
            getScriptParameter(options) {
                return this.getCurrentScript().getParameter(options);
            },
            getBundleId() {
                if (!this.bundleId) {
                    let script = this.getCurrentScript();
                    if (script) {
                        this.bundleId = (script.bundleIds || [""])[0];
                    }
                }
                return this.bundleId;
            },
            getAccountSubdomain() {
                let accountId = runtime.accountId;
                return accountId.toLowerCase().replace("_", "-");
            },
            getSuiteAppId() {
                return f5_constants.SUITEAPP_ID;
            },
            getFolderPath(url, folderName) {
                return [url || '', '/c.', runtime.accountId, (this.getBundleId() ? '/suitebundle' + this.getBundleId() : '/suiteapp'), '/', this.getSuiteAppId(), '/', folderName].join('');
            },
            getSrcPath(url) {
                return this.getFolderPath(url, 'src');
            },
            getLibPath(url) {
                return this.getFolderPath(url, 'lib');
            }
        };
    });