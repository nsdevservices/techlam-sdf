define([
        'N/file',
        './F5_Search'
    ],
    function (file, f5_search) {
        var TYPE = 'file';
        var FIELDS = {
            URL: 'url'
        }; //TODO Add more file fields
        return {
            load(options) {
                return file.load(options);
            },
            lookupUrl(id) {
                var values = f5_search.lookupFields({type: TYPE, id: id, columns: [FIELDS.URL]});
                return values[FIELDS.URL] || '';
            }
        };
    });