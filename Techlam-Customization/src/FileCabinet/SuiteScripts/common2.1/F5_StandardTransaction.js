define([
        './F5_Search',
        './F5_Record',
        './F5_Constants'
    ],
    function (f5_search, f5_record, f5_constants) {
        let TYPE = f5_search.Type.TRANSACTION;
        let FIELDS = f5_constants.TransactionField;
        let SUBLISTS = f5_constants.Sublist;
        let SHORTS = f5_constants.TransactionTypeShort;
        let NAMES = f5_constants.TransactionTypeName;
        let IDS = f5_constants.TransactionTypeInternalId;
        let SUBLIST_FIELDS = f5_constants.SublistField;
        let ADDRESS_FIELDS = f5_constants.AddressField;

        class StandardTransaction extends f5_record.Record {
            getTypeShort() {
                return SHORTS[this.type.toUpperCase()] || '';
            };

            getTypeName() {
                return NAMES[this.type.toUpperCase()] || '';
            };

           getTypeId() {
                return IDS[this.type.toUpperCase()] || '';
            };

            getTranId() {
                return this.getValue({fieldId: FIELDS.TRAN_ID});
            };

            getDate() {
                return this.getValue({fieldId: FIELDS.DATE});
            };

            getTranDate() {
                return this.getValue({fieldId: FIELDS.TRAN_DATE});
            };

            getEntity() {
                return this.getValue({fieldId: FIELDS.ENTITY});
            };

            getTotal() {
                return this.getValue({fieldId: FIELDS.TOTAL}) || 0;
            };

            getSubsidiary() {
                return this.getValue({fieldId: FIELDS.SUBSIDIARY});
            };

            getDepartment() {
                return this.getValue({fieldId: FIELDS.DEPARTMENT});
            };

            getClass() {
                return this.getValue({fieldId: FIELDS.CLASS});
            };

            getLocation() {
                return this.getValue({fieldId: FIELDS.LOCATION});
            };

            getJob() {
                return this.getValue({fieldId: FIELDS.JOB});
            };

            getStatusRef() {
                return this.getValue({fieldId: FIELDS.STATUS_REF});
            };

            getCreatedFrom() {
                return this.getValue({fieldId: FIELDS.CREATED_FROM}) || '';
            };

            getMemo() {
                return this.getValue({fieldId: FIELDS.MEMO});
            };

            getCreatedFromTranId() {
                let tranId = '';
                let text = this.getText({fieldId: FIELDS.CREATED_FROM}) || '';
                if (text) {
                    let arr = text.split('#');
                    tranId = arr[1];
                }

                return tranId;
            };

            getLocation() {
                return this.getValue({fieldId: FIELDS.LOCATION});
            };

            getApprovalStatus() {
                return this.getValue({fieldId: FIELDS.APPROVAL_STATUS});
            };

            getEntityStatus() {
                return this.getValue({fieldId: FIELDS.ENTITY_STATUS});
            };

            getOrderStatus() {
                return this.getValue({fieldId: FIELDS.ORDER_STATUS});
            };

            getLandedCostPerline() {
                return this.getValue({fieldId: FIELDS.LANDED_COST_PER_LINE});
            };

            getCurrency() {
                return this.getValue({fieldId: FIELDS.CURRENCY});
            };

            getExchangeRate() {
                return this.getValue({fieldId: FIELDS.EXCHANGE_RATE});
            };

            isItemSublist(sublistId) {
                return sublistId == SUBLISTS.ITEM
            };

            setDate(value) {
                this.setValue({fieldId: FIELDS.DATE, value: value});
            };

            setTranDate(value) {
                this.setValue({fieldId: FIELDS.TRAN_DATE, value: value});
            };

            setMemo(value) {
                this.setValue({fieldId: FIELDS.MEMO, value: value});
            };

            setSourceFile(value) {
                this.setValue({fieldId: FIELDS.SOURCE_FILE, value: value});
            };

            setSubsidiary(value) {
                this.setValue({fieldId: FIELDS.SUBSIDIARY, value: value});
            };

            setDepartment(value) {
                this.setValue({fieldId: FIELDS.DEPARTMENT, value: value});
            };

            setClass(value) {
                this.setValue({fieldId: FIELDS.CLASS, value: value});
            };

            setLocation(value) {
                this.setValue({fieldId: FIELDS.LOCATION, value: value});
            };

            setEntity(value) {
                this.setValue({fieldId: FIELDS.ENTITY, value: value});
            };

            setNextApprover(value) {
                this.setValue({fieldId: FIELDS.NEXT_APPROVER, value: value});
            };

            setCustomForm(value) {
                this.setValue({fieldId: FIELDS.CUSTOM_FORM, value: value});
            };

            setJob(value) {
                this.setValue({fieldId: FIELDS.JOB, value: value});
            };

            setPostingPeriod(value) {
                this.setValue({fieldId: FIELDS.POSTING_PERIOD, value: value});
            };

            setCurrentItemInventoryAssignments(options) {
                let me = this;
                let inventoryFieldId = options.inventoryFieldId;
                let inventoryAssignments = options.inventoryAssignments;
                let sublistId = SUBLISTS.INVENTORY_ASSIGNMENT
                let subRecord = this.getCurrentSublistSubrecord({
                    sublistId: SUBLISTS.ITEM,
                    fieldId: SUBLIST_FIELDS.INVENTORY_DETAIL
                });
                inventoryAssignments.forEach(function (inventoryAssignment) {
                    this.setCurrentItemInventoryAssignment({
                        subRecord: subRecord,
                        inventoryFieldId: inventoryFieldId,
                        number: inventoryAssignment.number,
                        quantity: inventoryAssignment.quantity
                    });
                })
            };

            setCurrentItemInventoryAssignment(options) {
                let subRecord = options.subRecord;
                if (!subRecord) {
                    subRecord = this.getCurrentSublistSubrecord({
                        sublistId: SUBLISTS.ITEM,
                        fieldId: SUBLIST_FIELDS.INVENTORY_DETAIL
                    });
                }
                subRecord.selectNewLine({sublistId: sublistId});
                subRecord.setCurrentSublistValue({
                    sublistId: sublistId,
                    fieldId: SUBLIST_FIELDS.QUANTITY,
                    value: options.quantity
                });
                subRecord.setCurrentSublistValue({
                    sublistId: sublistId,
                    fieldId: options.inventoryFieldId,
                    value: options.number
                });
                subRecord.commitLine({sublistId: sublistId});
            }

            setItemInventoryAssignment(options) {
                let inventoryFieldId = options.inventoryFieldId;
                let number = options.number;
                let quantity = options.quantity;
                let sublistId = SUBLISTS.INVENTORY_ASSIGNMENT;
                let subRecord = this.getSublistSubrecord({
                    sublistId: SUBLISTS.ITEM,
                    fieldId: SUBLIST_FIELDS.INVENTORY_DETAIL,
                    line: options.line
                });
                if (subRecord) {
                    subRecord.setSublistValue({sublistId: sublistId, fieldId: inventoryFieldId, value: number, line: 0});
                    subRecord.setSublistValue({sublistId: sublistId, fieldId: SUBLIST_FIELDS.QUANTITY, value: quantity, line: 0});
                }
            };

            getItemLines(options) {
                options.sublistId = SUBLISTS.ITEM;
                return this.getSublistLines(options);
            };

            getExpenseLines() {
                options.sublistId = SUBLISTS.EXPENSE;
                return this.getSublistLines(options);
            };

            getItemSublistValue(options) {
                return this.getSublistValue({sublistId: SUBLISTS.ITEM, fieldId: options.fieldId, line: options.line});
            };

            setShippingAddress(options) {
                let subRecord = this.getSubrecord({fieldId: FIELDS.SHIPPING_ADDRESS});
                subRecord.setValue({ fieldId: ADDRESS_FIELDS.COUNTRY, value: options.countryCode || ''});
                subRecord.setValue({ fieldId: ADDRESS_FIELDS.CITY, value: options.city || ''});
                subRecord.setValue({ fieldId: ADDRESS_FIELDS.STATE, value: options.state || ''});
                subRecord.setValue({ fieldId: ADDRESS_FIELDS.ZIP, value: options.zip || ''});
                subRecord.setValue({ fieldId: ADDRESS_FIELDS.ADDR1, value: options.addr1 || ''});
                subRecord.setValue({ fieldId: ADDRESS_FIELDS.ADDR2, value: options.addr2 || ''});
                subRecord.setValue({ fieldId: ADDRESS_FIELDS.ADDR3, value: options.addr3 || ''});
                subRecord.setValue({ fieldId: ADDRESS_FIELDS.ADDRESSEE, value: options.addressee || ''});
                subRecord.setValue({ fieldId: ADDRESS_FIELDS.ATTENTION, value: options.attention || ''});
                subRecord.setValue({ fieldId: ADDRESS_FIELDS.PHONE, value: options.phone || ''});
            };
        }

        return {
            StandardTransaction: StandardTransaction
        };
    });