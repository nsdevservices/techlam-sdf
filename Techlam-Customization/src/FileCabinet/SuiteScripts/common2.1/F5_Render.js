define([
        'N/render'
    ],
    function (render) {
        return {
            mergeEmail(options) {
                return render.mergeEmail(options);
            },
            transaction(options) {
                return render.transaction(options);
            },
            create() {
                return render.create();
            },
            PrintMode: render.PrintMode,
            DataSource: render.DataSource
        };
    });