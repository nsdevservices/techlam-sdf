define([
        './F5_StandardConstants'
    ],
    function (f5_standard) {
        var TRANSACTION_FIELDS = f5_standard.TransactionField;
        // TRANSACTION_FIELDS.FOO = 'custbody_f5_foo';     // Add custom fields here
        TRANSACTION_FIELDS.REQUESTED_BY = 'custbody_nsts_gaw_tran_requestor';
        TRANSACTION_FIELDS.SUBMITTED_FOR_APPROVAL = 'custbody_f5_submitforapproval';
        TRANSACTION_FIELDS.SUPER_APPROVE = 'custbody_orion_posuperapproval';
        TRANSACTION_FIELDS.CREATED_BY = 'custbody_nsts_gaw_created_by';
        var ENTITY_FIELDS = f5_standard.EntityField;
        // ENTITY_FIELDS.BAR = 'custentity_f5_foo';     // Add custom fields here
        var ITEM_FIELDS = f5_standard.ItemField;
        // ITEM_FIELDS.BAZ = 'custitem_f5_foo';     // Add custom fields here
        var ITEM_NUMBER_FIELDS = f5_standard.ItemNumberField;
        // ITEM_NUMBER_FIELDS.FOO = 'custitemnumber_f5_foo';     // Add custom fields here
        var ENTITY_FIELDS = f5_standard.EntityField;
        // ENTITY_FIELDS.BAR = 'custentity_f5_foo';     // Add custom fields here
        var SUBLISTS = f5_standard.Sublist;
        // Add custom sublists here
        SUBLISTS.MULTIPLE_PO = 'custpage_f5_sbl_multiple_po';
        var SUBLIST_FIELDS = f5_standard.SublistField;
        // Add custom fields here
        var ROLES = f5_standard.Role;
        // Add custom roles here
        var TABS = f5_standard.Tab;
        // Add custom tabs here
        var SUBTABS = f5_standard.Subtab;
        // Add custom subtabs here
        var BUTTONS = f5_standard.Button;
        BUTTONS.FILTER = 'custpage_f5_btn_filter';
        BUTTONS.MARK_ALL = 'custpage_f5_btn_mark_all';
        BUTTONS.UNMARK_ALL = 'custpage_f5_btn_unmark_all';
        BUTTONS.RESET = 'custpage_f5_btn_reset';
        // Add custom button here

        return {
            Field: f5_standard.Field,
            AddressField: f5_standard.AddressField,
            TransactionField: TRANSACTION_FIELDS,
            EntityField: ENTITY_FIELDS,
            ItemField: ITEM_FIELDS,
            ItemNumberField: ITEM_NUMBER_FIELDS,
            Sublist: SUBLISTS,
            SublistField: SUBLIST_FIELDS,
            WorkflowField: f5_standard.WorkflowField,
            Join: f5_standard.Join,
            TransactionTypeShort: f5_standard.TransactionTypeShort,
            TransactionTypeName: f5_standard.TransactionTypeName,
            TransactionTypeInternalId: f5_standard.TransactionTypeInternalId,
            CheckBox: f5_standard.CheckBox,
            Role: ROLES,
            Feature: f5_standard.Feature,
            Preference: f5_standard.Preference,
            User: f5_standard.User,
            Mode: f5_standard.Mode,
            ItemType: f5_standard.ItemType,
            ItemTypeInternalId: f5_standard.ItemTypeInternalId,
            Button: BUTTONS,
            Subtab: SUBTABS,
            Tab: TABS,
            CustomRecordType: {
                // FOO: 'customrecord_f5_foo'
            },
            CustomList: {
                // BAR: 'customlist_f5_bar'
            },
            CustomField: {
                BANNER_MESSAGES: 'custpage_f5_banner_messages',
                OH_IAT_WEEKLY_EXEMPT: 'custrecord_oh_iat_time_weekly_exempt'
            },
            ScriptParameter: {
                SL_DISPLAY_MULTI_PO_FOR_PROCESSING_FOLDER: 'custscript_f5_sl_dmp_for_processing_fold',
                SL_DISPLAY_MULTI_PO_PROCESSED_FOLDER: 'custscript_f5_sl_dmp_processed_fold',
                MR_MULTI_PO_REQUESTED_BY:'custscript_f5_mr_process_multi_po_reqby',
                MR_MULTI_PO_CREATED_BY:'custscript_f5_mr_process_multi_po_crtdby',
                MR_MULTI_PO_USER_EMAIL:'custscript_f5_mr_process_multi_po_email',
                OH_MR_IAT_EXEMPT_SS: 'custscript_oh_mr_iat_ex_ss'
            },
            ScriptId: {
                MR_PROCESS_MULTI_PO:'customscript_f5_mr_process_multi_po'
            },
            DeploymentId: {
                MR_PROCESS_MULTI_PO:'customdeploy_f5_mr_process_multi_po'
            },
            FormulaTemplate: {
                CASE_IN_STRINGS: "CASE WHEN {{name}} IN ({values}) THEN 1 ELSE 0 END",
                CASE_IN_INTEGERS: "CASE WHEN {{name}} IN ({values}) THEN 1 ELSE 0 END"
            },
            Prefix: {
                COLUMN: 'custpage_f5_col_'
            },
            Label: {
                BANNER_MESSAGES: 'Banner Messages',
                FILTER: 'Filter',
                MARK_ALL: 'Mark All',
                RESET: 'Reset',
                SUBMIT: 'Submit',
                UNMARK_ALL: 'Unmark All',
                PURCHASE_ORDERS:'Purchase Orders',
            },
            Message: {
                PLEASE_SELECT_AT_LEAST_ONE: 'Please select at least one (1).',
                LOADING_SUITELET_ERROR: 'There was an unexpected error while loading the page. Please contact your System Administrator for more information.',
                MAP_REDUCE_SCRIPT_TRIGGERED :'The Map Reduce script that will process the Purchase Orders is now triggered. An email will be sent to {email} once it is complete.',
                MULTI_PO_EMAIL_BODY:'The following files are processed:'
            },
            ErrorMessage: {
                NO_INTERNAL_ID_FOUND:'No Internal ID found.',
                TRANSACTION_NOT_BALANCED:'Purchase Order has been incorrectly set up. Vendor Bill not balanced.',
            },
            Title: {
                MULTIPLE_PURCHASE_ORDERS_PROCESSING:'Multiple Purchase Orders Processing'
            }
        }
    });