define([
        'N/file',
        './F5_Search'
    ],
    function (file, f5_search) {
        var TYPE = 'file';
        var FIELDS = {
            URL: 'url',
            FOLDER: 'folder',
            INTERNAL_ID: 'internalid'
        };
        return {
            load: function load(options) {
                return file.load(options);
            },
            create: function create(options) {
                return file.create(options);
            },
            moveToAnotherFolder: function moveToAnotherFolder(options) {
                var file = this.load(options.idFile);
                file.folder = options.idNewFolder;
                var idFile = file.save();

                return idFile;

            },
            lookupUrl: function lookupUrl(id) {
                var values = f5_search.lookupFields({type: TYPE, id: id, columns: [FIELDS.URL]});
                return values[FIELDS.URL] || '';
            },
            searchFiles: function searchFiles(folderId) {
                var search = f5_search.create({
                    type: TYPE,
                    filters: [[FIELDS.FOLDER, f5_search.Operator.ANYOF, folderId]],
                    columns: [FIELDS.INTERNAL_ID]
                });

                return search.getIds() || [];
            },
            convertFileContentsToObject: function convertFileContentsToObject(options) {
                var file = this.load(options.idFile);
                var iterator = file.lines.iterator();
                //Assumption is the file is CSV and the Headers are the first line
                var headers = [];
                iterator.each(function (line) {
                    headers = line.value.split(',');
                    return false;
                });
                var contents = [];
                iterator.each(function (line) {
                    var content = {}
                    var lineValues = line.value.match(/(".*?"|[^",\s]+)(?=\s*,|\s*$)/g);//Ignore the commas enclosed by double quotes
                    lineValues.map(function (value, index) {
                        value = value.replace(/\"/g, '').trim();
                        value = parseFloat(value) ? parseFloat(value.replace(/,/g, '')) : value;
                        content[headers[index]] = value;
                    });
                    if(options.includeFileId) {
                        content.idFile = options.idFile;
                    }
                    contents.push(content)
                    return true;
                });

                return contents;
            },
            convertFolderFilesIntoObjects: function convertFolderFilesIntoObjects(options) {
                var me = this;
                var files = me.searchFiles(options.idFolder);
                var contents = [];
                files.forEach(function (idFile) {
                    contents = contents.concat(me.convertFileContentsToObject({idFile:idFile, includeFileId:options.includeFileId}))
                });
                return contents
            },
            getPONumbersFromFileContents: function getPONumbersFromFileContents(contents) {
                var poNumbers = [];
                if (contents && contents.length > 0) {
                    poNumbers = _.map(contents, Object.keys(contents[0])[0]);
                    poNumbers = poNumbers.map(function (poNumber) {
                        return "'" + poNumber + "'"
                    })
                }
                return poNumbers;
            },
            updateFilesWithVendorBillDetails: function updateFilesWithVendorBillDetails(options) {
                try {
                    var idFile = options.idFile;
                    var contents = options.contents;
                    var idNewFile = null;
                    var fileName = null;
                    if(idFile && contents. length > 0) {
                        var oldFile = this.load(idFile);
                        var newFile = this.create({
                            name: oldFile.name,
                            fileType: oldFile.fileType,
                            folder:oldFile.folder,
                            contents: ''
                        });
                        fileName = oldFile.name;
                        var newContents = [];
                        var iterator = oldFile.lines.iterator();
                        //Assumption is the file is CSV and the Headers are the first line
                        var headers = [];
                        iterator.each(function (line) {
                            newContents.push(line.value + ',Vendor Bill InternalId, Error Encountered')
                            return false;
                        });
                        var i =0;
                        iterator.each(function (line) {
                            var content = contents[i];
                            newContents.push(line.value + ',' + (content.idVendorBill || '') + ',' + (content.errorMessage || ''));
                            i++;
                            return true;
                        });
                        log.debug('adding new contents', newContents);
                        newContents.map(function (content) {
                            newFile.appendLine({value:content})
                        })
                        idNewFile = newFile.save(); //Save first to overwrite then move the files
                        idNewFile = this.moveToAnotherFolder({idFile:idNewFile, idNewFolder:options.idNewFolder});
                        log.debug('idNewFile', idNewFile);
                    }

                    return {idNewFile:idNewFile, fileName:fileName};

                }catch (e) {
                    log.debug('e', JSON.stringify(e));
                }



            }
        };
    });