define([
        './F5_Record',
        './F5_Search',
        './F5_Constants'
    ],
    function (f5_record, f5_search, f5_constants) {
        var TYPE = f5_search.Type.ITEM;
        var FIELDS = f5_constants.ItemField;

        return {
            type: TYPE,
            getItemDetailsMap: function getItemDetailsMap(items) {
                var map = {};
                var search = f5_search.create({
                    type: TYPE,
                    filters: [FIELDS.INTERNAL_ID, f5_search.Operator.ANYOF, items],
                    columns: [
                        {name: FIELDS.ITEM_ID},
                        {name: FIELDS.AVERAGE_COST},
                        {name: FIELDS.LAST_COST},
                        {name: FIELDS.ALTERNATE_COST}
                    ]
                });
                (search.getResults() || []).forEach(function (result) {
                    map[result.id] = {
                        itemNumber: result.getValue({name: FIELDS.ITEM_ID}),
                        averageCost: parseFloat(result.getValue({name: FIELDS.AVERAGE_COST}) || 0),
                        lastCost: parseFloat(result.getValue({name: FIELDS.LAST_COST}) || 0),
                        alternateCost: parseFloat(result.getValue({name: FIELDS.ALTERNATE_COST}) || 0)
                    };
                });

                return map;
            }
        };
    });