define([
        './F5_Search',
        './F5_Record',
        './F5_Constants',
        './F5_StandardTransaction'
    ],
    function (f5_search, f5_record, f5_constants, f5_standardTransaction) {
        var TYPE = f5_search.Type.TRANSACTION;
        var TYPE_IDS = f5_constants.TransactionTypeInternalId;
        var FIELDS = f5_constants.TransactionField;
        var SUBLISTS = f5_constants.Sublist;
        var SUBLIST_FIELDS = f5_constants.SublistField;
        var keyFieldIdMap = {
            subsidiary: FIELDS.SUBSIDIARY,
            class: FIELDS.CLASS,
            memo: FIELDS.MEMO,
            internalId:FIELDS.INTERNAL_ID,
            documentNumber:FIELDS.TRAN_ID,
            submittedForApproval:FIELDS.SUBMITTED_FOR_APPROVAL,
            superApprove:FIELDS.SUPER_APPROVE,
            requestedBy:FIELDS.REQUESTED_BY,
            tranDateText:FIELDS.TRAN_DATE,
            approvalStatus:FIELDS.APPROVAL_STATUS,
            createdBy:FIELDS.CREATED_BY
        };
        var lineKeyFieldIdMap = {
            quantity: SUBLIST_FIELDS.QUANTITY,
        };

        function Transaction(record) {
            var me = this;
            // inherit from Record
            f5_standardTransaction.StandardTransaction.apply(this, [record]);

            this.setTransactionValues = function setTransactionValues(options) {
                var values = options.values || [];
                me.setValues({
                    keyFieldIdMap:options.keyFieldIdMap || keyFieldIdMap,
                    values:values
                })
            }

            this.removeOtherLines = function removeOtherLines(options) { //Remove all lines except the first one
                var sublistId = options.sublistId || SUBLISTS.ITEM;
                var remainingLine = options.remainingLine || 0;
                var nItemCount = this.getLineCount({sublistId:sublistId});
                for(var i = nItemCount - 1; i > remainingLine; i--) {
                    this.removeLine({
                        sublistId:sublistId,
                        line:i
                    })
                }
            };

            this.setFirstLineAmount = function setFirstLineAmount(options) {
                log.debug('options', options);
                var sublistId = options.sublistId || SUBLISTS.ITEM;
                this.setSublistValues({
                    sublistId: sublistId,
                    keyFieldIdMap:lineKeyFieldIdMap,
                    fieldValues:options.fieldValues,
                    line:0
                })
            };

            this.checkIfWithTaxCode = function checkIfWithTaxCode() {
                log.debug(FIELDS.TAX_RATE, FIELDS.TAX_RATE);
                var grossamt = this.getSublistValue({
                    sublistId:SUBLISTS.ITEM,
                    fieldId:'grossamt',
                    line:0
                });
                log.debug('grossamt', grossamt);

                return false;
            }

        };

        return {
            Transaction: Transaction,
            cast: function cast(record) {
                return new Transaction(record);
            },
            create: function create(options) {
                return new Transaction(f5_record.create(options));
            },
            load: function load(options) {
                return new Transaction(f5_record.load(options));
            },
            transform: function transform(options) {
                return new Transaction(f5_record.transform(options));
            },
            lookupCreatedFrom: function lookupCreatedFrom(options) {
                options.columns = [FIELDS.CREATED_FROM];
                var createdFrom = '';
                var fieldLookUpObj = f5_search.lookupFields(options);
                var column = fieldLookUpObj[FIELDS.CREATED_FROM];
                if (column && column[0]) {
                    createdFrom = column[0].value || ''
                }
                return createdFrom;
            },
            lookupTransactionType: function lookupTransactionType(options) {
                options.columns = [FIELDS.TYPE];
                var transactionType = '';
                var fieldLookUpObj = f5_search.lookupFields(options);
                var column = fieldLookUpObj[FIELDS.TYPE];
                if (column && column[0]) {
                    transactionType = column[0].value || ''
                }
                return transactionType;
            },
            submitOrderStatus: function submitAutomationStatus(options) {
                var values = {};
                values[FIELDS.ORDER_STATUS] = options.value;
                f5_record.submitFields({type: options.type, id: options.id, values: values});
            },
            searchFromDocumentNumber: function searchFromDocumentNumber(options) {
                var documentNumbers = options.documentNumbers;
                var formulaDN = f5_search.FormulaTemplate.CASE_IN_STRINGS.replace('{name}', FIELDS.TRAN_ID);
                formulaDN = formulaDN.replace('{values}', documentNumbers);
                var search = f5_search.create({
                    type: f5_search.Type.TRANSACTION,
                    filters: [
                        [FIELDS.MAINLINE, f5_search.Operator.IS, 'T'],
                        f5_search.LogicalOperator.AND,
                        [f5_search.FormulaField.NUMERIC + ':' + formulaDN, f5_search.Operator.EQUALTO, 1]
                    ],
                    columns: [FIELDS.INTERNAL_ID, FIELDS.TRAN_ID]
                })

                return search.getJSONResults(keyFieldIdMap)
            },
            transformPOToVendorBill: function transformPOToVendorBill(options) {
                var poId = options.poId;
                var vendorBill = null;
                if(poId) {
                    try {
                        vendorBill = this.transform({fromType:f5_search.Type.PURCHASE_ORDER, fromId:poId, toType:f5_search.Type.VENDOR_BILL});
                    } catch (e) {
                        throw e;
                    }



                }
                return vendorBill;
            }

        };
    });